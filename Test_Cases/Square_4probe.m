%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%
%% add path
addpath(genpath('../'));
%% Generate Geometry
figure(1);clf

InjectorLength=5;
InjectorWidth=5;
SquereWidth=15/2;

PolyXD=[-SquereWidth,-SquereWidth,-SquereWidth-InjectorLength/sqrt(2),-SquereWidth-InjectorLength/sqrt(2)+InjectorWidth/sqrt(2),-SquereWidth+InjectorWidth/sqrt(2),0];
PolyYD=[0,SquereWidth-InjectorWidth/sqrt(2),SquereWidth-InjectorWidth/sqrt(2)+InjectorLength/sqrt(2),SquereWidth+InjectorLength/sqrt(2),SquereWidth,SquereWidth];
PolyXD=[fliplr(PolyXD(2:end)),PolyXD(2:end)];PolyYD=[-fliplr(PolyYD(2:end)),PolyYD(2:end)];
PolyXD=[-fliplr(PolyXD(2:end-1)),PolyXD(2:end-1)];PolyYD=[fliplr(PolyYD(2:end-1)),PolyYD(2:end-1)];
PolyXD=[PolyXD,PolyXD(1)];   PolyYD=[PolyYD,PolyYD(1)];

plot(PolyXD,PolyYD,'o-');axis equal

%% RhoXX changes sign
lMRList=logspace(0,2,30);VHall=[];VRhoXX=[];
for k=1:numel(lMRList)
[Simulation]=RunSimulation( [] ,struct(...
    ...     Define Walls geometry and behavior
    'WallsCoordinates',struct('Polygons',{[PolyXD;PolyYD]}),...
    'WallsBehavior',struct(...
                              'Rough',{[1:16],@(Theta,Direction,Rc,lMR)RoughWalls(Theta,Direction,Rc,lMR,0)} ...
                          ),...
    ...     How bulk behaves
    'lMR',lMRList(k),'ThetaScatter',@(n)rand(n,1)*2*pi,...
    ...     balls emitters and Absorbers (wall numbers)
    'N',1e4,'R',inf,'MaxCollisions',50,'Absorbers',[2:4:14],'AbsorbCoeff',1,...
    ...     Emitter 
    'Emitter',2,'EmitterRange',[0,1],'EmittedFunciton',@(n)asin(rand(n,1)*2-1)+3*pi/4,...
    ...     Aaccumulator
    'GridSize',0.25,'FillerNumber',0,'Distribution',360,'Slice',3,  ... 
    ... RealUnits
    'n_res',1 ...
    ));
% PlotSimulation(Simulation)
EndWallAcumulate=accumarray(Simulation.Transport.EndWall,1);EndWallAcumulate=EndWallAcumulate(2:4:14)/sum(EndWallAcumulate);
S=0.5;D=-0.5;
a=EndWallAcumulate(1);b=EndWallAcumulate(2);c=EndWallAcumulate(3);d=EndWallAcumulate(4);
VHall(:,k)=-([a-1,c;c,a-1]^(-1))*[S*b+D*d;S*d+D*b];
VRhoXX(:,k)=-([a-1,d;b,a-1]^(-1))*[S*c+D*b;S*d+D*c];

IHall(:,k)=S-a*S-b*VHall(2,k)-d*VHall(1,k)-c*D;
IRhoXX(:,k)=S-a*S-b*VHall(2,k)-c*VHall(1,k)-d*D;

disp(k);
end
%% hall Effect
WRc=linspace(-10,10,50);VHall=[];VRhoXX=[];IHall=[];IRhoXX=[];
 W=SquereWidth*2;
%W=InjectorWidth;
for k=1:numel(WRc)
[Simulation]=RunSimulation( [] ,struct(...
    ...     Define Walls geometry and behavior
    'WallsCoordinates',struct('Polygons',{[PolyXD;PolyYD]}),...
    'WallsBehavior',struct(...
                              'Rough',{[1:16],@(Theta,Direction,Rc,lMR)RoughWalls(Theta,Direction,Rc,lMR,0)} ...
                          ),...
    ...     How bulk behaves
    'lMR',20,'ThetaScatter',@(n)rand(n,1)*2*pi,...
    ...     balls emitters and Absorbers (wall numbers)
    'N',1e3,'R',W./WRc(k),'MaxCollisions',50,'Absorbers',[2:4:14],'AbsorbCoeff',1,...
    ...     Emitter 
    'Emitter',2,'EmitterRange',[0,1],'EmittedFunciton',@(n)asin(rand(n,1)*2-1)+3*pi/4,...
    ...     Aaccumulator
    'GridSize',0.25,'FillerNumber',0,'Distribution',360,'Slice',3,  ... 
    ... RealUnits
    'n_res',1 ...
    ));
% PlotSimulation(Simulation)
EndWallAcumulate=accumarray(Simulation.Transport.EndWall,1);EndWallAcumulate=EndWallAcumulate(2:4:14)/sum(EndWallAcumulate);
S=0.5;D=-0.5;
a=EndWallAcumulate(1);b=EndWallAcumulate(2);c=EndWallAcumulate(3);d=EndWallAcumulate(4);
VHall(:,k)=-([a-1,c;c,a-1]^(-1))*[S*b+D*d;S*d+D*b];
VRhoXX(:,k)=-([a-1,d;b,a-1]^(-1))*[S*c+D*b;S*d+D*c];

IHall(:,k)=S-a*S-b*VHall(2,k)-d*VHall(1,k)-c*D;
IRhoXX(:,k)=S-a*S-b*VRhoXX(2,k)-c*VRhoXX(1,k)-d*D;

disp(k);
end
clf
subplot(2,2,1);plot(WRc,(VHall))
subplot(2,2,2);plot(WRc,(VRhoXX))
subplot(2,2,3);plot(WRc,(IHall))
subplot(2,2,4);plot(WRc,(IRhoXX))
%%
clf
subplot(1,2,1);plot(WRc,diff(VHall)./IHall,'.-','linewidth',2);title('Hall Resistance');
subplot(1,2,2);plot(WRc,diff(VRhoXX)./IRhoXX,'.-','linewidth',2);title('RhoXX');
%%
WRc=5;
[Simulation]=RunSimulation( [] ,struct(...
    ...     Define Walls geometry and behavior
    'WallsCoordinates',struct('Polygons',{[PolyXD;PolyYD]}),...
    'WallsBehavior',struct(...
                              'Rough',{[1:16],@(Theta,Direction,Rc,lMR)RoughWalls(Theta,Direction,Rc,lMR,0)} ...
                          ),...
    ...     How bulk behaves
    'lMR',200,'ThetaScatter',@(n)rand(n,1)*2*pi,...
    ...     balls emitters and Absorbers (wall numbers)
    'N',1e4,'R',W./WRc,'MaxCollisions',50,'Absorbers',[2:4:14],'AbsorbCoeff',1,...
    ...     Emitter 
    'Emitter',2,'EmitterRange',[0,1],'EmittedFunciton',@(n)asin(rand(n,1)*2-1)+3*pi/4,...
    ...     Aaccumulator
    'GridSize',0.5,'FillerNumber',30,'Distribution',360,'Slice',3,  ... 
    ... RealUnits
    'n_res',1 ...
    ));
%%
[SimulationRhoXX,SimulationHall]=LandawerButikker(Simulation)
PlotSimulation(SimulationRhoXX)
%% Hall bar
Geometry=LoadGeometry();
PlotGeometry(Geometry);
%%
ContactList=[2,5,8,11,14,18];
W=15;
RcList=W./linspace(-6,6,61);


RhoXX=[];RHall1=[];RHall2=[];R2probe=[];
Simulation={};
for j=1:numel(RcList)
j
    for i=1:numel(ContactList)
        Simulation{j}{i}=RunSimulation( [] ,struct(...
            'WallsCoordinates',struct('Polygons',Geometry),...
            'WallsBehavior',struct(...
                'Rough',{[1:numel(Geometry)],RoughWalls} ...
            ),...
            'N',1e4,'R',RcList(j),'lMR',20,'Absorbers',ContactList,'Emitter',ContactList(i),'GridSize',1,'FillerNumber',0));
    end
end
%%
for j=1:numel(Simulation)
    [Voltages,Currents,SimulationOut]=LandawerButikker(Simulation{j},[1,1;4,-1]);
    RhoXX(j)=diff(Voltages([3,2]))./Currents(1);
    RHall1(j)=diff(Voltages([2,6]))./Currents(1);
    RHall2(j)=diff(Voltages([3,5]))./Currents(1);
    R2probe(j)=diff(Voltages([4,1]))./Currents(1);
end
%%
Geometry=LoadGeometry();
%%
ContactList=[4,2];
Simulation={};
    for i=1:numel(ContactList)
        Contact=ContactList(i);
        Simulation{i}=RunSimulation( [] ,struct(...
            'WallsCoordinates',struct('Polygons',Geometry),...
            'WallsBehavior',struct(...
            'Rough',{[1:numel(Geometry)],@(Theta,Direction,Rc,lMR)RoughWalls(Theta,Direction,Rc,lMR,1)} ...
            ),...
            'N',1e5,'R',inf,'lMR',inf,'Absorbers',ContactList,'Emitter',Contact,'GridSize',0.25/2,'FillerNumber',0));
    end
%%
[Voltages,Currents,SimulationOut]=LandawerButikker(Simulation,[1,0.5;2,-0.5]);
R=-diff(Voltages)./Currents(1)