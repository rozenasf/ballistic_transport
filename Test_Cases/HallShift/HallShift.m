
ContactList=[8,2,4,6];
WRcList=linspace(-6,6,31);
R=[];
for j=1:numel(WRcList)
Simulation={};W=5;L=15;D=2;
for i=1:numel(ContactList)
    Simulation{i}=RunSimulation( [] ,struct(...
        'WallsCoordinates',struct('Polygons',{[-L/2,-W/2;-D/2,-W/2;D/2,-W/2;L/2,-W/2;L/2,W/2;D/2,W/2;-D/2,W/2;-L/2,W/2;-L/2,-W/2]'}),...
        'WallsBehavior',struct('Rough',{1:8,@RoughWalls}),...
        'N',1e4,'R',W./WRcList(j),'lMR',20,'Absorbers',ContactList,'Emitter',ContactList(i),...
        'GridSize',20,'FillerNumber',0));
end
[Voltages,Currents,SimulationLandawerButikker]=LandawerButikker(Simulation,[1,1;3,-1]);
R(j)=(Voltages(2)-Voltages(4))/Currents(1);
j
end
plot(WRcList,(R-fliplr(R))/2);
%%
WRcList=2
Simulation={};W=5;L=15;D=0.5;
j=1;
for i=1:numel(ContactList)
    Simulation{i}=RunSimulation( [] ,struct(...
        'WallsCoordinates',struct('Polygons',{[-L/2,-W/2;-D/2,-W/2;D/2,-W/2;L/2,-W/2;L/2,W/2;D/2,W/2;-D/2,W/2;-L/2,W/2;-L/2,-W/2]'}),...
        'WallsBehavior',struct('Rough',{1:8,@RoughWalls}),...
        'N',1e4,'R',W./WRcList(j),'lMR',inf,'Absorbers',ContactList,'Emitter',ContactList(i),...
        'GridSize',1,'FillerNumber',30));
end
[Voltages,Currents,SimulationLandawerButikker]=LandawerButikker(Simulation,[1,1;3,-1]);
R(j)=(Voltages(2)-Voltages(4))/Currents(1);
%%
WRcList=linspace(-6,6,61);
Simulation={};W=5;L=15;D=1;
HallVoltage=[];HallCurrent=[];ButikkerVoltage=[];
for j=1:numel(WRcList);
    j
ContactList=[8,2,4,6];

for i=1:numel(ContactList)
    Simulation{i}=RunSimulation( [] ,struct(...
        'WallsCoordinates',struct('Polygons',{[-L/2,-W/2;-D/2,-W/2;D/2,-W/2;L/2,-W/2;L/2,W/2;D/2,W/2;-D/2,W/2;-L/2,W/2;-L/2,-W/2]'}),...
        'WallsBehavior',struct('Rough',{1:8,@RoughWalls}),...
        'N',1e4,'R',W./WRcList(j),'lMR',20,'Absorbers',ContactList,'Emitter',ContactList(i),...
        'GridSize',1,'FillerNumber',30,'AbsorbCoeff',1));
end

[Voltages,Currents,SimulationLandawerButikker]=LandawerButikker(Simulation,[1,1;3,-1]);
HallVoltage(j)=mean(diff(SimulationLandawerButikker.IDensity.data(7.5+[-1.5:1.5],[1,end])')');
HallCurrent(j)=Currents(1);
ButikkerVoltage(j)=(Voltages(2)-Voltages(4));
end
%%
RHall=ButikkerVoltage./HallCurrent;
RHallBulk=-0.35*HallVoltage./HallCurrent;
clf
plot(WRcList,(RHall-fliplr(RHall))/2);
hold on;
plot(WRcList,(RHallBulk-fliplr(RHallBulk))/2);
TXYL('Hall From simulation','W/Rc','','Hall From Butikker - correct','Hall From Bulk')
%%
clf
WRcList=linspace(-6,6,11);
Slope=[];Current=[];Hall=[];
SS={};CurrentSingle=[];
for i=1:numel(WRcList)
    i
    
    Simulation=RunSimulation( [] ,struct(...
        'WallsCoordinates',struct('Polygons',{[-L/2,-W/2;L/2,-W/2;L/2,W/2;-L/2,W/2;-L/2,-W/2]'}),...
        'WallsBehavior',struct('Rough',{1:4,@RoughWalls}),...
        'N',1e5,'R',W./WRcList(i),'lMR',20,'Absorbers',[2,4],'Emitter',2,...
        'GridSize',0.5,'FillerNumber',15,'AbsorbCoeff',1,'n_res',1,'CleanBalls',1));
    
    Hall(i,:)=mean(Simulation.IDensity.data(15+[-3:3],:));
    Hall=(Hall-fliplr(Hall))/2;
    Current(i,:)=mean(Simulation.ICurrent.data(15+[-3:3],:));
    Current=(Current+fliplr(Current))/2;
    CurrentSingle(i)=Simulation.Transport.TotalCurrent;
    
%     SS{i}=Simulation;
%     Fit_Line(1,'X',[-0.5,0.5]);
%     Slope(i)=Fit.a;
%     Current(i)=mean(Simulation.Transport.Transmission);
end
%%
plot(Simulation.IDensity.axes{2}.data,(Hall'./real(Current')));
Fit_Line(1,'X',[-1,1])
%%
plot(real(Current(:,:)'))
%%
Simulation=RunSimulation( [] ,struct(...
        'WallsCoordinates',struct('Polygons',{[-L/2,-W/2;L/2,-W/2;L/2,W/2;-L/2,W/2;-L/2,-W/2]'}),...
        'WallsBehavior',struct('Rough',{1:4,@RoughWalls}),...
        'N',1e4,'R',100,'lMR',20,'Absorbers',[2,4],'Emitter',2,...
        'GridSize',0.5,'FillerNumber',15,'AbsorbCoeff',1,'n_res',1,'CleanBalls',1));

    %%
%     plot(WRcList,-(Slope-fliplr(Slope))/2./Current./WRcList)
R=(Hall./repmat(Current',1,size(Hall,2)));
plot(Simulation.IDensity.axes{2}.data,(R-fliplr(R))/2);
Fit_Line(1,'X',[-1,1]);
plot(WRcList,-[Fit.a]./WRcList)


%%
clf
plot(ButikkerVoltage);hold on
plot(-0.35*HallVoltage);
TXYL('Hall Voltage From simulation','W/Rc','','Hall From Butikker - correct','Hall From Bulk')
%%
clf
plot(WRcList,HallCurrent);
TXYL('Current simulation','W/Rc','','Hall From Butikker - correct','Hall From Bulk')
% k=k+1;
% out{k}.HallVoltage=HallVoltage;
% out{k}.HallCurrent=HallCurrent;
%%
HallResistance=HallVoltage./HallCurrent;
plot(WRcList,(HallResistance-fliplr(HallResistance))/2)
% R(j)=(Voltages(2)-Voltages(4))/Currents(1);
%%
% RFull2=R;

%%
ContactList=[8,4];
WRcList=linspace(-10,10,31);
R=[];
for j=1:numel(WRcList)
Simulation={};W=5;L=60;D=0.5;
for i=1:numel(ContactList)
    Simulation{i}=RunSimulation( [] ,struct(...
        'WallsCoordinates',struct('Polygons',{[-L/2,-W/2;-D/2,-W/2;D/2,-W/2;L/2,-W/2;L/2,W/2;D/2,W/2;-D/2,W/2;-L/2,W/2;-L/2,-W/2]'}),...
        'WallsBehavior',struct('Rough',{1:8,@SpecularWalls}),...
        'N',1e3,'R',W./WRcList(j),'lMR',inf,'Absorbers',ContactList,'Emitter',ContactList(i),...
        'GridSize',0.25,'FillerNumber',30));
end
[Voltages,Currents,SimulationLandawerButikker]=LandawerButikker(Simulation,[1,1;2,-1]);
% R(j)=(Voltages(1)-Voltages(2))/Currents(1);
Data=SimulationLandawerButikker.IDensity.data;
R(j)=mean(mean(Data(size(Data,1)/2+[-3:3],1:1)))-mean(mean(Data(size(Data,1)/2+[-3:3],end)))./Currents(1);
end
plot(WRcList,R);
%%
mean(mean(SimulationLandawerButikker.IDensity.data(25:35,1:2)))-mean(mean(SimulationLandawerButikker.IDensity.data(25:35,19:20)))