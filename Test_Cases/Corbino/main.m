Geometry=LoadGeometry('OUTPUT.SLDMAT');
PlotGeometry(Geometry);
%% Moon crest effect - no collimation
Collimation=pi*1000;
ContactList=[1:9,11:15];
X=linspace(-pi/2,pi/2,100);
Lmr=20;Lee=inf;
Ltr=1./(1./Lmr+1./Lee);Pee=Ltr/Lee;
Simulation=RunSimulation( [] ,struct(...
    'WallsCoordinates',struct('Polygons',Geometry),...
    'WallsBehavior',struct('Rough',{1:17,@RoughWalls}),...
    'N',1e5,'R',1.5,'lMR',Ltr,'Absorbers',ContactList,'Emitter',14,'EmittedFunciton',[X-pi/2;exp(-X.^2/(2*( Collimation /2.3548)^2)).*cos(X)],...
    'GridSize',0.2,'FillerNumber',30,'MaxCollisions',50,'leeP',Pee,'n_res',1));
PlotSimulation(Simulation)

%% Moon crest effect -  collimation
Collimation=pi*0.1;
ContactList=[1:9,11:15];
X=linspace(-pi/2,pi/2,100);
Lmr=20;Lee=inf;
Ltr=1./(1./Lmr+1./Lee);Pee=Ltr/Lee;
Simulation=RunSimulation( [] ,struct(...
    'WallsCoordinates',struct('Polygons',Geometry),...
    'WallsBehavior',struct('Rough',{1:17,@RoughWalls}),...
    'N',1e5,'R',1.5,'lMR',Ltr,'Absorbers',ContactList,'Emitter',14,'EmittedFunciton',[X-pi/2;exp(-X.^2/(2*( Collimation /2.3548)^2)).*cos(X)],...
    'GridSize',0.2,'FillerNumber',30,'MaxCollisions',50,'leeP',Pee,'n_res',1));
PlotSimulation(Simulation)
%%
%% Hydro single Point
Collimation=pi/6;
ContactList=[1:9,11:15];
X=linspace(-pi/2,pi/2,100);
Lmr=5;Lee=inf;
Ltr=1./(1./Lmr+1./Lee);Pee=Ltr/Lee;
Simulation=RunSimulation( [] ,struct(...
    'WallsCoordinates',struct('Polygons',Geometry),...
    'WallsBehavior',struct('Rough',{1:17,@RoughWalls}),...
    'N',1e5,'R',3,'lMR',Ltr,'Absorbers',ContactList,'Emitter',14,'EmittedFunciton',[X-pi/2;exp(-X.^2/(2*( Collimation /2.3548)^2)).*cos(X)],...
    'GridSize',0.1,'FillerNumber',30,'MaxCollisions',50,'leeP',Pee,'n_res',1));
   PlotSimulation(Simulation)
% min(min(Simulation.IDensity.data))
min(min(Simulation.IDensity.data))./max(max(Simulation.IDensity.data))
%%
Collimation=pi*20;
ContactList=[1:9,11:15];
X=linspace(-pi/2,pi/2,100);
Simulation=RunSimulation( [] ,struct(...
    'WallsCoordinates',struct('Polygons',Geometry),...
    'WallsBehavior',struct('Rough',{1:17,@SpecularWalls}),...
    'N',1e3,'R',inf,'lMR',10,'Absorbers',ContactList,'Emitter',14,'EmittedFunciton',@(n,X,Y)asin(rand(n,1)*2-1)-pi/2,...
    'GridSize',0.2,'FillerNumber',30,'MaxCollisions',50));
PlotSimulation(Simulation)
%[X;exp(-X.^2/(2*( Collimation /2.3548)^2)).*cos(X)]
%%
Lmr=20;Lee=3;Ltr=1./(1./Lmr+1./Lee);Pee=Ltr/Lee;

Collimation=pi/6;
ContactList=[1:9,11:15];
X=linspace(-pi/2,pi/2,100);
Rlist=1./linspace(0.05,1,5);
Curve=[];
for i=1:numel(Rlist)
    Simulation=RunSimulation( [] ,struct(...
        'WallsCoordinates',struct('Polygons',Geometry),...
        'WallsBehavior',struct('Rough',{1:17,@RoughWalls}),...
        'N',1e4,'R',Rlist(i),'lMR',Ltr,'Absorbers',ContactList,'Emitter',14,'EmittedFunciton',[X-pi/2;exp(-X.^2/(2*( Collimation /2.3548)^2)).*cos(X)],...
        'GridSize',0.2,'FillerNumber',60,'MaxCollisions',50,'leeP',Pee));
    %    PlotSimulation(Simulation)
    Curve(i,:)=mean(Simulation.IDensity.data(:,30:40),2);
end
plot(Simulation.IDensity.axes{1}.data,Curve','linewidth',3)
ChangePlotColor
TXYL('X cut 1um above the contact,PSF=1um,lee=3','X[um]','Potential','R=%0.1f',{[Rlist]})
%%
