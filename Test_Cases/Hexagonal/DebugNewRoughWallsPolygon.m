Walls=Simulation.Walls;
R=[];
for i=1:1e4
Info=struct(...
       'IncidentAngle',45 / 180*pi,... IncidentAngle
       'Dircetion',1,... Dircetion
       'WallNo',1); %  Wall Number
       
            Info.Polygon=Simulation.Balls.Polygon;
            Info.Normal=0;
            Info.Walls=Walls;
            
   [Info]=RoughWallsPolygon(Info);
   R(i)=Info.ReflectionAngle*180/pi;
end
hist(R,100)