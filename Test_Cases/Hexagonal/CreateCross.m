function Geometry=CreateCross(W1,W2,L1)
XY=[0,W1,W1,W2,W2,L1,L1;L1,L1,W2,W2,W1,W1,0];
XY=[XY(:,1:end-1),[fliplr(XY(1,:));-fliplr(XY(2,:))]];
XY=[XY(:,1:end-1),[-fliplr(XY(1,:));fliplr(XY(2,:))]];
for i=size(XY,2):-1:1
   if(prod(XY(:,i) )==0);
       XY(:,i)=[];
   end
end
XY=[XY,XY(:,1)];
Geometry={};
for i=1:size(XY,2)-1
   Geometry{end+1}=[XY(1,i),XY(1,i+1);XY(2,i),XY(2,i+1)] ;
end
% plot(XY(1,:),XY(2,:));
Geometry=RotateGeometry(Geometry,0);
PlotGeometry(Geometry)
axis equal
end