clf;
Balls=Simulation.Balls;

PlotPolygon(Balls.WallsPolygon);
hold on

% FullPolygons=DuplicatePolygons(Balls.Polygon,[Balls.Xc(Balls.Active,Balls.Step)';Balls.Yc(Balls.Active,Balls.Step)']);
FullPolygons=DuplicatePolygons(Balls.Polygon,[Balls.Xc(:)';Balls.Yc(:)']);
PlotPolygon(FullPolygons)
% axis equal
% xlim([0.6,2.2]);
% ylim([0.6,2.2]);
% plot(Balls.X(:),Balls.Y(:),'o')