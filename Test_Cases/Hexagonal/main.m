
addpath(genpath('../../'));
%%
Geometry=CreateCross(2,7.5,20); %W1,W2,L1
%%
%complex Hexagonal
% Polygon=GetnerateFermiSurface([0,pi/6-0.1,pi/6+0.1,pi/3],[1,0.8,0.8,1],-0.27+0.0); %Angles,K value, Rotation of shape
%simple Hexagonal
Polygon=GetnerateFermiSurface([0,pi/3],[1,1],-0.27); %Angles,K value, Rotation of shape

PlotPolygon(Polygon);axis equal
%%

ContactList=[5,10,15,20];
Simulation=RunSimulation( [] ,struct(...
    'WallsCoordinates',struct('Polygons',Geometry),...
    'WallsBehavior',struct('Rough',{1:20,@RoughWallsPolygon}),...
    'N',1e3,'R',Polygon*10,'lMR',1000,'Absorbers',ContactList,'Emitter',5,...
    'GridSize',0.4,'FillerNumber',30,'MaxCollisions',200));

PlotSimulation(Simulation)
%%
TrajectoryExplorer(Simulation,1:10,30) %Simulation, balls list to plot, number of max collisions

%% You can change 'N' parameter for higher quality

PolyFactor=1./linspace(1./300,1./15,21); %magnetic field
ShiftAngle=linspace(0.02,pi/2-0.01,5);   %Rotation of the fermi surface

Simulation=[];Current=[];Voltage=[];
SLB={};
for k=1:numel(ShiftAngle)         %%All Fermi surface angles
    k
    Polygon=GetnerateFermiSurface([0,pi/3],[1,1],pi/3-pi/4+ShiftAngle(k));
    for j=1:numel(PolyFactor)      %All magnetic fields
        for i=1:numel(ContactList) %Loop on all emitters for LandawerButikker
            ContactList=[5,10,15,20];
            Simulation{i}=RunSimulation( [] ,struct(...
                'WallsCoordinates',struct('Polygons',Geometry),...
                'WallsBehavior',struct('Rough',{1:20,@RoughWallsPolygon}),...
                'N',1e3,'R',Polygon*PolyFactor(j),'lMR',10000,'Absorbers',ContactList,'Emitter',ContactList(i),...
                'GridSize',20,'FillerNumber',0,'MaxCollisions',50));
        end
        j
        [Voltages,Currents,SimulationLandawerButikker]=LandawerButikker(Simulation,[1,1;2,-1]);
        SLB{j,k}=SimulationLandawerButikker;
        Current(j,k)=Currents(1);
        Voltage(j,k,:)=Voltages;
    end
end
%% Plot the Rhoxx Graph
clf
plot((Voltage(:,:,4)-Voltage(:,:,3))./Current,'linewidth',3)
ChangePlotColor
xlabel('magnetic field [arb]');
ylabel('RhoXX [arb]');
title('RhoXX as funciton of Rotation');
%% from cluster:
 load('Cluster_Hexagonal_Fermi.mat');
clf
plot(log(MagneticField),(V4-V3)./I,'.-','linewidth',2,'markersize',10);
ChangePlotColor;
legend('0','pi/12','2*pi/12','3*pi/12')
xlabel('log of magnetic field');
ylabel('RhoXX');
title('RhoXX as function of magnetic field for different Fermi surface rotations')
%%
RotationList=[0,pi/12,2*pi/12,3*pi/12]+0.001;
Simulation={};SimulationTypical={};SimulationLandawerButikker={};
for i=1:numel(RotationList)
    Polygon=GetnerateFermiSurface([0,pi/3],[1,1],RotationList(i));
        for k=1:4
            ContactList=[5,10,15,20];
            Simulation{k}=RunSimulation( [] ,struct(...
                'WallsCoordinates',struct('Polygons',RotateGeometry(CreateCross(2,7.5,15),0)),...
                'WallsBehavior',struct('Rough',{1:20,@RoughWallsPolygon}),...
                'N',1e4,'R',Polygon*1e4,'lMR',1e4,'Absorbers',ContactList,'Emitter',ContactList(k),...
                'GridSize',0.5,'FillerNumber',100,'MaxCollisions',50));
        end
        [Voltages,Currents,SimulationLandawerButikker{i}]=LandawerButikker(Simulation,[1,1;2,-1]);
end
for i=1:numel(RotationList)
    Polygon=GetnerateFermiSurface([0,pi/3],[1,1],RotationList(i));
        for k=1:2
            ContactList=[5,10,15,20];
            SimulationTypical{k,i}=RunSimulation( [] ,struct(...
                'WallsCoordinates',struct('Polygons',RotateGeometry(CreateCross(2,7.5,15),0)),...
                'WallsBehavior',struct('Rough',{1:20,@RoughWallsPolygon}),...
                'N',100,'R',Polygon*1e4,'lMR',1e4,'Absorbers',ContactList,'Emitter',ContactList(k),...
                'GridSize',0.5,'FillerNumber',100,'MaxCollisions',1e3));
        end
end
save('Typical_trajectories.mat','Simulation','SimulationLandawerButikker','SimulationTypical');
%%
figure(2);clf;hold on
o=1;
for i=1:4
    subplot(4,4,o);o=o+1;
    PlotPolygon(GetnerateFermiSurface([0,pi/3],[1,1],RotationList(i)))
    axis equal
end
for i=1:4
    subplot(4,4,o);o=o+1;
    imagesc(Simulation{1}.IDensity.axes{1}.data,Simulation{1}.IDensity.axes{2}.data,SimulationLandawerButikker{i}.IDensity.data');
    hold on;
    PlotWalls(Simulation{1,1}.Walls,Simulation{1,1}.Parameters);
    axis equal
    drawnow
    caxis([-1,1]);
    set(gca,'YDir','normal')
end
for k=1:2
    for i=1:4
        subplot(4,4,o);o=o+1;
        TrajectoryExplorer(SimulationTypical{k,i},40:50,10);
        drawnow
        axis equal
    end
end
%  PlotSimulation(Simulation);
%  TrajectoryExplorer(Simulation,1:10,30);
%  axes equal
 


