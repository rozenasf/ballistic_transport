Data=LoadCluster('\\data.wexac.weizmann.ac.il\ilani\asafr\spetial_ballistic_cases');
Data=SimulationIData(Data);
%%
Simulation=Data.data;R=[];
for j=1:numel(Simulation)
    [Voltages,Currents,SimulationOut]=LandawerButikker(Simulation{j},[1,1;4,-1]);
    RhoXX(j)=diff(Voltages([3,2]))./Currents(1);
    RHall1(j)=diff(Voltages([2,6]))./Currents(1);
    RHall2(j)=diff(Voltages([3,5]))./Currents(1);
    R2probe(j)=diff(Voltages([4,1]))./Currents(1);
    R(j)=Simulation{j}{1}.Parameters.R;
end
%%
Simulation=Data.data;
RHall=[];R_2probe=[];RhoXX=[];R=[];
Order=[1,2,3,4;2,3,4,1;3,4,1,2;4,1,2,3];

for j=1:numel(Simulation)
    RHall(j)=0;R_2probe(j)=0;RhoXX(j)=0;
    Counter=0;
    for i=1:1
    [Voltages,Currents]=LandawerButikker(Simulation{j},[Order(i,1),1;Order(i,3),-1]);
    RHall(j)=RHall(j)+(Voltages(Order(i,2))-Voltages(Order(i,4)))/Currents(Order(i,1));
    R_2probe(j)=R_2probe(j)+(Voltages(Order(i,1))-Voltages(Order(i,3)))/Currents(Order(i,1));
    
    [Voltages,Currents]=LandawerButikker(Simulation{j},[Order(i,1),1;Order(i,2),-1]);
    RhoXX(j)=RhoXX(j)+(Voltages(Order(i,4))-Voltages(Order(i,3)))/Currents(Order(i,1));
    Counter=Counter+1;
    end
    R(j)=Simulation{j}{1}.Parameters.R;
end
RhoXX=(RhoXX+fliplr(RhoXX))/2/Counter;
RHall=(RHall-fliplr(RHall))/2/Counter;
R_2probe=(R_2probe+fliplr(R_2probe))/2/Counter;
R=mean(reshape(R,5,141));
RhoXX=mean(reshape(RhoXX,5,141));
RHall=mean(reshape(RHall,5,141));
R_2probe=mean(reshape(R_2probe,5,141));
%%
plot(15./R,RHall,'o-')