%-------------------------------------------%

%       spetial_ballistic_transport         %

% Written by Asaf Rozen - Weizmann Insitute %

%       asaf.rozen@weizmann.ac.il           %

%-------------------------------------------%


see Test_Cases\Transport_Example\Transport_Example.mlx to get started fast

see Test_Cases/Examples.m for more basic examples



%% Field description (ordered by importance)

% WallsCoordinates - The geometry as list of polygons (not necessary closed ones)
% WallsBehavior    - What happens when a particle hits a wall. list of wall number and functions to run
% Absorbers        - Which walls are the absorbing walls
% Emitter          - What wall emits particles (note: you can also specify exactly X,Y,Theta list for emission)

% lMR              - Mean free path for momentum relaxing scattering event (i.e disorder scattering)
% N                - Number of particles to simulate
% R                - Cyclotron radius. Set to inf for no magnetic field.
% n_res            - The density of the reservoir emitting from the emitter.

% GridSize          - The grid size

% EmittedFunciton  - Distribution of emitted particles (for a common reservoir, this will be a sin(theta) distribution)
% EmitterRange     - What range of the emitter wall is active (example: [0,1] is all, [0.5,0.5] is from a point in the center)
% ThetaScatter     - Function to run when scattering from lMR
% AbsorbCoeff      - What is the absorption coefficient for the absorbing walls

% MaxCollisions    - The number of hits each particle has on a computational cycle. 50 is a reasonable number.
% FillerNumber     - Break each trajectory into FillerNumber points. Should be roughly few times mean free path divided by grid size
% Distribution     - How many angles to keep in the accumulation. 360 is fine.
% Slice            - Projecting the trajectories on the grid might be RAM costly. This breaks the process into steps. Few is fine
