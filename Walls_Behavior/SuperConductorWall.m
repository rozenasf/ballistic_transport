function [OutAngle,Rc,lMR,Charge]=SuperConductorWall(IncidentAngle,Direction,Rc,lMR,Charge)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

OutAngle=IncidentAngle;
Rc=-Rc;
Charge=-Charge;
end