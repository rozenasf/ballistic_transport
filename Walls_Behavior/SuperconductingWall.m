function [OutAngle,Rc,lMR,AlphaCharge]=SuperconductingWall(IncidentAngle,Direction,Rc,lMR,AlphaCharge)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

OutAngle=IncidentAngle;
Rc=-Rc;
AlphaCharge=-AlphaCharge;


end
%(IncidentAngle,Direction,Rc,lMR,AlphaCharge)