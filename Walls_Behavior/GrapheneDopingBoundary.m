function [OutAngle,Rc,lMR]=GrapheneDopingBoundary(ThetaI,Direction,Rc,lMR,nRatio,dk)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

if(~exist('dk'));dk=0;end
%Snall Law for S polerized light
% DiffusiveVector=(randn(size(ThetaI))-0.5)*diffusivity;
OutAngle=ThetaI*0;
nRatioPerBall=ThetaI*0;
DirectionIDX=(Direction==1);

nRatioPerBall(DirectionIDX)=nRatio*ones(sum(DirectionIDX),1);
nRatioPerBall(~DirectionIDX)=1/nRatio*ones(sum(~DirectionIDX),1);

% ThetaI=ThetaI+DiffusiveVector;

ThetaT=real(asin((1./nRatioPerBall).*sin(ThetaI)));
if(sign(nRatioPerBall)==1)
    ReflectionProbability=1-(cos(ThetaI).*cos(ThetaT))./(cos( (ThetaI+ThetaT)/2)).^2;
else
    ReflectionProbability=1-...
        (cos(ThetaI).*cos(ThetaT))./(cos( (ThetaI+ThetaT)/2)).^2.*...
        exp(pi*dk.*sin(ThetaT).*sin(ThetaI).*((-nRatioPerBall)./(1-nRatioPerBall)));
%         ReflectionProbability(ThetaT==0)=0;
end
IsReflected=(rand(size(ReflectionProbability))<ReflectionProbability);
OutAngle(~IsReflected) =  ThetaT(~IsReflected)+pi;
OutAngle( IsReflected) = -ThetaI( IsReflected);

Rc(~IsReflected)=Rc(~IsReflected).*nRatioPerBall(~IsReflected);

end