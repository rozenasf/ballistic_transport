function [OutAngle,Rc,lMR]=PartialRoughWalls(IncidentAngle,Direction,Rc,lMR,P)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

Logic=find(rand(numel(IncidentAngle),1)<P);
OutAngle=-IncidentAngle;
OutAngle(Logic)=asin(rand(numel(IncidentAngle(Logic)),1)*2-1);
end