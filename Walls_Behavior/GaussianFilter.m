function Info=GaussianFilter(Info,Options)

%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

%Snall Law for S polerized light
Info.ReflectionAngle=Info.IncidentAngle*0;

IsReflected=(rand(size(Info.IncidentAngle))>exp(-Info.IncidentAngle.^2./(2*Options.Sigma^2)));

Info.ReflectionAngle(~IsReflected) =  Info.IncidentAngle(~IsReflected)+pi;
Info.ReflectionAngle( IsReflected) = -Info.IncidentAngle( IsReflected);
end