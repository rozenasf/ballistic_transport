function [Info]=RoughWalls(Info,Options)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%
% IncidentAngle,Direction,Rc,lMR,Alpha,P
if(~exist('Options','var'));P=0;else  P=Options; end % P=0 means rough

IncidentAngle=Info.IncidentAngle;

Info.ReflectionAngle=IncidentAngle*0;
Probability=P.*ones(size(IncidentAngle));

IsProbability=(rand(size(Probability))<Probability);

Info.ReflectionAngle(~IsProbability) =  asin(rand(numel(IncidentAngle(~IsProbability)),1)*2-1);
Info.ReflectionAngle( IsProbability) = -IncidentAngle( IsProbability);

if(isfield(Info,'Polygon'))
IncomingAngle=Info.Normal + Info.IncidentAngle + pi;
SignIncidentAngle=AngDiff(PolyAngleFromPolyK(Info.Polygon,IncomingAngle)'+pi,Info.Normal)>=pi/2;
 OutGoingAngle=Info.Normal + Info.ReflectionAngle;
 
 NotLegal = xor(SignIncidentAngle,AngDiff(PolyAngleFromPolyK(Info.Polygon,OutGoingAngle)',Info.Normal)>=pi/2);
 KKK=0;
 while(sum(NotLegal)>0)
     KKK=KKK+1;
      Info.ReflectionAngle(NotLegal) =  asin(rand(numel(IncidentAngle(NotLegal)),1)*2-1);

     OutGoingAngle(NotLegal)=Info.ReflectionAngle(NotLegal)+Info.Normal(NotLegal);
     NotLegal(NotLegal) = xor(SignIncidentAngle(NotLegal),AngDiff(PolyAngleFromPolyK(Info.Polygon,OutGoingAngle(NotLegal))',Info.Normal(NotLegal))>pi/2);
 end
 
%  randpdf(Parameters.EmittedFunciton(2,:),Parameters.EmittedFunciton(1,:),[N,1])
end
1;
% -1.6071
% -0.0189
end