function [Info]=RoughWallsPolygon(Info,Options)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

Info.OutputAngle=Info.InputAngle*nan;

SignIncidentAngle=AngDiff(mod(PolyAngleFromPolyK(Info.Polygon,Info.InputAngle)',2*pi),Info.Normal)>=pi/2;
%  [SignIncidentAngle(:),mod(PolyAngleFromPolyK(Info.Polygon,Info.InputAngle(:))',2*pi)*180/pi,Info.Normal(:)*180/pi]

1;
for i=unique(Info.WallNo)'
    for j=[0,1]
        Logic= ((Info.WallNo==i) & (SignIncidentAngle~=j));
        if(sum(Logic)>0)
            Info.OutputAngle(Logic)=interp1(Info.Walls(i).CumSumProbability{j+1},Info.Walls(i).ScatterAngles{j+1},rand(sum(Logic),1),'nearest');
        end
    end
end

end