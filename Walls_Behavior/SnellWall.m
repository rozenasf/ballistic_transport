function [OutAngle,Rc,lMR]=SnellWall(ThetaI,Direction,Rc,lMR,nRatio)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

%Snall Law for S polerized light
OutAngle=ThetaI*0;
nRatioPerBall=ThetaI*0;
DirectionIDX=(Direction==1);

nRatioPerBall(DirectionIDX)=nRatio*ones(sum(DirectionIDX),1);
nRatioPerBall(~DirectionIDX)=1/nRatio*ones(sum(~DirectionIDX),1);

ThetaT=real(asin(nRatioPerBall.*sin(ThetaI)));

ReflectionProbability=((cos(ThetaI)-1./nRatioPerBall.*cos(ThetaT))./(cos(ThetaI)+1./nRatioPerBall.*cos(ThetaT))).^2;
IsReflected=(rand(size(ReflectionProbability))<ReflectionProbability);
OutAngle(~IsReflected) =  ThetaT(~IsReflected)+pi;
OutAngle( IsReflected) = -ThetaI( IsReflected);
end