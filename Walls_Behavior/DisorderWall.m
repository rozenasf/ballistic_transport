function [OutAngle,Rc,lMR]=DisorderWall(ThetaI,Direction,Rc,lMR,Disorder)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

OutAngle=ThetaI+pi;

DirectionIDX=(Direction==1);

lMR(DirectionIDX) =Disorder(2)*ones(sum( DirectionIDX),1);
lMR(~DirectionIDX)=Disorder(1)*ones(sum(~DirectionIDX),1);

end