function [Info]=SpecularWalls(Info)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

Info.ReflectionAngle=-Info.IncidentAngle;

% 
% Info.ReflectionAngle=-Info.IncidentAngle;
% 
% 
% 
% IncomingAngle=Info.Normal + Info.IncidentAngle + pi;
% SignIncidentAngle=AngDiff(PolyAngleFromPolyK(Info.Polygon,IncomingAngle)'+pi,Info.Normal)>=pi/2;
%  OutGoingAngle=Info.Normal + Info.ReflectionAngle;
%  NotLegal = xor(SignIncidentAngle,AngDiff(PolyAngleFromPolyK(Info.Polygon,OutGoingAngle)',Info.Normal)>=pi/2);
%  while(sum(NotLegal)>0)
%      Info.ReflectionAngle(NotLegal) =  Info.ReflectionAngle(NotLegal)-0.01;
%      OutGoingAngle(NotLegal)=Info.ReflectionAngle(NotLegal)+Info.Normal(NotLegal);
%      NotLegal = xor(SignIncidentAngle,AngDiff(PolyAngleFromPolyK(Info.Polygon,OutGoingAngle)',Info.Normal)>pi/2);
%  end
%  
% disp([[Info.Normal,Info.IncidentAngle,Info.ThetaK]*180/pi])
end