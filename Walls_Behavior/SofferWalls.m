function [OutAngle,Rc,lMR]=SofferWalls(IncidentAngle,Direction,Rc,lMR,alpha)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

% angle-dependent probability for specular scattering
% based on https://arxiv.org/pdf/cond-mat/9411067.pdf, eqn. 13
% incident angle is with respect to normal!
P = exp(-(alpha*cos(IncidentAngle)).^2);
ll = rand(numel(IncidentAngle),1)>P;
Logic=find(ll);
OutAngle=-IncidentAngle;
OutAngle(Logic)=asin(rand(numel(IncidentAngle(Logic)),1)*2-1);
%table(IncidentAngle/pi*180, P, ll, OutAngle/pi*180)
end
