function [OutAngle,Rc,lMR]=RejectBoundary(IncidentAngle,Direction,Rc,lMR,P)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

Logic=find(rand(numel(IncidentAngle),1)<P);
OutAngle=IncidentAngle+pi;

OutAngle(Logic)=-IncidentAngle(Logic);
end