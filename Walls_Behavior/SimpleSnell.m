function Info=SimpleSnell(Info,Options)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

IncidentAngle=Info.IncidentAngle;

if(Info.Direction==1)
    n=Options.n;
else
    n=1./Options.n;
end

Info.ReflectionAngle=asin(n*sin(IncidentAngle))+pi;
Info.ReflectionAngle(imag(Info.ReflectionAngle)~=0)=-IncidentAngle(imag(Info.ReflectionAngle)~=0);

end