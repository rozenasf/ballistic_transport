function [OutAngle,Rc,lMR,AlphaCharge,OutAngleK]=RoughWalls_Hexagonal(IncidentAngle,Direction,Rc,lMR,P,AlphaCharge,IncidentAngleK)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

 if(~exist('P'));P=0;end % P=0 means rough
 OutAngleK=IncidentAngle*0;
 Probability=P.*ones(size(IncidentAngle));
% 
 IsProbability=(rand(size(Probability))<Probability);
% 
 OutAngleK(~IsProbability) =  asin(rand(numel(IncidentAngle(~IsProbability)),1)*2-1);
 OutAngleK( IsProbability) = -1e-6-IncidentAngle( IsProbability);
 OutAngle=IncidentAngle*nan;
%OutAngleK=-1e-6-IncidentAngleK;
% OutAngle=nan;
%OutAngle=-1e-6-IncidentAngle;
end