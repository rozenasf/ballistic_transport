function Stream=PlotSystem(IDensity,ICurrent,Walls,StreamWallIndex)
    cla
    %contour(Density,30,'LineWidth',3)
    e=pcolor(IDensity);
    e.FaceAlpha=0.5;
    PlotWalls(Walls);
    hold on
    t=linspace(0,1,20);
    if(isstruct(StreamWallIndex))
        xlist=StreamWallIndex.X;
        ylist=StreamWallIndex.Y;
    else
    WallR=Walls(StreamWallIndex);
    xlist=Scale2Real(WallR.X(1),1,Parameters,Walls)+t*diff(Scale2Real(WallR.X,1,Parameters,Walls));
    ylist=Scale2Real(WallR.Y(1),2,Parameters,Walls)+t*diff(Scale2Real(WallR.Y,2,Parameters,Walls));
    end

    [x,y]=meshgrid(ICurrent.axes{2}.data,ICurrent.axes{1}.data);

     [Stream]=stream2(y',x',real(ICurrent.data)',imag(ICurrent.data)',xlist,ylist);
    streamline(Stream);
     %quiver(y,x,real(ICurrent.data),imag(ICurrent.data),2)
    colormap(jet)
    colorbar;
    hold off
    
    axis equal
    hold off
end
