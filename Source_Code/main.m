%% Define problem parameters
ParametersArray=struct('P',1,'lMR',80,'ThetaScatterMR',2*pi,...
    'MaxCollisions',1e3,'N',1e4,'R',{-10,10},'MaxMument',2,'Absorbers',[1,5],...
    'Emitter',5,'EmitterRange',[0,1],...
    'EmittedFunciton',@()pi+asin(rand*2-1)/100,...
    'GridSize',0.2,'Charge',{1,1});
WallsCoordinates={
    [-20,4.5;
    -20,-4.5;
    0,-4.5;
    0,-1.5;
    20,-1.5;
    20,1.5;
    0,1.5;
    0,4.5;
    -20,4.5]'
    };

%% Run Simulation
RunSimulation;
%%
Stream=PlotSystem(IDensity,ICurrent,Walls,Parameters.Emitter);
%%
%     AnimateStream(ICurrent,Stream);