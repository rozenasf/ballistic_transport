function [Simulation,Balls]=RunBolzmanSimulation(Simulation,Parameters)
% global Balls
if(numel(Parameters)>1)
    for i=1:numel(Parameters)
        %if it is a parameter array, loop over it
        [Simulation,Balls]=RunBolzmanSimulation(Simulation,Parameters(i));
    end
else

    if(isempty(Simulation)) %initialize an empty simulation
        Simulation=[];
        Simulation.Walls=CreateWalls(Parameters);
        Simulation.Accumulator={};
        Simulation.Size=[ceil(Simulation.Walls(1).MaxScaled(1)-Simulation.Walls(1).MinScaled(1)),...
                ceil(Simulation.Walls(1).MaxScaled(2)-Simulation.Walls(1).MinScaled(2))];
        Simulation.Parameters=Parameters;
        
        for i=1:Parameters.MaxMument
            Simulation.Accumulator{i}=zeros(Simulation.Size);
        end
        if(isfield(Parameters,'Distribution'))
            Simulation.Distribution.Data=zeros([Simulation.Size,Parameters.Distribution]);
            Simulation.Distribution.ThetaList=((1:Parameters.Distribution)-0.5)*360/Parameters.Distribution;
        end
    end
    Simulation.Tic.TotalTic=tic;
    %% Generate Balls
    Magnetic=~isinf(Parameters.R);
    if(Magnetic)
        Balls=InitializeBallsWithField(Simulation.Walls,Parameters);
    else
        Balls=InitializeBallsNoField(Simulation.Walls,Parameters);
    end
    %% --- Main Loop ---
    Simulation.Tic.CalculationTic=tic;
    for i=1:Parameters.MaxCollisions-1
        if(Magnetic)
            Balls=CalculateNextCollisionWithField(Balls,Simulation.Walls);
        else
            Balls=CalculateNextCollisionNoField(Balls,Simulation.Walls);
        end
        %if(sum(isnan(Balls.WallNo(Balls.Active,Balls.Step+1)))>0);continue;end % patch to allow the leakage not to affect to overnight calculation
        [Done,Balls]=ScatterV2(Balls,Simulation.Walls,Parameters);
        if(Done);break;end %all balls are collected
        
        Balls=UpdateBallsMagneticSpecificParameters(Balls,Parameters);
        
        Balls.Step=Balls.Step+1;
    end
    Simulation.Tic.CalculationTic=toc(Simulation.Tic.CalculationTic);
    IterationsNeeded=i;
    %% Calculate Accumulator
    Simulation.Tic.AccumTic=tic;
    AccumulatorSize=Simulation.Size;
    if (Magnetic)
        [NewAccumulator,Distribution]=AddToAccumulatorWithField(Balls,Parameters,AccumulatorSize);
    else
        [NewAccumulator,Distribution]=AddToAccumulatorNoField(Balls,Parameters,AccumulatorSize);
    end
    
    % Add to existing Accumulator
    for i=1:Parameters.MaxMument
        Simulation.Accumulator{i}=Simulation.Accumulator{i}+NewAccumulator{i};
    end
    if(isfield(Parameters,'Distribution'))
       Simulation.Distribution.Data=Simulation.Distribution.Data+Distribution;
    end
    Simulation.Tic.AccumTic=toc(Simulation.Tic.AccumTic);
    %Create Idata objects
    if(Parameters.MaxMument<2)
         for i=1:2
             Simulation.Accumulator{i}=squeeze(sum(bsxfun(@times, Simulation.Distribution.Data,...
                 reshape(exp(-1i*Simulation.Distribution.ThetaList/180*pi*(i-1)), 1, 1,...
                 size(Simulation.Distribution.ThetaList,2))), 3));
         end
    end
        [IDensity,ICurrent,X,Y]=GetIdata(Simulation.Accumulator,Parameters,Simulation.Walls);
        Simulation.IDensity=IDensity;
        Simulation.ICurrent=ICurrent;
        Simulation.X=X;Simulation.Y=Y;
   
 % print output
     if(IterationsNeeded==(Parameters.MaxCollisions-1))
        fprintf('Increace number of max collisions\n');
    else
        fprintf('needed %d iterations,Trajectory %0.2f [s], Ram %0.2f [s]\n',IterationsNeeded,Simulation.Tic.CalculationTic,Simulation.Tic.AccumTic);
    end
end
end
%%
