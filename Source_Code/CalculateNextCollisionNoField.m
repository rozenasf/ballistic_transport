function Balls=CalculateNextCollisionNoField(Balls,Walls)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

CrashAt=nan(numel(Balls.Active),3,numel(Walls));

for i=1:(numel(Walls)-2)
    Wall=Walls(i);
    switch Wall.Type
        case 1 %X0=X1
            Y=Balls.m(Balls.Active,Balls.Step)*Wall.X(1)+Balls.n(Balls.Active,Balls.Step);
            Logic=(Wall.X(1)*Balls.Dir(Balls.Active,Balls.Step)>Balls.X(Balls.Active,Balls.Step).*Balls.Dir(Balls.Active,Balls.Step)).*InRange(Y,Wall.Y(1),Wall.Y(2));
            Logic(Logic==0)=nan;
            CrashAt(:,:,i)=[Wall.X(1).*Logic,Y.*Logic,i.*Logic];
            
        case 2 %Y0=Y1
            X=(Wall.Y(1)-Balls.n(Balls.Active,Balls.Step))./Balls.m(Balls.Active,Balls.Step);
            Logic=(Wall.Y(1)*Balls.Dir(Balls.Active,Balls.Step).*Balls.m(Balls.Active,Balls.Step)>Balls.Y(Balls.Active,Balls.Step).*Balls.Dir(Balls.Active,Balls.Step).*Balls.m(Balls.Active,Balls.Step)).*InRange(X,Wall.X(1),Wall.X(2));
            Logic(Logic==0)=nan;
            CrashAt(:,:,i)=[X.*Logic,Wall.Y(1).*Logic,i.*Logic];
        case 0
            X=-(Balls.n(Balls.Active,Balls.Step)-Wall.n)./(Balls.m(Balls.Active,Balls.Step)-Wall.m);
            Logic=(X.*Balls.Dir(Balls.Active,Balls.Step)>Balls.X(Balls.Active,Balls.Step).*Balls.Dir(Balls.Active,Balls.Step)+1e-8);
            Logic=Logic.*InRange(X,Wall.X(1),Wall.X(2));
            Logic(Logic==0)=nan;
            Y=X.*Balls.m(Balls.Active,Balls.Step)+Balls.n(Balls.Active,Balls.Step);
            CrashAt(:,:,i)=[X.*Logic,Y.*Logic,i.*Logic];
    end
end

X=Balls.X(Balls.Active,Balls.Step)+Balls.Dir(Balls.Active,Balls.Step).*sqrt(exprnd(Balls.lMR(Balls.Active,Balls.Step)).^2./(1+Balls.m(Balls.Active,Balls.Step).^2));
CrashAt(:,:,numel(Walls)-1)=[X,Balls.m(Balls.Active,Balls.Step).*X+Balls.n(Balls.Active,Balls.Step),(numel(Walls)-1).*ones(numel(X),1)];
%Bulk dying

if(isfield(Balls,'DeltaT'))
    L=Balls.DeltaT*Balls.Vfermi-Balls.LengthOfFlight(Balls.Active);
    X=Balls.X(Balls.Active,Balls.Step)+Balls.Dir(Balls.Active,Balls.Step).*sqrt(L.^2./(1+Balls.m(Balls.Active,Balls.Step).^2));
CrashAt(:,:,numel(Walls))=[X,Balls.m(Balls.Active,Balls.Step).*X+Balls.n(Balls.Active,Balls.Step),(numel(Walls)).*ones(numel(X),1)];
else
    CrashAt(:,:,numel(Walls))=[Balls.Dir(Balls.Active,Balls.Step)*nan,X*0,(numel(Walls))*ones(numel(X),1)];
end

[~,ClosestCrashIDX]=min((permute(CrashAt(:,1,:),[1,3,2]).*(Balls.Dir(Balls.Active,Balls.Step)*ones(1,numel(Walls)))),[],2);
IDX=sub2ind(size(CrashAt), [1:numel(Balls.Active)]', ones(numel(Balls.Active),1) ,ClosestCrashIDX);
       Balls.X(Balls.Active,Balls.Step+1)=CrashAt(IDX);
       Balls.Y(Balls.Active,Balls.Step+1)=CrashAt(IDX+numel(Balls.Active));
       Balls.WallNo(Balls.Active,Balls.Step+1)=CrashAt(IDX+2*numel(Balls.Active));
       
       Balls.Theta1(Balls.Active,Balls.Step)=Balls.Theta(Balls.Active,Balls.Step);
       
       Balls.LengthOfFlight(Balls.Active)=Balls.LengthOfFlight(Balls.Active)+...
           sqrt( (Balls.X(Balls.Active,Balls.Step+1)-Balls.X(Balls.Active,Balls.Step)).^2 ...
               + (Balls.Y(Balls.Active,Balls.Step+1)-Balls.Y(Balls.Active,Balls.Step)).^2 );
end