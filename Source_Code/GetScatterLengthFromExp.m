function OutIndex=GetScatterLengthFromExp(AxMat,Lee,X,Y,Factor)
    %This works on first axis. second axis is just more particles
    %if factor is the same direction as the AxMat, this is maximal
    %contribution
OutIndex=sum(...
ones(size(X,1),1)*exprnd(Lee,1,size(X,2))>...
cumsum(real(conj(Factor).*AxMat.ExtractFromMatrix(X,Y))))+1;
OutIndex(OutIndex==(size(X,1)+1))=nan;
end