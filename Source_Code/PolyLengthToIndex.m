function Index=PolyLengthToIndex(Polygon,Length)
    PolyLength=(sqrt(( Polygon(1,1:(end-1))- Polygon(1,2:end)).^2+( Polygon(2,1:(end-1))- Polygon(2,2:end)).^2));
    PolyLengthCum=cumsum( [0,PolyLength(1:end)]);
    PolyTotalLength=sum( PolyLength);

    ModLength=mod(Length,PolyTotalLength);
    ExtraRotations=(Length-ModLength)/PolyTotalLength;
    
% %     MainIndex=find(PolyLengthCum<=ModLength,1,'last');
%     MainIndex=interp1(1:numel(PolyLengthCum),PolyLengthCum,ModLength,'previous');
    MainIndex=interp1(PolyLengthCum,1:numel(PolyLengthCum),ModLength,'previous');
    
    NotNan=~isnan(MainIndex); %%% NEW TO fix the max collisions!!!
    Index=MainIndex*nan;
    
    Index(NotNan)=MainIndex(NotNan)+(ModLength(NotNan)-PolyLengthCum(MainIndex(NotNan))')./PolyLength(MainIndex(NotNan))'+ExtraRotations(NotNan)*(size(Polygon,2)-1);
    
end