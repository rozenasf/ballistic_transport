function [Walls,Balls]=AllowedAnglesPolygon(Walls,Balls)
Polygon=Balls.Polygon;
% AngleList=XYToTheta(Polygon(1,2:end)-Polygon(1,1:(end-1)),Polygon(2,2:end)-Polygon(2,1:(end-1)));
AngleList=PolyAngleFromIndex(Polygon,1:(size(Polygon,2)-1)); %same as line before, more readable
for i=find([Walls.Type]<=2)
    for j=1:2
        Normal=mod(Walls(i).Normal+pi*(j-1),2*pi);
        IndexesAllowed=find(AngDiff(Normal,AngleList')<pi/2);
        
        IndexesAllowed_Extend=[IndexesAllowed,IndexesAllowed+1]';
        KSegmets=reshape(PolyKFromPolyIndex(Polygon,mod(IndexesAllowed_Extend(:)-1,size(Polygon,2)-1)+1),2,numel(IndexesAllowed));
        
        KSegmets=UnWrapPhase(MergeSegmentList(KSegmets));
        
        N=5e3;
        
        X=linspace(-pi/2*(1-1/(10*N)),pi/2*(1-1/(10*N)),N)+Normal;
        X=FilterWithAngleSegments(mod(X,2*pi),KSegmets);
        X=mod(X,2*pi);
        Probability=cos(X-Normal-pi);
        
        CumSumProbability=cumsum(Probability);CumSumProbability=CumSumProbability./CumSumProbability(end);
        %interp1(CumSumProbability,X,rand(1e4,1))
        for k=2:numel(X)
           if(X(k)<X(k-1))
               X(k:end)=X(k:end)+2*pi;
           end
        end
        Walls(i).ScatterAngles{j}=X;
        Walls(i).CumSumProbability{j}=CumSumProbability;
        if(i==4 && j==2);
            1;
        end
    end
end
end