function Angle=PolyAngleFromIndex(Polygon,Index)
    AngleList=XYToTheta(Polygon(1,2:end)-Polygon(1,1:(end-1)),Polygon(2,2:end)-Polygon(2,1:(end-1)));
    Angle=AngleList(floor(Index));
end