function TrajectoryExplorer(Simulation,ParticleList,Length)
    cla
hold on
    Balls=Simulation.Balls;
    N=ParticleList;
    Polygon=Simulation.Parameters.R/Simulation.Parameters.GridSize;
    PolyNumber=size(Polygon,2)-1;

    PolyIndex=Balls.PolyIndex(N,1:Length);
    PolyIndex1=Balls.PolyIndex1(N,1:Length);

    PolyIndex1(PolyIndex1<PolyIndex)=PolyIndex1(PolyIndex1<PolyIndex)+size(Balls.Polygon,2)-1;

    Colors=jet(numel(N));
    for i=1:numel(N)
        for j=1:size(PolyIndex,2)


            PlotIndexList=[PolyIndex(i,j),ceil(PolyIndex(i,j)):floor(PolyIndex1(i,j)),PolyIndex1(i,j)];
            PlotIndexList=mod(PlotIndexList-1,size(Balls.Polygon,2)-1)+1;
            if(Balls.ExtraRotations(N(i),j)>0);PlotIndexList=[1:PolyNumber,PolyNumber+1-1e-9];end
            [X,Y]=PolyIndexToPoint(Simulation.Balls.Polygon,PlotIndexList');

            plot( Scale2Real(X+Balls.Xc(N(i),j),1,Simulation.Parameters,Simulation.Walls),...
                Scale2Real(Y+Balls.Yc(N(i),j),2,Simulation.Parameters,Simulation.Walls),'linewidth',1,...
                'Color',Colors(i,:));
        end
    end
    if(numel(N)==1);ChangePlotColor;end
    hold on
     PlotWalls(Simulation.Walls,Simulation.Parameters)

    Ax=gca;
    Lines=Ax.Children;
    for i=1:numel(Lines)
        Lines(i).XData= Real2Scale(Lines(i).XData,1,Simulation.Parameters,Simulation.Walls);
        Lines(i).YData= Real2Scale(Lines(i).YData,1,Simulation.Parameters,Simulation.Walls);
    end
end
