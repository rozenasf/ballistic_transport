%%
L=15;
N=1e4;
 Rlist=5./([0.25:0.25:5]);
 SimulationN={};SimulationP={};Hall={};Add={};
 for i=1:numel(Rlist)
     SimulationN{i}=[];SimulationP{i}=[];Hall{i}=[];
 end
 %

 for k=1:1
 for i=1:numel(Rlist)
 R=Rlist(i);Colimation=3;Lmr=5;

    k
[SimulationP{i}]=RunSimulation( SimulationP{i} ,struct(...
    ...     Define Walls geometry and behavior
    'WallsCoordinates',struct('Polygons',{[0,0;0,5;L,5;L,0;0,0]'}),...
    'WallsBehavior',struct(...
                              'Rough',{[1,2,3,4],@(Theta,Direction,Rc,Lmr)RoughWalls(Theta,Direction,Rc,Lmr)}...
                          ),...
    ...     How bulk behaves
    'lMR',Lmr,'ThetaScatter',@(n)rand(n,1)*2*pi,...
    ...     balls emitters and Absorbers (wall numbers)
    'N',N,'R',R,'MaxCollisions',100,'Charge',{1,-1},'Absorbers',[1,3],'AbsorbCoeff',1,...
    ...     Emitter
    'Emitter',{1,3},'EmitterRange',[0,1],'EmittedFunciton',{@()asin(rand*2-1)/Colimation,@()asin(rand*2-1)/Colimation+pi},...
    ...     Aaccumulator
    'GridSize',0.2,'FillerNumber',30,'MaxMument',2,'Distribution',360  ... 
    ));

    [SimulationN{i}]=RunSimulation( SimulationN{i} ,struct(...
    ...     Define Walls geometry and behavior
    'WallsCoordinates',struct('Polygons',{[0,0;0,5;L,5;L,0;0,0]'}),...
    'WallsBehavior',struct(...
                              'Rough',{[1,2,3,4],@(Theta,Direction,Rc,Lmr)RoughWalls(Theta,Direction,Rc,Lmr)}...
                          ),...
    ...     How bulk behaves
    'lMR',Lmr,'ThetaScatter',@(n)rand(n,1)*2*pi,...
    ...     balls emitters and Absorbers (wall numbers)
    'N',N,'R',-R,'MaxCollisions',100,'Charge',{1,-1},'Absorbers',[1,3],'AbsorbCoeff',1,...
    ...     Emitter
    'Emitter',{1,3},'EmitterRange',[0,1],'EmittedFunciton',{@()asin(rand*2-1)/Colimation,@()asin(rand*2-1)/Colimation+pi},...
    ...     Aaccumulator
    'GridSize',0.2,'FillerNumber',30,'MaxMument',2,'Distribution',360  ... 
    ));

 end
 clf
 for i=1:numel(Rlist)
     Hall{i}=SubtractSimulations({SimulationN{i},SimulationP{i}});
     Add{i}=AddSimulations({SimulationN{i},SimulationP{i}});
     I=Add{1}.ICurrent.roi(1,5,10).real.mean(1).mean(1);
     HallCut=(-Hall{i}.IDensity.roi(2,4.5,5).mean(2)+Hall{i}.IDensity.roi(2,0,0.5).mean(2));
    plot(HallCut./I./Rlist(i)+i/5); 
    hold on
 end
 end
%%
 clf
 for i=1:numel(Rlist)
     Hall{i}=SubtractSimulations({SimulationN{i},SimulationP{i}});
     Add{i}=AddSimulations({SimulationN{i},SimulationP{i}});
     I=Add{1}.ICurrent.roi(1,5,10).real.mean(1).mean(1);
     HallCut=(-Hall{i}.IDensity.roi(2,4.5,5).mean(2)+Hall{i}.IDensity.roi(2,0,0.5).mean(2));
    plot(HallCut./I./Rlist(i)+i/2); 
    hold on
 end