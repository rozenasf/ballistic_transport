function Walls=CreateWalls(Parameters)
WallsCoordinates=Parameters.WallsCoordinates;
k=1;

Walls=struct('X',{},'Y',{},'P',{},'m',{},'n',{},'Type',{},'Behavior',{});
Walls(1).Min=min([WallsCoordinates.Polygons]');
Walls(1).Max=max([WallsCoordinates.Polygons]');

for i=1:numel(WallsCoordinates)
    for j=1:size(WallsCoordinates(i).Polygons,2)-1
        XReal=[WallsCoordinates(i).Polygons(1,j);WallsCoordinates(i).Polygons(1,j+1)];
        YReal=[WallsCoordinates(i).Polygons(2,j);WallsCoordinates(i).Polygons(2,j+1)];
        X=Real2Scale(XReal,1,Parameters,Walls);Y=Real2Scale(YReal,2,Parameters,Walls);
        Walls(k).Length=sqrt((XReal(1)-XReal(2)).^2+(YReal(1)-YReal(2)).^2);
        
        if(diff(X)==0)
            [~,IDX]=sort(Y);
            Walls(k).Type=1;
            
        elseif(diff(Y)==0)
            Walls(k).Type=2;
            [~,IDX]=sort(X);
            
        else
            Walls(k).Type=0;
            [~,IDX]=sort(X);
            
        end
        X=X(IDX);Y=Y(IDX);
        Walls(k).X=X;
        Walls(k).Y=Y;
%         Walls(k).P=Parameters.P;
        Walls(k).m=diff(Y)/diff(X);
        Walls(k).n=Y(1)-Walls(k).m*X(1);
        switch Walls(k).Type
            case 0
                 AddPi=(1-sign(diff(XReal)))/2;
                Walls(k).Normal=Mod2Pi(-atan(Walls(k).m)+pi/2+pi*AddPi);
            case 1
                 AddPi=(1-sign(diff(YReal)))/2;
                Walls(k).Normal=0+pi*AddPi;
            case 2
                 AddPi=(1-sign(diff(XReal)))/2;
                Walls(k).Normal=pi/2+pi*AddPi;
        end
        if(~ismember(k,Parameters.Absorbers))
            Walls(k).AbsorbCoeff=0;
        else
            if(isfield(Parameters,'AbsorbCoeff'))
                Walls(k).AbsorbCoeff=Parameters.AbsorbCoeff;
            else
                Walls(k).AbsorbCoeff=1;
            end
        end
        
         
        k=k+1;
    end
end

Walls(k).Type=3; %one before last wall is random scaterer
Walls(k).P=1;
        if(~ismember(k,Parameters.Absorbers))
            Walls(k).AbsorbCoeff=0; %to allow them to be captured at the bulk
        else
            Walls(k).AbsorbCoeff=1;
        end
            
% Walls(k).AbsorbCoeff=0;

k=k+1;

Walls(k).Type=3; %last wall is absorbtion scaterer
Walls(k).P=1;
Walls(k).AbsorbCoeff=1;

Walls(1).MinScaled=[min2([Walls.X]),min2([Walls.Y])];
Walls(1).MaxScaled=[max2([Walls.X]),max2([Walls.Y])];


end