%%
L=5;
Viscosity=0.01;
    ParametersArray=struct('P',0,'lMR',8/3,'ThetaScatter',@(n)randn(n,1)*2*pi/8,'HydroScatter',@(Theta,X,Y)Viscosity*sin(Theta).*HydroMask(X+size(HydroMask,1).*(Y-1)), ...
        'MaxCollisions',300,'N',1e4,'R',inf,'MaxMument',2,'Absorbers',[1,3],...
        'Emitter',{1,3},'EmitterRange',[0,1],...
        'EmittedFunciton',{@()asin(rand*2-1),@()asin(rand*2-1)+pi},...
        'GridSize',0.05,'Charge',{1,-1},'ScatterFunction',@(Theta,n)asin(rand(n,1)*2-1), ...
        'FillerNumber',ceil(200/8),'Scramble',0.5,'AbsorbCoeff',1);%@(n)rand(n,1)
    WallsCoordinates={[0,0;0,1;L,1;L,0;0,0]'};
    RunSimulation;
%%
    for i=1:10
        ContinueSimulation;
    end
    %%
    figure(4);
    Smooth=1;
    JX=smooth(Simulation.ICurrent.roi(1,1,4).mean(1).real.data,Smooth);
    JX=(JX+flipud(JX))/2;
    XVec=Simulation.ICurrent.roi(1,1,4).mean(1).axes{1}.data;
    plot(XVec,gradient(JX))
    
    HydroMask=ones(size(Simulation.IDensity,1),1)*gradient(JX)'./sum(JX);
%%
figure();H=uicontrol('Style','togglebutton','String','STOP');
%%
Iteration=0;
while(1)
    Iteration=Iteration+1
    for kkk=1:numel(Simulation)
        UnPackStruct(Simulation{kkk});
        ContinueSimulation;
        
    end
    if(floor(Iteration/10)==Iteration/10)
        save('Q:\users\asafr\simulations\L_5_Lmr_8_ScatterType_Rc_change_3_randn_model_levitov.mat', 'Simulation');
    end
    drawnow();if(H.Value);break;end
end
%%
% jjj=1;
ROIRange=[0.3,0.7]*L;Smooth=1;
%RGB='kmrgb';  
figure(1)
for i=1:numel(Simulation)
    subplot(size(Simulation,2),size(Simulation,1),i)
    Simulation{i}.ICurrent.real.pcolor;
    [Y,X]=meshgrid(ScatterFactor,1./RcList);
    title(sprintf('W/Rc=%0.1f, ScatterFactor %0.1f',X(i),Y(i)));
    colormap(jet(64))
end
figure(3)
for i=1:numel(Simulation)
    subplot(size(Simulation,2),size(Simulation,1),i)
    Simulation{i}.IDensity.real.pcolor;
    [Y,X]=meshgrid(ScatterFactor,1./RcList);
    title(sprintf('W/Rc=%0.1f, ScatterFactor %0.1f',X(i),Y(i)));
    colormap(jet(64))
end
figure(4);
for i=1:numel(Simulation)
    subplot(size(Simulation,2),size(Simulation,1),i)
    Ey=1*Simulation{i}.IDensity.roi(1,ROIRange(1),ROIRange(2)).mean(1);Ey.data=smooth(Ey.data,Smooth);
    %Ey.axes{1}.data=Ey.axes{1}.data-0.5;
    Ey.diff.plot('linewidth',3);
    [Y,X]=meshgrid(ScatterFactor,1./RcList);
    title(sprintf('W/Rc=%0.1f, ScatterFactor %0.1f',X(i),Y(i)));
    colormap(jet(64))
    xlim([0.,1])
    ylim([0,max(Ey.diff.data)])
end
suptitle('Ey')
figure(5);
for i=1:numel(Simulation)
    subplot(size(Simulation,2),size(Simulation,1),i)
    JX=1*Simulation{i}.ICurrent.roi(1,ROIRange(1),ROIRange(2)).mean(1);JX.data=smooth(JX.data,Smooth);
    %Ey.axes{1}.data=Ey.axes{1}.data-0.5;
    JX.real.plot('linewidth',3);
    [Y,X]=meshgrid(ScatterFactor,1./RcList);
    title(sprintf('W/Rc=%0.1f, ScatterFactor %0.1f',X(i),Y(i)));
    colormap(jet(64))
    xlim([0.,1])
    ylim([0,max(JX.real.data)])
end
suptitle('Current')


