function Balls=InitializeBallsNoField(Walls,Parameters)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

N=Parameters.N;MaxCollisions=Parameters.MaxCollisions;

Balls=struct('X',nan(N,MaxCollisions),'Y',nan(N,MaxCollisions),'Theta',...
    nan(N,MaxCollisions),'Theta1',nan(N,MaxCollisions),'m',nan(N,MaxCollisions),'n',nan(N,MaxCollisions),...
    'Dir',nan(N,MaxCollisions),'lMR',nan(N,MaxCollisions),'R',nan(N,MaxCollisions),...
    'WallNo',nan(N,MaxCollisions),'Done',zeros(N,1),'AlphaCharge',nan(N,MaxCollisions));

Balls=InitializeGenericBalls(Walls,Parameters,Balls);
Balls.m(:,1)=-tan(Balls.Theta(:,1));
Balls.n(:,1)=Balls.Y(:,1)-Balls.m(:,1).*Balls.X(:,1);
Balls.Dir(:,1)=(~InRange(Balls.Theta(:,1),pi/2,3/2*pi))*2-1;
Balls.R(:,1)=inf*ones(N,1);
end