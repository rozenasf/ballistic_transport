function [Accumulator,Distribution]=AddToAccumulatorFermiSurface(Balls,Parameters,AccumulatorSize)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

Distribution=[];
Accumulator={};
for i=1:Parameters.MaxMument
    Accumulator{i}=zeros(AccumulatorSize);
end

Distribution=zeros([AccumulatorSize,Parameters.Distribution]);

tic

IDXShiftPositive=Balls.PolyIndex1(:)<Balls.PolyIndex(:);
    Balls.PolyIndex1(IDXShiftPositive)=Balls.PolyIndex1(IDXShiftPositive)+size(Balls.Polygon,2)-1;

tic
if(~isfield(Parameters,'Slice'));Parameters.Slice=2;end
Slice=round(linspace(1,size(Balls.Xc,1)+1,Parameters.Slice));
for k=1:(numel(Slice)-1)
    DistributionTemp=zeros([AccumulatorSize,Parameters.Distribution]);
    RelevantBalls=Slice(k):(Slice(k+1)-1);
    TXc=single(Balls.Xc(RelevantBalls,:))';
    if(prod(isnan(TXc(:)))==1);continue;end
    TYc=single(Balls.Yc(RelevantBalls,:))';

    TI=Balls.PolyIndex(RelevantBalls,:)'   -1;
    TI1=Balls.PolyIndex1(RelevantBalls,:)' -1;
    EX1=Balls.ExtraRotations(RelevantBalls,:)';

    IDX=find(~isnan(TXc)); %FAST FIX, SHOULD CHANGE BACK TO TXc!!!!!1
    
    TXc=TXc(IDX);TYc=TYc(IDX);

    
    TI=TI(IDX);TI1=TI1(IDX);
    EX1=EX1(IDX);
%     if(sum(isnan(TI1))>0);error('HI');end
    
    TL=PolyIndexToLength(Balls.Polygon,TI    +1);
    TL1=PolyIndexToLength(Balls.Polygon,TI1  +1);
    
%     NPoly=size(Balls.Polygon,2)-1;
%     TI1Mod=mod(TI-1,NPoly)+1;
%     TI1Extra=(TI1-TI1Mod)/NPoly;
    
    %TL=Balls.PolyLengthCum(floor(TI))+(TI-floor(TI))*Balls.PolyLength(floor(TI));
    %TL1=Balls.PolyLengthCum(floor(TI1Mod))+(TI1Mod-floor(TI1Mod))*Balls.PolyLength(floor(TI1Mod))+TI1Extra*Balls.PolyTotalLength;
    %
    
    FillerNumbel=Parameters.FillerNumber;
    
    Spacing=ones(numel(IDX),1)*[(0.5/FillerNumbel):(1/FillerNumbel):(1-0.5/FillerNumbel)];
    Spacing=Spacing+(rand(size(Spacing))-0.5)/FillerNumbel;
    
    Rep=ones(1,size(Spacing,2));
    
    TL1=TL1+Balls.PolyLength*EX1;
    
%     PolyIndexes=(TI1*Rep).*Spacing+(TI*Rep).*(1-Spacing);
    PolyLengths=(TL1*Rep).*Spacing+(TL*Rep).*(1-Spacing);
    PolyIndexes=PolyLengthToIndex(Balls.Polygon,PolyLengths(:))-1;
    
    [X,Y]=PolyIndexToPoint(Balls.Polygon,mod(PolyIndexes(:),size(Balls.Polygon,2)-1)+1);
    TXcVec=TXc*Rep;
    TYcVec=TYc*Rep;
    
    ListX=floor(TXcVec(:)+X);
    ListY=floor(TYcVec(:)+Y);
    

    ListT=pi/2+PolyKFromPolyIndex(Balls.Polygon,mod(PolyIndexes(:),size(Balls.Polygon,2)-1)+1);
    %The new Parameters.GridSize addition is to set correctly the normalization
    %exp(-1i*Balls.Kf.*(TP(:,1)-TP1(:,1)).*Rc*ones(1,FillerNumbel).*Spacing).*
    Weight=-((TL(:,1)-TL1(:,1))*ones(1,FillerNumbel)/FillerNumbel.*Parameters.GridSize);
    
    IDX = (ListX(:)) + (ListY(:)-1)*(AccumulatorSize(1));
    Logic=find(((IDX>=1) .* (IDX<=prod(AccumulatorSize))));
    
    if(isfield(Parameters,'Distribution'))
        BarIndexList=floor(mod(-(pi/2-ListT(Logic))/(2*pi),1)*Parameters.Distribution);
        out=accumarray(IDX(Logic)+prod(AccumulatorSize)*BarIndexList(:),Weight(Logic));
        DistributionTemp(1:numel(out))=out;
        Distribution=DistributionTemp+Distribution; %*Parameters.Charge
    end
end
end