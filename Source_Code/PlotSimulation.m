function PlotSimulation(Simulation,FigNumber,ShowDensity)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%
if(isfield(Simulation,'IVoltage'));ShowDensity=0;else;ShowDensity=1;end
if(exist('FigNumber'))
    Fig=figure(FigNumber);
else
    Fig=gcf;
end

Fig1=figure(Fig.Number+1);clf;
Fig1.Visible='off';
Titles= {'Density', 'Current in X direction' ,  'Current in Y direction' , 'Density  (AVG on Y) '  ,'Current in X direction (AVG on Y)', 'Current in Y direction (AVG on Y)', 'Density  (AVG on X) '  ,'Current in X direction (AVG on X)', 'Current in Y direction (AVG on X)'};

for i=1:9
    
    subplot(3,3,i);
    title(Titles(i),'FontSize',7.5);
    if i<7
        xlabel('X direction','FontSize',8);
    else
        xlabel('Y direction','FontSize',8);
    end
    Ax2{i}=gca;
    hold all;
end

figure(Fig.Number)

%Stream_DefLine SteamObj
clf
%contour(Density,30,'LineWidth',3)
if(~ShowDensity)
    e=imagesc(Simulation.IVoltage.axes{1}.data,Simulation.IVoltage.axes{2}.data,Simulation.IVoltage.data');
else
    e=imagesc(Simulation.IDensity.axes{1}.data,Simulation.IDensity.axes{2}.data,Simulation.IDensity.data');
end
e.AlphaData=0.5;
hold on
PlotWalls(Simulation.Walls,Simulation.Parameters)
hold off
set(gca,'YDir','normal')
set(gca,'XDir','normal')
axis equal
ylim([Simulation.Walls(1).Min(2),Simulation.Walls(1).Max(2)]);
xlim([Simulation.Walls(1).Min(1),Simulation.Walls(1).Max(1)]);
colorbar
Ax=gca;
Ax.Position=[0.1,0.1,0.75,0.8];
%  title(['$Current = ',sprintf('%0.1f',Simulation.TotalCurrent*1e6),' \mu A$'],'interpreter','latex');
%
uicontrol('Parent',gcf,'Style','togglebutton','String','Stream Lines','Units','normalized','Position',...
    [0.0 0.9 0.15 0.1],'Visible','on','Callback',@AddStream);
uicontrol('Parent',gcf,'Style','pushbutton','String','Scale Color','Units','normalized','Position',...
    [0.15 0.9 0.15 0.1],'Visible','on','Callback',@ScaleColor);
uicontrol('Parent',gcf,'Style','pushbutton','String','PlotAverage','Units','normalized','Position',...
    [0.3 0.9 0.15 0.1],'Visible','on','Callback',@PlotAverage);
uicontrol('Parent',gcf,'Style','edit','String','Contour','Units','normalized','Position',...
    [0.45 0.9 0.1 0.1],'Visible','on','Callback',@PlotContour);
uicontrol('Parent',gcf,'Style','edit','String','Quiver','Units','normalized','Position',...
    [0.55 0.9 0.1 0.1],'Visible','on','Callback',@PlotQuiver);
uicontrol('Parent',gcf,'Style','pushbutton','String','PlotPolar','Units','normalized','Position',...
    [0.65 0.9 0.15 0.1],'Visible','on','Callback',@PlotPolar);
uicontrol('Parent',gcf,'Style','pushbutton','String','Trajectories','Units','normalized','Position',...
    [0.8 0.9 0.15 0.1],'Visible','on','Callback',@Trajectories);
colormap(jet(256))
TrajectoriesLines={};
drawnow
pause(eps)
if(isfield(Simulation.Parameters,'Distribution') && ~isempty(Simulation.Distribution))
    Fig2=figure(Fig.Number+2);clf;
    Fig2.Visible='off';
    PolarPlotLine=polarplot(Simulation.Distribution.ThetaList/180*pi,Simulation.Distribution.ThetaList*0);
end
Fig3=figure(Fig.Number+3);clf;Fig3.Visible='off';
TrajectroyPlot=[];
figure(Fig);
Stream_DefLine=[];SteamObj=[];ScaleColor_DefBox=[];PlotPolar_DefBox=[];Index=0; LineSet=[];ContourObj=[];QuiverObj=[];
Trajectories_DefBox=[];
    function AddStream(a,b)
        %persistent Stream_DefLine SteamObj
        if(a.Value)
            Stream_DefLine=imline;
            addNewPositionCallback(Stream_DefLine,@(pos) AddStreamLines(pos));
            pos=Stream_DefLine.getPosition;
            AddStreamLines(pos)
        else
            delete(Stream_DefLine);
            delete(SteamObj);
        end
        function AddStreamLines(pos)
            delete(SteamObj);
            t=linspace(0,1,20);
            [y,x]=meshgrid(Simulation.Y,Simulation.X);
            [Stream]=stream2(x',y',real(Simulation.Accumulator{2})',imag(Simulation.Accumulator{2})',pos(1,1)+diff(pos(:,1))*t,pos(1,2)+diff(pos(:,2))*t);
            [Stream2]=stream2(x',y',-real(Simulation.Accumulator{2})',-imag(Simulation.Accumulator{2})',pos(1,1)+diff(pos(:,1))*t,pos(1,2)+diff(pos(:,2))*t);
            SteamObj=streamline([Stream,Stream2]);
        end
    end
    function Trajectories(a,b)
        
        
        Fig3.Visible='on';figure(Fig);
        %global ScaleColor_DefBox
        delete(Trajectories_DefBox);
        Trajectories_DefBox=imrect;
        addNewPositionCallback(Trajectories_DefBox,@(pos) TrajectoriesOperate(pos));
        pos=Trajectories_DefBox.getPosition;
        
        TrajectoriesOperate(pos)
        function TrajectoriesOperate(pos)
            
            
            TargetNumberOfLines=50;
            OrderTrajectories=1;

            RangeX=[pos(1),pos(1)+pos(3)];RangeY=[pos(2),pos(2)+pos(4)];
            XIDX=find((Simulation.X<RangeX(2) & Simulation.X>RangeX(1) ));
            YIDX=find((Simulation.Y<RangeY(2) & Simulation.Y>RangeY(1) ));
            Charge=Simulation.Transport.Charge(1,1);Distribution={};
            if(size(Simulation.Transport.Charge,2)==1)
                Distribution{1}=squeeze(mean(mean((Simulation.Distribution.Data(XIDX,YIDX,:)),1),2));
                 W=[1,0];
                W_NumberOfLines=1;
                RB='k';
            elseif (size(Simulation.Transport.Charge,2)==2)
                Data=squeeze(mean(mean((Simulation.Distribution.Data(XIDX,YIDX,:)),1),2));
                Counter=squeeze(mean(mean((Simulation.Distribution.Counter(XIDX,YIDX,:)),1),2));
                Distribution{1}=(Charge*Counter+Data)/2;
                Distribution{2}=(Charge*Counter-Data)/2;
                W_NumberOfLines=[sum(Distribution{1}),sum(Distribution{2})]/(sum(Distribution{1})+sum(Distribution{2}));
                RB='rb';
            else
                error('only 2 types of charges allowed!');
            end
            
            if(~isempty(TrajectoriesLines))
                for i=1:numel( TrajectoriesLines)
                    delete(TrajectoriesLines{i});
                end
                TrajectoriesLines={};
            end
            TrajectoriesLines={};
            for Type=1:numel(Distribution)
                [~,MaxIDX]=max(Distribution{Type});
                ThetaList=[1:360];
                Distribution{Type}=Distribution{Type}( mod(MaxIDX+[1:360]-1,360)+1);
                ThetaList=mod(MaxIDX+[1:360]-1,360)+1;
                CumDistribution=[cumsum(Distribution{Type})];

                CumDistribution=CumDistribution./CumDistribution(end);
                NumberOfLines=round(TargetNumberOfLines*W_NumberOfLines(Type));
                if(NumberOfLines<1);continue;end
                
                ProbVector=linspace(0,1,NumberOfLines);
                ProbVector=(ProbVector(1:end-1)+ProbVector(2:end))/2;
                
                List=[];
                for i=1:numel(ProbVector)
                    Target=ProbVector(i);
                    IDX=find(diff(CumDistribution>Target));
                    a=CumDistribution(IDX);b=CumDistribution(IDX+1);
                    W=(Target-a)./(b-a);
                    if(isempty(IDX));continue;end
                    List(end+1)=W*ThetaList(IDX)+(1-W)*ThetaList(IDX+1);
                    
                end
                ToAdd=0;
                if(numel(List)<4);ToAdd=4-numel(List);List=[List,ones(1,ToAdd)*List(end)];end

                Emitter=struct('X',ones(1,numel(List))*mean(RangeX),'Y',ones(1,numel(List))*mean(RangeY),'Theta',List/180*pi+pi);
                
                Parameters2=Simulation.Parameters;
                Parameters2.N=numel(Emitter.X);
                Parameters2.GridSize=10;
                Parameters2.R=-Parameters2.R;
                Parameters2.FillerNumber=0;
                Parameters2.CleanBalls=0;
                Parameters2.Emitter=Emitter;
                
                Simulation2=RunSimulation( [] ,Parameters2);
                
                NPhi=50;
                R=abs(Simulation2.Parameters.R/Simulation2.Parameters.GridSize);
                Balls=Simulation2.Balls;
                
                LogicDir=(Balls.Dir(:)==1);
                IDXShiftPositive=Balls.Phi1(:)>Balls.Phi(:);
                Balls.Phi1(LogicDir & IDXShiftPositive)=Balls.Phi1(LogicDir & IDXShiftPositive)-2*pi;
                Balls.Phi1(~LogicDir & ~IDXShiftPositive)=Balls.Phi1(~LogicDir & ~IDXShiftPositive)+2*pi;
                
                for i=1:(Simulation2.Parameters.N)-ToAdd
                    
                    Relevant=~isnan(Balls.Phi1(i,:));
                    Phi1=Balls.Phi1(i,Relevant);
                    Phi=Balls.Phi(i,Relevant);
                    if(Balls.ExtraRotations(i,1)>0)
                        %continue;
                        Phi1=0;Phi=2*pi;
                    end
                    Xc=Balls.Xc(i,Relevant);
                    Yc=Balls.Yc(i,Relevant);
                    for j=1:min([numel(Xc),OrderTrajectories])%numel(Xc)
                        PhiList=linspace(Phi1(j),Phi(j),NPhi);
                        Y=-R*sin(PhiList)+Yc(j);
                        X= R*cos(PhiList)+Xc(j);
                        X=Scale2Real(X,1,Simulation2.Parameters,Simulation2.Walls);
                        Y=Scale2Real(Y,2,Simulation2.Parameters,Simulation2.Walls);
                        hold on;TrajectoriesLines{end+1}=plot(X,Y,RB(Type),'linewidth',1);
                    end
                end
                
            end
            
        end
    end
    function PlotPolar(a,b)
        Fig2.Visible='on';figure(Fig);
        %global ScaleColor_DefBox
        delete(PlotPolar_DefBox);
        PlotPolar_DefBox=imrect;
        addNewPositionCallback(PlotPolar_DefBox,@(pos) PlotPolarOperate(pos));
        pos=PlotPolar_DefBox.getPosition;
        
        PlotPolarOperate(pos)
        function PlotPolarOperate(pos)
            
            Xlist=find((Simulation.X<pos(1)+pos(3)) .* (Simulation.X>pos(1)));
            Ylist=find((Simulation.Y<pos(2)+pos(4)) .* (Simulation.Y>pos(2)));
            V=mean(mean(Simulation.Distribution.Data(Xlist,Ylist,:))); %maybe transpose here...
            PolarPlotLine.RData=V;
        end
    end
    function ScaleColor(a,b)
        %global ScaleColor_DefBox
        delete(ScaleColor_DefBox);
        ScaleColor_DefBox=imrect;
        addNewPositionCallback(ScaleColor_DefBox,@(pos) ScaleColorOperate(pos));
        pos=ScaleColor_DefBox.getPosition;
        
        ScaleColorOperate(pos)
        function ScaleColorOperate(pos)
            
            Xlist=find((Simulation.X<pos(1)+pos(3)) .* (Simulation.X>pos(1)));
            Ylist=find((Simulation.Y<pos(2)+pos(4)) .* (Simulation.Y>pos(2)));
            [X,Y]=meshgrid(Xlist,Ylist);
            if(~ShowDensity)
                Ma=max2(Simulation.IVoltage.data(X(:),Y(:)));
                Mi=min2(Simulation.IVoltage.data(X(:),Y(:)));
            else
                Ma=max2(Simulation.IDensity.data(X(:),Y(:)));
                Mi=min2(Simulation.IDensity.data(X(:),Y(:)));
            end
            caxis([Mi,Ma]);
        end
    end
    function PlotAverage(a,b)
        Fig1.Visible='on';figure(Fig);
        %persistent Index LineSet
        %if(isempty(Index));Index=0;end
        Index=Index+1;
        %delete(PlotAverage_DefBox);
        PlotAverage_DefBox=imrect;
        addNewPositionCallback(PlotAverage_DefBox,@(pos) PlotAverageOperate(pos,Index));
        set(PlotAverage_DefBox,'delete',@(a,b)GeneralDelete(a,b,Index));
        pos=PlotAverage_DefBox.getPosition;
        PlotAverageOperate(pos,Index)
        function GeneralDelete(a,b,Index)
            try for eee=1:9;delete(LineSet{Index,eee});end;end
        end
        function PlotAverageOperate(pos,Index)
            
            Xlist=find((Simulation.X<pos(1)+pos(3)) .* (Simulation.X>pos(1)));
            Ylist=find((Simulation.Y<pos(2)+pos(4)) .* (Simulation.Y>pos(2)));
            [X,Y]=meshgrid(Xlist,Ylist);
            try for eee=1:9;delete(LineSet{Index,eee});end;end
            if(~ShowDensity)
                Voltage=Simulation.IVoltage.data;
            else
                Voltage=Simulation.IDensity.data;
            end
            %             LineSet{Index,1}=plot(Ax2{1},Simulation.X(Xlist),Simulation.Accumulator{1}(Xlist,Ylist));
            LineSet{Index,1}=plot(Ax2{1},Simulation.X(Xlist),Voltage(Xlist,Ylist));
            
            LineSet{Index,2}=plot(Ax2{2},Simulation.X(Xlist),real(Simulation.Accumulator{2}(Xlist,Ylist)));
            LineSet{Index,3}=plot(Ax2{3},Simulation.X(Xlist),imag(Simulation.Accumulator{2}(Xlist,Ylist)));
            
            %             LineSet{Index,4}=plot(Ax2{4},Simulation.X(Xlist),mean(Simulation.Accumulator{1}(Xlist,Ylist),2));
            LineSet{Index,4}=plot(Ax2{4},Simulation.X(Xlist),mean(Voltage(Xlist,Ylist),2));
            
            LineSet{Index,5}=plot(Ax2{5},Simulation.X(Xlist),mean(real(Simulation.Accumulator{2}(Xlist,Ylist)),2));
            LineSet{Index,6}=plot(Ax2{6},Simulation.X(Xlist),mean(imag(Simulation.Accumulator{2}(Xlist,Ylist)),2));
            
            %             LineSet{Index,7}=plot(Ax2{7},Simulation.Y(Ylist),mean(Simulation.Accumulator{1}(Xlist,Ylist),1));
            LineSet{Index,7}=plot(Ax2{7},Simulation.Y(Ylist),mean(Voltage(Xlist,Ylist),1));
            LineSet{Index,8}=plot(Ax2{8},Simulation.Y(Ylist),mean(real(Simulation.Accumulator{2}(Xlist,Ylist)),1));
            LineSet{Index,9}=plot(Ax2{9},Simulation.Y(Ylist),mean(imag(Simulation.Accumulator{2}(Xlist,Ylist)),1));
            
        end
    end
    function PlotContour(a,b)
        %persistent ContourObj
        delete(ContourObj);
        if(str2num(a.String)>0)
            hold on
            [~,ContourObj]=contour(Simulation.X,Simulation.Y,Simulation.IDensity.data',str2num(a.String));
            ContourObj.LineWidth=3;
            hold off
        end
    end
    function PlotQuiver(a,b)
        %persistent QuiverObj
        delete(QuiverObj);
        if(str2num(a.String)>0)
            hold on
            QuiverObj=quiver(Simulation.X,Simulation.Y,real(Simulation.Accumulator{2}'),-imag(Simulation.Accumulator{2}'),str2num(a.String));
            hold off
        end
        % quiver(y,x,real(ICurrent.data),imag(ICurrent.data),2)
    end
end