function Balls=SetStepBack(Balls)
    Names=fieldnames(Balls);
    for i=1:numel(Names)
       if(size(Balls.(Names{i}),2)==Balls.Step+1)
          Balls.(Names{i})(:,1)=Balls.(Names{i})(:,Balls.Step);
          Balls.(Names{i})(:,2:end)=Balls.(Names{i})(:,2:end)*nan;
       end
    end
    Balls.Step=1;
end