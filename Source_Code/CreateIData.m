function out=CreateIData(data,varargin)
            Ax={};
            for i=1:numel(varargin)/2
               Ax{i}=iaxis(varargin{2*i-1},varargin{2*i});
            end
            out=idata('TempIData',data,Ax,{0},' ',[]);
end