% Integral calculation

    R=1;

    W=2.5*R;
    
    y_vec=[0:W/1000:W]; max_y=y_vec(end);
    V=zeros(length(y_vec),1);
    for i=1:numel(y_vec)

        y1=y_vec(i); 
     y2=max_y-y1;
       Rho1=@(theta)(exp.^(sqrt(1-(cos(theta)-y1).^2));
      

   V(i)=real(integral(Rho1,0,pi));
   
    end
    
    V=V+flip(V);
    figure;
    plot(y_vec,V,'.'); title('Density   plot');
    
   % Electric field calculation
    
    E=abs(gradient(V));
   figure; plot(y_vec,E,'.'); title('E_y   plot');
    
    
   