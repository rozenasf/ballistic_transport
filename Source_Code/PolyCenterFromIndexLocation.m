function [Xc,Yc]=PolyCenterFromIndexLocation(Polygon,X,Y,PolyIndex)
    [Ex,Ey]=PolyIndexToPoint(Polygon,PolyIndex);
    Xc=X-Ex;
    Yc=Y-Ey;
end