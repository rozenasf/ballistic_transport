function out=AddSimulations_no_distrib(varargin)

N = numel(varargin);
out=varargin{1};
for i=2:N
    out.IDensity=out.IDensity+varargin{i}.IDensity;
    out.ICurrent=out.ICurrent+varargin{i}.ICurrent;
    if(isfield(out,'IVoltage'))
        out.IVoltage=out.IVoltage+varargin{i}.IVoltage;
    end
    %         if(~isempty(out.Distribution))
    %             out.Distribution.Data=out.Distribution.Data+varargin{i}.Distribution.Data;
    %         end
    out.TotalCurrent=out.TotalCurrent+varargin{i}.TotalCurrent;
    for j=1:numel(out.Accumulator)
        out.Accumulator{j}=out.Accumulator{j}+varargin{i}.Accumulator{j};
    end
end

out.IDensity = out.IDensity/N;
out.ICurrent = out.ICurrent/N;
out.TotalCurrent = out.TotalCurrent/N;

end

