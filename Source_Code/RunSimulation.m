function [Simulation]=RunSimulation(Simulation,Parameters)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%
% global Balls
if(numel(Parameters)>1)
    for i=1:numel(Parameters)
        %if it is a parameter array, loop over it
        Simulation=RunSimulation(Simulation,Parameters(i));
    end
else

    %Fill up empty parameters
    if(~isfield(Parameters,'MaxMument'));Parameters.MaxMument=0;end
    if(~isfield(Parameters,'ThetaScatter'));Parameters.ThetaScatter=@(n)rand(n,1)*2*pi;end
    if(~isfield(Parameters,'MaxCollisions'));Parameters.MaxCollisions=50;end
    if(~isfield(Parameters,'AbsorbCoeff'));Parameters.AbsorbCoeff=1;end
    if(~isfield(Parameters,'EmitterRange'));Parameters.EmitterRange=[0,1];end
    if(~isfield(Parameters,'Distribution'));Parameters.Distribution=360;end
    if(~isfield(Parameters,'Slice'));Parameters.Slice=3;end
    if(~isfield(Parameters,'n_res'));Parameters.n_res=1;end
    if(~isfield(Parameters,'R'));Parameters.R=inf;end
    if(~isfield(Parameters,'lMR'));Parameters.lMR=inf;end
    if(~isfield(Parameters,'Seed'));rng('shuffle');else rng(Parameters.Seed);end
    if(~isfield(Parameters,'Flip'));Parameters.Flip=0;end
    if(~isfield(Parameters,'EmittedFunciton'));
        if(isfield(Parameters,'Collimation'))
            X=linspace(-pi/2,pi/2,1000);
            Parameters.EmittedFunciton=[X+pi*Parameters.Flip;exp(-X.^2/(2*( Parameters.Collimation /2.3548)^2)).*cos(X)];
        else
            if(numel(Parameters.R)==1)
                Parameters.EmittedFunciton=@(n,X,Y)asin(rand(n,1)*2-1)+PerpendicularAngle(X,Y)+pi*Parameters.Flip;
            else
                Parameters.EmittedFunciton=@(n,X,Y)RandomContactLeakProtected(n,X,Y,Parameters.R,Parameters.Flip);
            end
        end
    end

    
if(~isfield(Parameters,'CleanBalls'));Parameters.CleanBalls=0;end

    if(isempty(Simulation)) %initialize an empty simulation
        
        Simulation=[];
        Simulation.Aux.Counter=0;
        Simulation.Walls=CreateWalls(Parameters);
        Simulation.Accumulator={};
        Simulation.Size=[ceil(Simulation.Walls(1).MaxScaled(1)-Simulation.Walls(1).MinScaled(1)),...
            ceil(Simulation.Walls(1).MaxScaled(2)-Simulation.Walls(1).MinScaled(2))];
        Simulation.Parameters=Parameters;
        
        for i=1:Parameters.MaxMument
            Simulation.Accumulator{i}=zeros(Simulation.Size);
        end
        if(isfield(Parameters,'Distribution') && Parameters.FillerNumber~=0)
            Simulation.Distribution.Counter=zeros([Simulation.Size,Parameters.Distribution]);
            Simulation.Distribution.Data   =zeros([Simulation.Size,Parameters.Distribution]);
            Simulation.Distribution.ThetaList=((1:Parameters.Distribution)-0.5)*360/Parameters.Distribution;
        end
    end
    Simulation.Tic.TotalTic=tic;
    %% Generate Balls
    Magnetic=~isinf(sum(sum(Parameters.R)));
    if(Magnetic)
        if(numel(Parameters.R)==1)
            Balls=InitializeBallsWithField(Simulation.Walls,Parameters);
        else
            Balls=InitializeBallsFermiSurface(Simulation.Walls,Parameters);
            [Simulation.Walls,Balls]=AllowedAnglesPolygon(Simulation.Walls,Balls);
        end
    else
        Balls=InitializeBallsNoField(Simulation.Walls,Parameters);
    end
    if(~isstruct(Parameters.Emitter))
        EmiterWidthNormalization=Simulation.Walls(Parameters.Emitter).Length;
    else
        EmiterWidthNormalization=1;
    end
    %% --- Main Loop ---
    
     Done=0;
     Simulation.Tic=struct('CalculationToc',0,'AccumToc',0,'TotalTime',tic);
     TotalIterations=0;
     while(~Done)
         Simulation.Tic.CalculationTic=tic;
        for i=1:Parameters.MaxCollisions-2
            if(Magnetic)
                 if(numel(Parameters.R)==1)
                    Balls=CalculateNextCollisionWithField(Balls,Simulation.Walls);
                 else
                    Balls=CalculateNextCollisionFermiSurface(Balls,Simulation.Walls);
                 end
            else
                Balls=CalculateNextCollisionNoField(Balls,Simulation.Walls);
            end
 
            %if(sum(isnan(Balls.WallNo(Balls.Active,Balls.Step+1)))>0);continue;end % patch to allow the leakage not to affect to overnight calculation
            [Done,Balls]=ScatterV2(Balls,Simulation.Walls,Parameters);
            
            if(Done);break;end %all balls are collected
            if(isfield(Parameters,'MaxIterations') && TotalIterations+i>Parameters.MaxIterations);Done=1;break;end
            Balls=UpdateBallsMagneticSpecificParameters(Balls,Parameters);

            Balls.Step=Balls.Step+1;
        end
                Simulation.Balls=Balls;
        
         
         
        Simulation.Tic.CalculationToc=Simulation.Tic.CalculationToc+toc(Simulation.Tic.CalculationTic);
        IterationsNeeded=i;
        TotalIterations=TotalIterations+IterationsNeeded;
        %% Calculate Accumulator

         qe_=1.6022e-19;h_=6.6261e-34;Vf=1e6;
        

        if(isfield(Parameters,'Vsd'));
            Quantum_Conductance=2*qe_^2/h_;Kf=sqrt(pi*Parameters.n);
            Simulation.Vsd=Parameters.Vsd;
            Simulation.n_res=Parameters.Vsd     *...
                             Quantum_Conductance*...
                             2                  *...
                             Kf                 *...
                             1/qe_              *...
                             1/Vf               *...
                             1e-12              ;

            Parameters.Charge = 1/pi            *... %convert to n_res
                EmiterWidthNormalization        *... %convert to n_res
                1/Parameters.N                  *... %convert to n_res
                Simulation.n_res;
        elseif(isfield(Parameters,'n_res')) %units of 1/um^2
                Simulation.n_res=Parameters.n_res;
                Parameters.Charge = 1/pi        *...
                EmiterWidthNormalization        *...
                1/Parameters.N                  *...
                Simulation.n_res;
        end

        Simulation.Tic.AccumTic=tic;
        if(Parameters.FillerNumber~=0)
        
        AccumulatorSize=Simulation.Size;


        if (Magnetic)
            if(numel(Parameters.R)==1)
                [NewAccumulator,DistributionCounter]=AddToAccumulatorWithField(Balls,Parameters,AccumulatorSize);
            else
                [NewAccumulator,DistributionCounter]=AddToAccumulatorFermiSurface(Balls,Parameters,AccumulatorSize);
            end
        else
            [NewAccumulator,DistributionCounter]=AddToAccumulatorNoField(Balls,Parameters,AccumulatorSize);
        end
        1;
    %     Distribution
        % Add to existing Accumulator , shouldn't be used
        for i=1:Parameters.MaxMument
            Simulation.Accumulator{i}=Simulation.Accumulator{i}+NewAccumulator{i};
        end

        if(isfield(Parameters,'Distribution'))
            Simulation.Distribution.Counter=...
                Simulation.Distribution.Counter + DistributionCounter./Parameters.GridSize^2;%.*EmiterWidthNormalization
            Simulation.Distribution.Data   =...
                Simulation.Distribution.Data    + DistributionCounter./Parameters.GridSize^2.*Parameters.Charge;%.*EmiterWidthNormalization
        end
        end
        
        Simulation.Tic.AccumToc=Simulation.Tic.AccumToc+toc(Simulation.Tic.AccumTic);
        Balls=SetStepBack(Balls);
     end
     if(Parameters.CleanBalls)
         Balls=CleanUpBalls(Balls);
     else
         Simulation.Balls=Balls;
     end
    
    
   %Calculate the first and second moments from distribution
   if(Parameters.FillerNumber~=0)
    if( Parameters.MaxMument<2)
        for i=1:2
            Simulation.Accumulator{i}=squeeze(sum(bsxfun(@times, Simulation.Distribution.Data,...
                reshape(exp(-1i*Simulation.Distribution.ThetaList/180*pi*(i-1)), 1, 1,...
                size(Simulation.Distribution.ThetaList,2))), 3));
        end
    end
    %Create Idata objects
    Counter=sum(Simulation.Distribution.Counter,3)';
    [IDensity,ICurrent,ICounter,X,Y]=GetIdata(Simulation.Accumulator,Counter,Parameters,Simulation.Walls);
    Simulation.IDensity=IDensity;
    Simulation.ICurrent=ICurrent;
    Simulation.ICounter=ICounter;
    Simulation.X=X;Simulation.Y=Y;
   end
    % print output
    Simulation.Tic.TotalTime=toc(Simulation.Tic.TotalTime);
    if(IterationsNeeded==(Parameters.MaxCollisions-2))
        fprintf('Increase number of max collisions\n');
    elseif ~isfield(Parameters, 'quiet')
        fprintf('needed %d iterations, Trajectory %0.2f [s], Ram %0.2f [s], Total %0.2f [s]\n',TotalIterations,Simulation.Tic.CalculationToc,Simulation.Tic.AccumToc,Simulation.Tic.TotalTime);
    end
    Simulation.IterationsNeeded = IterationsNeeded;

    if(isfield(Balls,'HydroEvents'))
        Simulation.HydroEvents=Balls.HydroEvents;
        nanHydro=~isnan(Simulation.HydroEvents.X);
        Simulation.HydroEvents.X=Simulation.HydroEvents.X(nanHydro);
        Simulation.HydroEvents.Y=Simulation.HydroEvents.Y(nanHydro);
        Simulation.HydroEvents.Theta=Simulation.HydroEvents.Theta(nanHydro);
    end
    if(~isfield(Simulation,'Transport'))
        Simulation.Transport=struct('StartWall',[],'EndWall',[],'LengthOfFlight',[],'Charge',[],'Transmission',[],'WallLength',[],'TotalCurrent',[]);
    end
    
       
    Simulation.Transport.StartWall     (:,end+1) = Balls.InitialWalls;
    Simulation.Transport.EndWall       (:,end+1) = Balls.FinalWall;
    Simulation.Transport.LengthOfFlight(:,end+1) = Balls.LengthOfFlight;
    Simulation.Transport.Charge        (:,end+1) = Parameters.Charge*ones(size(Balls.WallNo,1),1);
    Simulation.Transport.Transmission  (end+1)   = mean(Simulation.Transport.StartWall(:,end)~=Simulation.Transport.EndWall(:,end));
    Simulation.Transport.WallLength    (end+1)   = EmiterWidthNormalization;
    Simulation.Transport.TotalCurrent  (end+1)   = Parameters.Charge                                                        *...
                                                   mean(Simulation.Transport.StartWall(:,end)~=Simulation.Transport.EndWall(:,end)) *...
                                                   ((Parameters.Absorbers(1)==Simulation.Transport.StartWall(1,end))*2-1)         *... WIERD, probebly problem from changing to working with resuvioar
                                                   Parameters.N;

    if(Parameters.FillerNumber~=0)
        % Convert to measurables output
        Simulation.TotalCurrent =     mean(Simulation.Transport.TotalCurrent);
        Simulation.IDensity.data     = Simulation.IDensity.data/length(Simulation.Transport.Transmission);
        Simulation.ICurrent.data     = Simulation.ICurrent.data/length(Simulation.Transport.Transmission);
    end
    
    EndWallAcumulate=zeros(size(Simulation.Walls));
    EndWalls=accumarray(Simulation.Transport.EndWall,1);
    EndWallAcumulate(1:numel(EndWalls))=EndWalls;
    Simulation.TransmissionProbablity=EndWallAcumulate(Simulation.Parameters.Absorbers)/sum(EndWallAcumulate);

    if(isfield(Parameters,'Vsd'))
     Simulation.IVoltage=Simulation.IDensity    *...
                        1/Quantum_Conductance   *...
                        1/2                     *...
                        1/Kf                    *...
                        qe_                     *...
                        Vf                      *...
                        1e12                    ;
      Simulation.ICurrent= Simulation.ICurrent  *...
                           qe_                  *...
                           1e12                 ;
      Simulation.TotalCurrent=Simulation.TotalCurrent *...
                           qe_                        *...
                           1e12                       ;
    end
    

    
%     Simulation.ICurrent=0.5* Simulation.ICurrent .* (Parameters.Vsd/2) .*Quantum_Conductance.*Kf./...
%         (Parameters.N)/...
%         (numel(Simulation.Transport.Transmission)/2)./...
%         Parameters.GridSize;
    
    if(Parameters.MaxMument~=0)
        warning('It is strongly recomended to set MaxMument to 0!');
    end
%     Simulation.Distribution=[];
end

end
%%
