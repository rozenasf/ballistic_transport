function Balls=UpdateBallsMagneticSpecificParameters(Balls,Parameters)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%
    if(numel(Parameters.R)==1)
        if(~isinf(Parameters.R))
            Balls.Phi(:,Balls.Step+1)=mod(Balls.Theta(:,Balls.Step+1)+pi/2*Balls.Dir(:,Balls.Step+1),2*pi);
            Balls.Xc(:,Balls.Step+1)=Balls.X(:,Balls.Step+1)+Balls.R(:,Balls.Step+1).*sin(Balls.Theta(:,Balls.Step+1)).*Balls.Dir(:,Balls.Step+1);
            Balls.Yc(:,Balls.Step+1)=Balls.Y(:,Balls.Step+1)+Balls.R(:,Balls.Step+1).*cos(Balls.Theta(:,Balls.Step+1)).*Balls.Dir(:,Balls.Step+1);
        else
            Balls.m(Balls.Active,Balls.Step+1)=-tan(Balls.Theta(Balls.Active,Balls.Step+1));
            Balls.n(Balls.Active,Balls.Step+1)=Balls.Y(Balls.Active,Balls.Step+1)-Balls.m(Balls.Active,Balls.Step+1).*Balls.X(Balls.Active,Balls.Step+1);
            Balls.Dir(Balls.Active,Balls.Step+1)=(~InRange(Balls.Theta(Balls.Active,Balls.Step+1),pi/2,3/2*pi))*2-1;
        end
    else
        Balls.PolyIndex(:,Balls.Step+1)=PolyIndexFromPolyK(Balls.Polygon,Balls.Theta(:,Balls.Step+1));
        [Xc,Yc]=PolyCenterFromIndexLocation(Balls.Polygon,Balls.X(:,Balls.Step+1),Balls.Y(:,Balls.Step+1),Balls.PolyIndex(:,Balls.Step+1));
        Balls.Xc(:,Balls.Step+1)=Xc;
        Balls.Yc(:,Balls.Step+1)=Yc;
    end
end