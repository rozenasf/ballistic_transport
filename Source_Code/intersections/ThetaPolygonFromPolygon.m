function ThetaPolygon=ThetaPolygonFromPolygon(Polygon)
    ThetaPolygon=mod((atan(-Polygon(2,1:end-1)./Polygon(1,1:end-1))+pi*(Polygon(1,1:end-1)<0)),2*pi);
end