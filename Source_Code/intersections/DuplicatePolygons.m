function FullPolygons=DuplicatePolygons(Polygon,Centers)
[Xt1,Xt2]=meshgrid([Polygon(1,:),nan],Centers(1,:));
[Yt1,Yt2]=meshgrid([Polygon(2,:),nan],Centers(2,:));
X=Xt1'+Xt2';Y=Yt1'+Yt2';
FullPolygons=[X(:)';Y(:)'];
end