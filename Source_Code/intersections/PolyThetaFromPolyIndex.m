function [PolyTheta,PolyK]=PolyThetaFromPolyIndex(Polygon,PolyIndex)
    [X,Y]=PolyIndexToPoint(Polygon,PolyIndex);
    PolyTheta=(atan(-Y./X)+pi*(X<0));
    PolyK=mod(PolyTheta+pi/2,2*pi);
end