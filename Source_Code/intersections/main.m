%% Theta Fermi Surface
%KThetaPolygonBase=[0,pi/6-0.04,pi/6+0.04,pi/3;sqrt(3)/2,1,1,sqrt(3)/2]; %interesting hexagon
 KThetaPolygonBase=[0,pi/6;0.1,0.1]; %regular hexagon
Symmetry=6;
KThetaPolygon=[];
for i=1:Symmetry+1
    KThetaPolygon=[KThetaPolygon, [ KThetaPolygonBase(:,(1:end-1)) ]+repmat([1;0]*(i-1)*2*pi/Symmetry,1,size(KThetaPolygonBase,2)-1) ];
end
clf
polarplot(KThetaPolygon(1,:),KThetaPolygon(2,:));
%% from polar to cartezian
KPolygon=[sin(KThetaPolygon(1,:)).*KThetaPolygon(2,:);cos(KThetaPolygon(1,:)).*KThetaPolygon(2,:)];
XPolygon=[KPolygon(2,:);-KPolygon(1,:)];

XPolygon=RotatePolygon(XPolygon,0.0);
ThetaPolygon=ThetaPolygonFromPolygon(Polygon);

plot(XPolygon(1,:),XPolygon(2,:))
%% PolygonIndexToCoordinates

Polygon=XPolygon;Centers=[0,0];

[xp,yp,xi,yi]=intersections(Polygon(1,:),Polygon(2,:),[-1,1],[0.4,0.4]); 
Points=PolyIndexToPoint(Polygon,xi);

clf
plot(XPolygon(1,:),XPolygon(2,:))
hold on
plot(Points(1,:),Points(2,:),'o')
%%
N=1;
clf
Walls=struct('X',{0,1,1,0,0},'Y',{0,0,1,1,0});
Centers=[0.95;1.02];
plot([Walls.X],[Walls.Y]);
hold on


FullPolygons=DuplicatePolygons(Polygon,Centers);
[PolyNumber,PolyIndex,WallNumber,X,Y]=GetPolyTrajectroyIntersetions(FullPolygons,Walls,size(Polygon,2));


% PolyTheta=ThetaPolygon(floor(PolyIndex))/pi*180

PlotPolygon(FullPolygons)
 plot(X,Y,'o')
 %%
[PolyTheta,PolyK]=PolyThetaFromPolyIndex(Polygon,PolyIndex);

% axis equal
%%

%%
X1=[0,1,1,0,0;0,0,1,1,0];
X1=X1-mean(X1);
X2=obj.RotatePolygon90(X1);
clf;
plot(X1(1,:),X1(2,:));
hold on;
plot(X2(1,:),X2(2,:));
