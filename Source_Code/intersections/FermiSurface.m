classdef FermiSurface
   properties
       KPolygon
       XPolygon
   end
   methods
       function obj=FermiSurface(KPolygon)
           obj.KPolygon=KPolygon;
       end
       function Polyout=RotatePolygon90(obj,Polyin)
           Polyout=[0,1;-1,0]*Polyin;
       end
   end
end