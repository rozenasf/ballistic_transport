N=1e1;
P=6;
x1=nan(N*(P+2),1);
y1=nan(N*(P+2),1);

x2=[0,1,1,0,0];y2=[0,0,1,1,0];
R=0.1;
for i=1:N
   x1( ((i-1)*(P+2)+1):i*(P+2) ) = [rand(1) + R*sin(linspace(0,2*pi,P+1)),nan];
   y1( ((i-1)*(P+2)+1):i*(P+2) ) = [rand(1) + R*cos(linspace(0,2*pi,P+1)),nan];
end
clf;
plot(x1,y1)
hold on
plot(x2,y2)
tic
[xp,yp,xi,yi]=intersections(x1,y1,x2,y2);
toc
plot(xp,yp,'o')
axis equal
%%
Walls=[0,1,1,0,0;0,0,1,1,0];
R=0.2;P=6;
TrajectoryBasic=[R*sin(linspace(0,2*pi,P)),nan;R*cos(linspace(0,2*pi,P)),nan];

CenterVec=rand(2,1e5);
 tic
[X1,X2]=meshgrid(CenterVec(1,:),TrajectoryBasic(1,:));
[Y1,Y2]=meshgrid(CenterVec(2,:),TrajectoryBasic(2,:));
FullTrajectories=[X1(:)+X2(:),Y1(:)+Y2(:)]';
  toc
[xp,yp,xi,yi]=intersections(FullTrajectories(1,:),FullTrajectories(2,:),Walls(1,:),Walls(2,:));
  toc
TrajectroryPolygonOrder=size(TrajectoryBasic,2)+1;
BallNumber=floor(xi/TrajectroryPolygonOrder)+1;
LocationOnPolygon=xi-(BallNumber-1)*TrajectroryPolygonOrder;
% plot(X1(:)+X2(:),Y1(:)+Y2(:))