function [X,Y]=PolyIndexToPoint(Polygon,Index)
IndexFloor=floor(Index);
IndexResidue=Index-IndexFloor;
X=Index*0;Y=Index*0;
IDXNotNan=~isnan(Index);
X(IDXNotNan)=(1-IndexResidue(IDXNotNan,1))'.*Polygon(1,IndexFloor(IDXNotNan))+IndexResidue(IDXNotNan,1)'.*Polygon(1,IndexFloor(IDXNotNan)+1);
Y(IDXNotNan)=(1-IndexResidue(IDXNotNan,1))'.*Polygon(2,IndexFloor(IDXNotNan))+IndexResidue(IDXNotNan,1)'.*Polygon(2,IndexFloor(IDXNotNan)+1);

end