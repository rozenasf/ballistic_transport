function [PolyNumber,PolyIndex,WallNumber,X,Y]=GetPolyTrajectroyIntersetions(FullPolygons,Walls,PolySize)
%Assuming all of the walls are one polygon!!!
% persistent  FullPolygons2
% MEX=1;
% if(MEX)
%     FullPolygons2=nan(2,2e5);
%     FullPolygons2(:,1:size(FullPolygons,2))=FullPolygons;
%     [X,Y,xi,yi]=intersections_mex(FullPolygons2(1,:),FullPolygons2(2,:),Walls(1,:),Walls(2,:),0); 
% else
    [X,Y,xi,yi]=intersections(FullPolygons(1,:),FullPolygons(2,:),Walls(1,:),Walls(2,:),0); 
% end

    PolyNumber=floor((floor(xi)-1)/(PolySize+1))+1;
    PolyIndex=xi-(PolyNumber-1)*(PolySize+1);
    WallNumber=floor(yi);
    WallNumber=(WallNumber-1)/3+1;
    if(floor(WallNumber)~=WallNumber);error('fractionwall!');end
    
end