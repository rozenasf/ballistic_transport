function CheckSumOK=WallAccumulation()
    global Balls Walls
    Balls.FinalWall=Balls.FinalWall(Balls.FinalWall~=0);
    Acumvec=accumarray(Balls.FinalWall,1,[numel(Walls)-2,1]);
    for i=1:numel(Walls)-2
       Walls(i).Counter=Acumvec(i);
    end
    CheckSumOK=(sum(Acumvec)==size(Balls.WallNo,1));
end