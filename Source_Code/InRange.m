function out=InRange(Value,Lim1,Lim2)
    out=(Value>=Lim1) .* (Value<=Lim2);
end