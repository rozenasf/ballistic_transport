function Data=LoadCluster(Location,RunIndex)
if(~exist('RunIndex'));RunIndex=[];end
if(isempty(RunIndex))
    if(Location(end)~='/');Location=[Location,'/'];end
out=dir([Location,'cr*']);
I=0;J=0;
for i=1:numel(out)
    Temp=regexp(out(i).name,'_','split');
    if(str2num(Temp{2})>J);I=i;J=str2num(Temp{2});end
end
out=out(I);
else
    out=dir([Location,'cr_',num2str(RunIndex),'*']);
end
FileList=dir([Location,'\',out.name,'\run*']);
Data=struct.empty();
for i=1:numel(FileList)
   fprintf('Data file: %d\n',i)
   Index=regexp(FileList(i).name(5:end),'_','split');Index=str2num(Index{1});
   Data{Index}=load([Location,'\',out.name,'\',FileList(i).name]);%Data{Index}=Data{Index}.func_output;
end
[~,Size]=ExplodeStruct(Data{1}.ParameterSweepBare);
if(numel(size(Size,2))>1)
Data=reshape(Data,Size);
end

end