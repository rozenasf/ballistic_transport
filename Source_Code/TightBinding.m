t_1=1;t_3=0.14;a=[0.5,-sqrt(3)/2]';b=[0.5,sqrt(3)/2]';
epsilon=@(k) -2*t_1*(cos(k*a)+cos(k*b)+cos(-k*(b+a)))...
    -2*t_3*(cos(k*a).^2+cos(k*b).^2+cos(k*(a+b)).^2);
k_x=linspace(-1.3*pi,1.3*pi,600);
k_y=linspace(-1.3*pi,1.3*pi,600);
[k_x_bla,k_y_bla]=meshgrid(k_x,k_y);
a2=2*pi*a/sin(pi/3);
b2=2*pi*b/sin(pi/3);
In1stBZ=@(temp) abs(temp*a)<pi & abs(temp*b)<pi & abs((temp*[1,0;0,-1])*b)<pi & abs((temp*[1,0;0,-1])*a)<pi ;
eps=zeros(numel(k_x),numel(k_y));
for i=1:numel(k_x)
    for j=1:numel(k_y)
        temp=[k_x(i);k_y(j)]';
        eps(i,j)=epsilon(temp);
        inBZ(i,j)=In1stBZ(temp);
    end
end
[Fx,Fy]=gradient(eps);
F=sqrt(Fx.^2+Fy.^2);
theta=atan2(Fy,Fx);
thresh=0.05;e_f=1;
 plot(atan2(k_y_bla(abs(eps-e_f)<thresh & inBZ),k_x_bla(abs(eps-e_f)<thresh & inBZ)),F(abs(eps-e_f)<thresh & inBZ),'.');
 figure;plot(atan2(k_y_bla(abs(eps-e_f)<thresh & inBZ),k_x_bla(abs(eps-e_f)<thresh & inBZ)),theta(abs(eps-e_f)<thresh & inBZ),'.');
 %%
 %% make theoretical band structure
t_1=1;t_3=0.14;a=[0.5,-sqrt(3)/2]';b=[0.5,sqrt(3)/2]';
%epsilon=@(k) -2*t_1*(cos(k*a)+cos(k*b)+cos(-k*(b+a)))...
 %   -2*t_3*(cos(k*a).^2+cos(k*b).^2+cos(k*(a+b)).^2);
In1stBZ=@(temp) abs(temp*a)<pi & abs(temp*b)<pi & abs((temp*[1,0;0,-1])*b)<pi & abs((temp*[1,0;0,-1])*a)<pi ;
Product_angle_A=@(theta,a) [cos(theta),sin(theta)]*a; 
epsilon_theta=@(k,theta) -2*t_1*(cos(k*Product_angle_A(theta,a))+cos(k*Product_angle_A(theta,b))+cos(-k*Product_angle_A(theta,a+b)))...
    -2*t_3*(cos(k*Product_angle_A(theta,a)).^2+cos(k*Product_angle_A(theta,b)).^2+cos(k*Product_angle_A(theta,a+b)).^2);

%% mess around
k=linspace(0,pi,100);
hold on;plot(k,epsilon_theta(k,pi/6));
E_f=1;
%solve(epsilon_theta(k,theta)==E_f,
thetak=atan2(k_y_bla(abs(eps-e_f)<thresh & inBZ),k_x_bla(abs(eps-e_f)<thresh & inBZ));
thetakr=thetak+rand(numel(thetak),1)*1e-6;
[thetak_sorted,I]=sort(thetakr);
thetaSpeed=theta(abs(eps-e_f)<thresh & inBZ);
FSpeed=F(abs(eps-e_f)<thresh & inBZ);
Kx_roi=k_x_bla(abs(eps-e_f)<thresh & inBZ);
Ky_roi=k_y_bla(abs(eps-e_f)<thresh & inBZ);
%
thetaSpeedSorted=thetaSpeed(I);
FSpeedSorted=FSpeed(I);
Kx_roi_sorted=Kx_roi(I);
Ky_roi_sorted=Ky_roi(I);
%
ThetaList=linspace(-pi,pi,300);

Vf_theta=interp1(thetak_sorted,thetaSpeedSorted,ThetaList);
F_theta=interp1(thetak_sorted,FSpeedSorted,ThetaList);
Kx_theta=interp1(thetak_sorted,Kx_roi_sorted,ThetaList);
Ky_theta=interp1(thetak_sorted,Ky_roi_sorted,ThetaList);

% p=polyfit(thetak,thetaSpeed,100);
figure;plot(Kx_theta,Ky_theta,'.');
hold on;
% plot(linspace(-pi,pi,300),interp1(thetak_sorted,thetaSpeedSorted,linspace(-pi,pi,300)));
% for i=1:60
%     find()
% end
%% get fermisurface data



