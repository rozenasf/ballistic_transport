function Theta=ThetaK2Theta(ThetaK,Hexagonal)
    Theta=mod(pi/3*floor((ThetaK+pi/6+Hexagonal)./(pi/3)),2*pi);
end