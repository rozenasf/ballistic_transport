function [Voltages,Currents,SimulationOut]=LandawerButikker(Simulation,VoltageSource)
options=optimset('Display','off');
if(size(VoltageSource,1)<numel(Simulation))
Voltages=fsolve(@(V)LandawerButikkerEquation(Simulation,VoltageSource,V),zeros(size(Simulation)),options);
else
    Voltages=VoltageSource(:,2)';
end
Currents=CurrentFromVoltage(Simulation,Voltages);

if(nargout==3)
    SimulationOut=AddSimulationsWeighted(Simulation,Voltages);
end

end