function Out=AppendStruct(In,varargin)
Out=In;
for i=1:numel(varargin)/2
    Out.(varargin{i*2-1})=varargin{i*2};
end
end