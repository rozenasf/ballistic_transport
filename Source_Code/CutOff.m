    function ThetaOut=CutOff(ThetaIn)
        ThetaOut=sign(ThetaIn).*(pi/2-abs(pi/2-abs(ThetaIn)));
    end