function out=SubtractSimulations(varargin)
    out=varargin{1};
    for i=2:numel(varargin)
        out.IDensity.data=out.IDensity.data-varargin{i}.IDensity.data;
        out.ICurrent.data=out.ICurrent.data-varargin{i}.ICurrent.data;
        if(isfield(out,'IVoltage'))
        out.IVoltage=out.IVoltage-varargin{i}.IVoltage;
        end
        if(~isempty(out.Distribution))
        out.Distribution.Data=out.Distribution.Data-varargin{i}.Distribution.Data;
        end
        out.TotalCurrent=out.TotalCurrent-varargin{i}.TotalCurrent;
        for j=1:numel(out.Accumulator)
            out.Accumulator{j}=out.Accumulator{j}-varargin{i}.Accumulator{j};
        end
    end
end