function out=FilterWithAngleSegments(in,Segments)
out=[];
for i=1:size(Segments,2)
    out=[out,in((in<=Segments(2,i)) & (in>=Segments(1,i)))];
end
% in=mod(in,2*pi);
% for j=numel(in):-1:1
%     if(sum((repmat(in(j),1,size(Segments,2))>=Segments(1,:)).*(repmat(in(j),1,size(Segments,2))< Segments(2,:)))==0)
%         in(j)=[];
%     end
% end
end
%%
%     1.6944    4.2124    5.2596         0
%     2.1180    4.8360    6.2832    1.0708
% in : 1X1e4