function Idata2=SmoothIdata(Idata,)
XSpacing=mean(diff(Idata.axes{1}.data));
YSpacing=mean(diff(Idata.axes{2}.data));

XVec=linspace(-3*Sigma,3*Sigma,6*ceil(Sigma/XSpacing)+1);
YVec=linspace(-3*Sigma,3*Sigma,6*ceil(Sigma/YSpacing)+1);
[XX,YY]=meshgrid(XVec,YVec);
SmoothMat=exp((-XX.^2-YY.^2)./(2*Sigma));
Matrix=conv2(Matrix,SmoothMat,'same');
Idata2=Idata*1;
Idata2.data=Matrix;
end