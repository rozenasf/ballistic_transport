function out=PackToStruct(varargin)
switch nargout
    case 0
        out=varargin{1};
        for i=2:numel(varargin);
            out.(inputname(i))=varargin{i};
        end
        assignin('caller',inputname(1),out);
    case 1
        out=struct();
        for i=1:numel(varargin);
            out.(inputname(i))=varargin{i};
        end
end
end