function Currents=CurrentFromVoltage(Simulation,Voltages)
Currents=Voltages*0;
for i=1:numel(Voltages)
    Wi=Simulation{1}.Walls(Simulation{1}.Parameters.Absorbers(i)).Length;
%     Currents(i)=Voltages(i)*Wi*2/pi;
      Currents(i)=Voltages(i)*Wi/pi;
    for j=1:numel(Voltages)
        Wj=Simulation{1}.Walls(Simulation{1}.Parameters.Absorbers(j)).Length;
        Currents(i) = Currents(i) - Voltages(j).*Simulation{j}.TransmissionProbablity(i)*Wj/pi;
    end
end

end