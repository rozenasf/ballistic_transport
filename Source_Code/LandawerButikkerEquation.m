function out=LandawerButikkerEquation(Simulation,VoltageSource,Voltages)
    Currents=CurrentFromVoltage(Simulation,Voltages);
    out=Currents;
    for i=1:size(VoltageSource,1)
        out(VoltageSource(i,1))=Voltages(VoltageSource(i,1))-VoltageSource(i,2);
    end
end