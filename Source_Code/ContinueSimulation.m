global Simulation;

for j=1:size(ParametersArray,1)
    for l=1:size(ParametersArray,2)
        Parameters=ParametersArray(j,l);
        %%
        if(~isfield(Parameters,'ThetaScatter'))
            Parameters.ThetaScatter=@(n)(rand(n,1)-0.5)*2*pi;
        end
        %% Generate Balls
        Magnetic=~isinf(Parameters.R);
        if(Magnetic)
            InitializeBallsWithField();
        else
            InitializeBallsNoField();
        end
        %% --- Main Loop ---
        CalculationTic=tic;
        for i=1:Parameters.MaxCollisions-1
            if(Magnetic)
                CalculateNextCollisionWithField()
            else
                CalculateNextCollisionNoField();
            end
            if(sum(isnan(Balls.WallNo(Balls.Active,Balls.Step+1)))>0);continue;end % patch to allow the leakage not to affect to overnight calculation
            if(ScatterV2());break;end
            %         if(Scatter());break;end
            UpdateBallsMagneticSpecificParameters();
            %     if(ScatterNoField());break;end
            Balls.Step=Balls.Step+1;
        end
        CPUTic=toc(CalculationTic);
        if(i==(Parameters.MaxCollisions-1))
            fprintf('Increace number of max collisions\n');
        else
            fprintf('needed %d iterations\n',i);
        end
        %fprintf('Time for Calculation:%0.2f\n',toc(CalculationTic));
        %% Add to accumulator
        AccumTic=tic;
        if (Magnetic)
            AddToAccumulatorWithField()
        else
            AddToAccumulatorNoField()
        end
        RamTic=toc(AccumTic);
        [IDensity,ICurrent]=GetIdata();
        RamCPURatio=RamTic/CPUTic;
        TimePerBall=toc(CalculationTic)/Parameters.N;
    end
    
end

% fprintf('Total Time:%0.2f\n',toc(TotalTic));
[IDensity,ICurrent,X,Y]=GetIdata();

if(isfield(Parameters,'CellIndex'))
    Loc=num2cell(Parameters.CellIndex);
    Simulation{Loc{:}}=PackToStruct(IDensity,ICurrent,Walls,ParametersArray,Accumulator,Info,X,Y);
else
    Simulation=PackToStruct(IDensity,ICurrent,Walls,ParametersArray,Accumulator,Info,X,Y);
end
