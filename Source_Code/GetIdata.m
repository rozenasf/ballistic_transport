function [IDensity,ICurrent,ICounter,Xvec,Yvec]=GetIdata(Accumulator,Counter,Parameters,Walls)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

Xvec=linspace(1,Walls(1).MaxScaled(1),size(Accumulator{1},1)+1);
Xvec=mean([Xvec(1:end-1)',Xvec(2:end)'],2);
Xvec=Scale2Real(Xvec,1,Parameters,Walls);
Yvec=linspace(1,Walls(1).MaxScaled(2),size(Accumulator{1},2)+1);
Yvec=mean([Yvec(1:end-1)',Yvec(2:end)'],2);
Yvec=Scale2Real(Yvec,2,Parameters,Walls);
Ax{1}=struct('data', Xvec);
Ax{2}=struct('data', Yvec);
IDensity=struct('data',Accumulator{1});IDensity.axes=Ax;
ICurrent=struct('data',Accumulator{2});ICurrent.axes=Ax;
ICounter=struct('data',Counter');ICounter.axes=Ax;
end