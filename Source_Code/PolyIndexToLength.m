function Length=PolyIndexToLength(Polygon,Index)
    PolyLength=(sqrt(( Polygon(1,1:(end-1))- Polygon(1,2:end)).^2+( Polygon(2,1:(end-1))- Polygon(2,2:end)).^2));
    PolyLengthCum=cumsum( [0,PolyLength(1:end-1)]);
    PolyTotalLength=sum( PolyLength);

    NPoly=size(Polygon,2)-1;
    IndexMod=mod(Index-1,NPoly)+1;
    Extra=(Index-IndexMod)/NPoly;
Length=Index*nan;
    NotNan=~isnan(IndexMod); %NEW TO fix the max collisions!!!
    Length(NotNan)=PolyLengthCum(floor(IndexMod(NotNan)))'+(IndexMod(NotNan)-floor(IndexMod(NotNan))).*PolyLength(floor(IndexMod(NotNan)))'+Extra(NotNan)*PolyTotalLength;
end