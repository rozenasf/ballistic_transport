function PolyIndex=PolyIndexFromPolyK(Polygon,PolyK)
PolygonThetaValues=ThetaPolygonFromPolygon(Polygon);

PolyTheta=mod(PolyK-pi/2,2*pi);
PolyIndex=PolyK*0;
PolySize=numel(PolygonThetaValues);

%This method is faster then interp ...
EffectiveFloor1=sum(repmat(PolygonThetaValues,numel(PolyTheta),1)<repmat(PolyTheta,1,numel(PolygonThetaValues)) ,2);


% EffectiveFloor2=interp1(PolygonThetaValues,1:numel(PolygonThetaValues),PolyTheta,'previous');
Index=mod(EffectiveFloor1-1,PolySize)+1;

x2=Polygon(1,Index)';
        x1=Polygon(1,mod(Index,PolySize)+1)';
        y2=Polygon(2,Index)';
        y1=Polygon(2,mod(Index,PolySize)+1)';
        Alpha = ( y2 - x2 .* tan(-PolyTheta) ) ./  ( (y2-y1) - tan(-PolyTheta).*(x2-x1));
        PolyIndex=Index+Alpha;
%         
%     for i=1:numel(PolyTheta)
%         Index=mod(sum(PolygonThetaValues<PolyTheta(i))-1,PolySize)+1;
%         x2=Polygon(1,Index);
%         x1=Polygon(1,mod(Index,PolySize)+1);
%         y2=Polygon(2,Index);
%         y1=Polygon(2,mod(Index,PolySize)+1);
%         Alpha = ( y2 - x2 * tan(-PolyTheta(i)) ) ./  ( (y2-y1) - tan(-PolyTheta(i))*(x2-x1));
%         PolyIndex(i)=Index+Alpha;
%     end
    
end