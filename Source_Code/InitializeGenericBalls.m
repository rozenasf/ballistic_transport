function Balls=InitializeGenericBalls(Walls,Parameters,Balls)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

N=Parameters.N;


if(isfield(Parameters,'EmitterRange'))
    t=Parameters.EmitterRange(1)+rand(N,1)*diff(Parameters.EmitterRange);
else
    t=rand(N,1);
end
if(isstruct(Parameters.Emitter))
    Balls.X(:,1)=Real2Scale(Parameters.Emitter.X,1,Parameters,Walls); %Real2Scale(,1)
    Balls.Y(:,1)=Real2Scale(Parameters.Emitter.Y,2,Parameters,Walls);%Real2Scale(,2)
    Balls.Theta(:,1)=Parameters.Emitter.Theta;
    Balls.WallNo(:,1)=(numel(Walls)-1)*ones(N,1);
else
    WallNo=Parameters.Emitter;
    Balls.X(:,1)=Walls(WallNo).X(1)+t*diff(Walls(WallNo).X);
    Balls.Y(:,1)=Walls(WallNo).Y(1)+t*diff(Walls(WallNo).Y);
    Balls.WallNo(:,1)=WallNo*ones(N,1);
    if(isa(Parameters(1).EmittedFunciton,'function_handle'))
        if(nargin(Parameters(1).EmittedFunciton)==0)
            for i=1:N
                Balls.Theta(i,1)=mod(Parameters.EmittedFunciton(),2*pi);
            end
        elseif(nargin(Parameters(1).EmittedFunciton)==1)
            Balls.Theta(:,1)=mod(Parameters.EmittedFunciton(N),2*pi);
        elseif(nargin(Parameters(1).EmittedFunciton)==3)

            Balls.Theta(:,1)=mod(Parameters.EmittedFunciton(N,...
                Scale2Real(Walls(WallNo).X,1,Parameters,Walls),...
                Scale2Real(Walls(WallNo).Y,2,Parameters,Walls)),...
                2*pi);
        else
            error('wrong EmittedFunction structure');
        end
    else
        Balls.Theta(:,1)=randpdf(Parameters.EmittedFunciton(2,:),Parameters.EmittedFunciton(1,:),[N,1]);
    end
end
    Balls.InitialWalls=Balls.WallNo(:,1);
Balls.Active=[1:N]';
Balls.Step=1;
Balls.lMR(:,1)=Parameters.lMR/Parameters.GridSize*ones(N,1);
Balls.FinalWall=zeros(N,1);
Balls.LengthOfFlight=zeros(N,1);
if(isfield(Parameters,'DeltaT'))
    Balls.DeltaT=Parameters.DeltaT;
    Balls.Vfermi=Parameters.Vfermi/Parameters.GridSize;
end
if(isfield(Parameters,'AlphaCharge'))
    Balls.AlphaCharge(:,1)=Parameters.AlphaCharge;
else
    Balls.AlphaCharge(:,1)=ones(N,1);
end
if(isfield(Parameters,'leeP'))
    Balls.HydroEvents=struct('X',[],'Y',[],'Theta',[],'Location',0);
end
end