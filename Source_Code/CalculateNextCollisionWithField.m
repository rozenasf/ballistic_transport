function Balls=CalculateNextCollisionWithField(Balls,Walls)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

CrashAt=nan(numel(Balls.Active),4,2*numel(Walls));%XYPI

for i=1:(numel(Walls)-2)
    Wall=Walls(i);
    switch Wall.Type
        case 1 %X0=X1
            LogicA=(Wall.X(1)-Balls.Xc(Balls.Active,Balls.Step)).^2<Balls.R(Balls.Active,Balls.Step).^2;
            Delta=sqrt(Balls.R(Balls.Active(LogicA),Balls.Step).^2-(Balls.Xc(Balls.Active(LogicA),Balls.Step)-Wall.X(1)).^2);
            Y1=nan(numel(Balls.Active),1);Y2=Y1;X=Y1;P1=Y1;P2=Y1;
            X(LogicA)=Wall.X(1)*ones(sum(LogicA),1);
            
            Y1(LogicA)=Balls.Yc(Balls.Active(LogicA),Balls.Step)+Delta;
            Y2(LogicA)=Balls.Yc(Balls.Active(LogicA),Balls.Step)-Delta;

            Logic1=InRange(Y1,Wall.Y(1),Wall.Y(2));
            Logic2=InRange(Y2,Wall.Y(1),Wall.Y(2));
            Logic1(Logic1==0)=nan;
            Logic2(Logic2==0)=nan;
            
            P1(LogicA)=mod(atan(-(Y1(LogicA,1)-Balls.Yc(Balls.Active(LogicA),Balls.Step))...
                ./(X(LogicA,1)-Balls.Xc(Balls.Active(LogicA),Balls.Step)))+(X(LogicA,1)<Balls.Xc(Balls.Active(LogicA),Balls.Step))*pi,2*pi);
            P2(LogicA)=mod(atan(-(Y2(LogicA,1)-Balls.Yc(Balls.Active(LogicA),Balls.Step))...
                ./(X(LogicA,1)-Balls.Xc(Balls.Active(LogicA),Balls.Step)))+(X(LogicA,1)<Balls.Xc(Balls.Active(LogicA),Balls.Step))*pi,2*pi);
            
            CrashAt(:,:,2*i-1)=[X.*Logic1,Y1.*Logic1,P1.*Logic1,i.*Logic1];
            CrashAt(:,:,2*i)  =[X.*Logic2,Y2.*Logic2,P2.*Logic2,i.*Logic2];
            
        case 2 %Y0=Y1
            LogicA=(Wall.Y(1)-Balls.Yc(Balls.Active,Balls.Step)).^2<Balls.R(Balls.Active,Balls.Step).^2;
            Delta=sqrt(Balls.R(Balls.Active(LogicA),Balls.Step).^2-(Balls.Yc(Balls.Active(LogicA),Balls.Step)-Wall.Y(1)).^2);
            X1=nan(numel(Balls.Active),1);X2=X1;Y=X1;P1=X1;P2=X1;
            
            Y(LogicA)=Wall.Y(1)*ones(sum(LogicA),1);
            X1(LogicA)=Balls.Xc(Balls.Active(LogicA),Balls.Step)+Delta;
            X2(LogicA)=Balls.Xc(Balls.Active(LogicA),Balls.Step)-Delta;

            Logic1=InRange(X1,Wall.X(1),Wall.X(2));
            Logic2=InRange(X2,Wall.X(1),Wall.X(2));
            Logic1(Logic1==0)=nan;
            Logic2(Logic2==0)=nan;
            
            P1(LogicA)=mod(atan(-(Y(LogicA,1)-Balls.Yc(Balls.Active(LogicA),Balls.Step))...
                ./(X1(LogicA,1)-Balls.Xc(Balls.Active(LogicA),Balls.Step)))+(X1(LogicA,1)<Balls.Xc(Balls.Active(LogicA),Balls.Step))*pi,2*pi);
            P2(LogicA)=mod(atan(-(Y(LogicA,1)-Balls.Yc(Balls.Active(LogicA),Balls.Step))...
                ./(X2(LogicA,1)-Balls.Xc(Balls.Active(LogicA),Balls.Step)))+(X2(LogicA,1)<Balls.Xc(Balls.Active(LogicA),Balls.Step))*pi,2*pi);
            
            CrashAt(:,:,2*i-1)=[X1.*Logic1,Y.*Logic1,P1.*Logic1,i.*Logic1];
            CrashAt(:,:,2*i)  =[X2.*Logic2,Y.*Logic2,P2.*Logic2,i.*Logic2];
        case 0

            a=(Wall.m.^2+1)*ones(size(Balls.Active,1),1);
            b=-2.*Balls.Xc(Balls.Active,Balls.Step)+2*Wall.m.*(Wall.n-Balls.Yc(Balls.Active,Balls.Step));
            c=((Wall.n-Balls.Yc(Balls.Active,Balls.Step)).^2+Balls.Xc(Balls.Active,Balls.Step).^2-Balls.R(Balls.Active,Balls.Step).^2);
            
            Delta=b.^2-4.*a.*c;
            LogicA=(Delta>0);
            Delta=Delta(LogicA);
            a=a(LogicA);
            b=b(LogicA);
            c=c(LogicA);
            X1=nan(numel(Balls.Active),1);X2=X1;Y1=X1;Y2=X1;
            P1=nan(numel(Balls.Active),1);P2=nan(numel(Balls.Active),1);
            X1(LogicA,1)=(-b+sqrt(Delta))./(2.*a);
            X2(LogicA,1)=(-b-sqrt(Delta))./(2.*a);
            Y1(LogicA,1)=Wall.m.*X1(LogicA,1)+Wall.n;
            Y2(LogicA,1)=Wall.m.*X2(LogicA,1)+Wall.n;
            
             Logic1=InRange(X1,Wall.X(1),Wall.X(2));
             Logic2=InRange(X2,Wall.X(1),Wall.X(2));
             Logic1(Logic1==0)=nan;
             Logic2(Logic2==0)=nan;

            P1(LogicA)=mod(-atan((Y1(LogicA,1)-Balls.Yc(Balls.Active(LogicA,1),Balls.Step))./(X1(LogicA,1)-Balls.Xc(Balls.Active(LogicA,1),Balls.Step)))+...
                (X1(LogicA,1)<Balls.Xc(Balls.Active(LogicA),Balls.Step))*pi,2*pi);
            P2(LogicA)=mod(-atan((Y2(LogicA,1)-Balls.Yc(Balls.Active(LogicA,1),Balls.Step))./(X2(LogicA,1)-Balls.Xc(Balls.Active(LogicA,1),Balls.Step)))+...
                (X2(LogicA,1)<Balls.Xc(Balls.Active(LogicA),Balls.Step))*pi,2*pi);
            
            CrashAt(:,:,2*i-1)=[X1.*Logic1,Y1.*Logic1,P1.*Logic1,i.*Logic1];
            CrashAt(:,:,2*i)  =[X2.*Logic2,Y2.*Logic2,P2.*Logic2,i.*Logic2];
            end
end

 DPhi=Balls.Dir(Balls.Active,Balls.Step).*exprnd(Balls.lMR(Balls.Active,Balls.Step))./Balls.R(Balls.Active,Balls.Step);
 NewPhi=Balls.Phi(Balls.Active,Balls.Step)-DPhi;
 CrashAt(:,:,2*numel(Walls)-2)=[Balls.Xc(Balls.Active,Balls.Step)+Balls.R(Balls.Active,Balls.Step).*cos(NewPhi),...
     Balls.Yc(Balls.Active,Balls.Step)-Balls.R(Balls.Active,Balls.Step).*sin(NewPhi),...
     NewPhi,(numel(Walls)-1)*ones(size(NewPhi))];
%Bulk Dying
if(isfield(Balls,'DeltaT'))
    L=Balls.DeltaT*Balls.Vfermi-Balls.LengthOfFlight(Balls.Active);
    DPhi=Balls.Dir(Balls.Active,Balls.Step).*L./Balls.R(Balls.Active,Balls.Step);
 NewPhi=Balls.Phi(Balls.Active,Balls.Step)-DPhi;
 CrashAt(:,:,2*numel(Walls)-1)=[Balls.Xc(Balls.Active,Balls.Step)+Balls.R(Balls.Active,Balls.Step).*cos(NewPhi),...
     Balls.Yc(Balls.Active,Balls.Step)-Balls.R(Balls.Active,Balls.Step).*sin(NewPhi),...
     NewPhi,(numel(Walls))*ones(size(NewPhi))];
else

    CrashAt(:,:,2*numel(Walls)-1)=[NewPhi*0+1,NewPhi*0+1,NewPhi*0-nan,(numel(Walls))*ones(size(DPhi))];
end

% [~,ClosestCrashIDX]=min(mod(repmat(Balls.Dir*Balls.Phi(Balls.Active,Balls.Step),[1,1,size(CrashAt,3)])-Balls.Dir*CrashAt(:,3,:)-1e-6,2*pi),[],3);
DeltaPhiCrashList=repmat(Balls.Dir(Balls.Active,Balls.Step).*Balls.Phi(Balls.Active,Balls.Step),[1,1,size(CrashAt,3)])-repmat(Balls.Dir(Balls.Active,Balls.Step),[1,1,size(CrashAt,3)]).*CrashAt(:,3,:)-1e-6;
DeltaPhiCrashList(DeltaPhiCrashList<0)=DeltaPhiCrashList(DeltaPhiCrashList<0)+2*pi;
[~,ClosestCrashIDX]=min(DeltaPhiCrashList,[],3);

IDX=sub2ind(size(CrashAt), [1:numel(Balls.Active)]', ones(numel(Balls.Active),1) ,ClosestCrashIDX);

Balls.X(Balls.Active,Balls.Step+1)=CrashAt(IDX);
Balls.Y(Balls.Active,Balls.Step+1)=CrashAt(IDX+numel(Balls.Active));
Balls.WallNo(Balls.Active,Balls.Step+1)=CrashAt(IDX+3*numel(Balls.Active));



Balls.Phi1(Balls.Active,Balls.Step)=mod(CrashAt(IDX+2*numel(Balls.Active)),2*pi); 

Balls.ExtraRotations(Balls.Active,Balls.Step)=floor(abs(DPhi+1e-6)/(2*pi));
RegularScatterLogic=(Balls.WallNo(Balls.Active,Balls.Step+1)<=(numel(Walls)-2)); %was 1- but is correct nows
Balls.ExtraRotations(Balls.Active(RegularScatterLogic),Balls.Step)=zeros(sum(RegularScatterLogic),1);

Phi=Balls.Phi(Balls.Active,Balls.Step);Phi1=Balls.Phi1(Balls.Active,Balls.Step);

 LogicDir=(Balls.Dir(Balls.Active,Balls.Step)==1);
 IDXShiftPositive=Phi1(:)>Phi(:);
 Phi1(LogicDir & IDXShiftPositive)=Phi1(LogicDir & IDXShiftPositive)-2*pi;
 Phi1(~LogicDir & ~IDXShiftPositive)=Phi1(~LogicDir & ~IDXShiftPositive)+2*pi;

% if(1)%(Balls.Dir==1)
%     IDXShift=Phi1(:)>Phi(:);
%     Phi1(IDXShift)=Phi1(IDXShift)-2*pi;
% else
%     IDXShift=Phi1(:)<Phi(:);
%     Phi1(IDXShift)=Phi1(IDXShift)+2*pi;
% end

Balls.LengthOfFlight(Balls.Active)=Balls.LengthOfFlight(Balls.Active)+ ...
((Phi-Phi1)+2*pi*Balls.ExtraRotations(Balls.Active,Balls.Step)).*Balls.R(Balls.Active,Balls.Step);
newlength=((Phi-Phi1)+2*pi*Balls.ExtraRotations(Balls.Active,Balls.Step)).*Balls.R(Balls.Active,Balls.Step);
Balls.AccumulatedLength(Balls.Active,Balls.Step+1)=rand(size(newlength))*1e3;
%Here i removed 2 Balls.Dir ???

Balls.Theta1(Balls.Active,Balls.Step)=mod(Balls.Phi1(Balls.Active,Balls.Step)-pi/2*Balls.Dir(Balls.Active,Balls.Step),2*pi);
end