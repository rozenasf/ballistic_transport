function PlotWalls(Walls,Parameters)
% global Walls
Ax=gca;
    for i=1:numel(Walls)
        %color='blue';
        color=[70,146,198]/255;
        if(Walls(i).AbsorbCoeff==1);color='red';end
       line(Ax,Scale2Real(Walls(i).X,1,Parameters,Walls),Scale2Real(Walls(i).Y,2,Parameters,Walls),...
           'Color',color,'LineWidth',1);

    end

end