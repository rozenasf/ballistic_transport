function LinesAsCell=LoadGeometry(FilePath)
if(~exist('FilePath'));FilePath='Output.SLDMAT';end
Lines=[];
Geometry=load(FilePath);
% f=fopen(FilePath,'r');
% while(~feof(f))
%    eval(fgetl(f) );
% end
% fclose (f);
% Geometry=round(Lines.Default.(['Sketch1'])*1000)/1000;

LinesAsCell={};
for i=1:size(Geometry,1)
    LinesAsCell{i}=[Geometry(i,1),Geometry(i,3);Geometry(i,2),Geometry(i,4)];
end

end