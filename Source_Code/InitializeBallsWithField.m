function Balls=InitializeBallsWithField(Walls,Parameters)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

N=Parameters.N;MaxCollisions=Parameters.MaxCollisions;
Balls=struct('X',nan(N,MaxCollisions),'Y',nan(N,MaxCollisions),'Theta',...
    nan(N,MaxCollisions),'Theta1',nan(N,MaxCollisions),...
    'Phi',nan(N,MaxCollisions),'Phi1',nan(N,MaxCollisions),...
    'Xc',nan(N,MaxCollisions),'Yc',nan(N,MaxCollisions),'R',nan(N,MaxCollisions),...
    'Dir',nan(N,MaxCollisions),'lMR',nan(N,MaxCollisions),...
    'WallNo',nan(N,MaxCollisions),'Done',zeros(N,1),...
    'ExtraRotations',zeros(N,MaxCollisions),'AlphaCharge',nan(N,MaxCollisions),'AccumulatedLength',nan(N,MaxCollisions));


%ThetaMid+asin(rand(N,1)*2-1)
Balls.R(:,1)=abs(Parameters.R/Parameters.GridSize)*ones(N,1);
Balls.Dir(:,1)=sign(Parameters.R)*ones(N,1);

Balls=InitializeGenericBalls(Walls,Parameters,Balls);
Balls.AccumulatedLength(:,1)=zeros(size(Balls.AccumulatedLength,1),1);
Balls.Phi(:,1)=mod(Balls.Theta(:,1)+pi/2*sign(Parameters.R),2*pi);
Balls.Xc(:,1)=Balls.X(:,1)+Balls.R(:,1).*sin(Balls.Theta(:,1)).*Balls.Dir(:,1);
Balls.Yc(:,1)=Balls.Y(:,1)+Balls.R(:,1).*cos(Balls.Theta(:,1)).*Balls.Dir(:,1);
% Balls.Kf=Parameters.Kf;
end