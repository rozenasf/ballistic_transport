function WS=WallScatterings
    WS.Specular=@Specular;
    WS.Diffusive=@Diffusive;
    WS.PartiallyDiffusive=@PartiallyDiffusive;
    WS.XMumentumKick=@XMumentumKick;
    WS.GaussSpread=@GaussSpread;
    
    function out=Specular(Theta,Rand)
        out=-Theta;
    end
    function out=Diffusive(Theta,Rand)
        out=(Rand-0.5)*pi;
    end
    function out=PartiallyDiffusive(Theta,Rand,P)
        out=(1-P)*(Rand-0.5)*pi-P*Theta;
    end
    function out=XMumentumKick(Theta,Rand,P)
        Xmumentum=sin(Theta)+erfinv(Rand*2-1)*P;
        Ymumentum=-cos(Theta);
        out=atan(Xmumentum./Ymumentum);
    end
    function out=GaussSpread(Theta,Rand,P)
        out=CutOff(-Theta+erfinv(Rand*2-1)*P);
    end
end