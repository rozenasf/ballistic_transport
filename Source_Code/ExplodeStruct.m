function [UserOut,Size]=ExplodeStruct(User)
Names=fieldnames(User);
A={};
for i=1:numel(Names)
    A{i}=User.(Names{i});
end
B=cell(size(A));
[B{:}]=ndgrid(A{:});
UserOut=User;
for i=1:numel(B{1})
    UserOut(i)=User;
    for j=1:numel(Names)
        UserOut(i).(Names{j})=B{j}(i);
    end
end
Size=size(B{1});Size(Size==1)=[];
end