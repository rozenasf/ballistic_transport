function Theta=PerpendicularAngle(X,Y)
    if(diff(X)==0)
        Theta=0;
    elseif(diff(Y)==0)
        Theta=pi/2;
    else
        Theta=-atan(diff(Y)./diff(X))-pi/2;
    end
    Theta=Theta+pi*((-sin(Theta)*mean(Y)+cos(Theta)*mean(X))>0);
    Theta=mod(Theta,2*pi);
end