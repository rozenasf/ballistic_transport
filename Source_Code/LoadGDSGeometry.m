function Geometry=LoadGDSGeometry(FilePath)
GDS=gds_load(FilePath);
Polygon=GDS.structures.elements.element.points/1e3;

Geometry={};
for i=1:size(Polygon,2)-1
    Geometry{i}=[Polygon(1,i),Polygon(1,i+1);Polygon(2,i),Polygon(2,i+1)];
end

end