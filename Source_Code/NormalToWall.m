function Angle=NormalToWall(Walls,WallNo)
    Angle=pi/2-atan(diff(Walls(WallNo).Y)./diff(Walls(WallNo).X));
    Angle(Angle>pi/2)=Angle-pi;
end