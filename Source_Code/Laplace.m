
%%

X=4;
ny=64;
nx=ny*X;

niter=300000;

x=linspace(0,X,nx);
y=linspace(0,1,ny);
dx=mean(diff(x))*1;dy=mean(diff(y))*1;
Mid=21;

%%

pn=zeros(ny,nx);
p=ones(ny,1)*linspace(0,1,nx);
%%

p=Boundary(p,Mid);

%%

list=[];
j=2:nx-1;
i=2:ny-1;
tic
for it=1:niter
    pn=p;
    p(i,j)=((dy^2*(pn(i+1,j)+pn(i-1,j)))+(dx^2*(pn(i,j+1)+pn(i,j-1))))/(2*(dx^2+dy^2));
    
    p=Boundary(p,Mid);
%     if(mod(it,1e4)==0)
%         toc
%         plot(p(1,:));
%         Xlim=xlim;
%         
%         xlim([0,size(p,2)/4]);
%         out1=Fit_Line(1);
%         xlim([3*size(p,2)/4,size(p,2)]);
%         out2=Fit_Line(1);
%         list(end+1)=[out1.fit(1)./out2.fit(1)]
%         xlim(Xlim)
%         plot(list)
%         drawnow
%     end
end

%%
%Plotting the solution
surf(x,y,p,'EdgeColor','none');
shading interp
title({'2-D Laplace''s equation';['{\itNumber of iterations} = ',num2str(it)]})
xlabel('Spatial co-ordinate (x) \rightarrow')
ylabel('{\leftarrow} Spatial co-ordinate (y)')
zlabel('Solution profile (P) \rightarrow')
% %%
% % imagesc(del2(p));
% % colorbar
% plot(p(1,:));
% Xlim=xlim;
% 
% xlim([0,size(p,2)/4]);
% out1=Fit_Line(1);
% xlim([3*size(p,2)/4,size(p,2)]);
% out2=Fit_Line(1);
% [out1.fit(1)./out2.fit(1)]
% xlim(Xlim)