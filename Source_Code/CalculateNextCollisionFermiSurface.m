function Balls=CalculateNextCollisionFermiSurface(Balls,Walls)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

FullPolygons=DuplicatePolygons(Balls.Polygon,[Balls.Xc(Balls.Active,Balls.Step)';Balls.Yc(Balls.Active,Balls.Step)']);
% FullPolygons=FullPolygons(:,1:end-1);
[PolyNumber,PolyIndex,WallNumber,X,Y]=GetPolyTrajectroyIntersetions(FullPolygons,Balls.WallsPolygon,size(Balls.Polygon,2));

% %FAST FIX!
NOTNAN=find(~isnan(PolyNumber));
PolyNumber=PolyNumber(NOTNAN);
PolyIndex=PolyIndex(NOTNAN);
WallNumber=WallNumber(NOTNAN);
X=X(NOTNAN);
Y=Y(NOTNAN);

RandLength=exprnd(Balls.lMR(Balls.Active,Balls.Step));
LmrIndexList=PolyLengthToIndex(Balls.Polygon,PolyIndexToLength(Balls.Polygon,Balls.PolyIndex(Balls.Active,Balls.Step))+RandLength);
ExtraRotations=[PolyNumber*0;floor(RandLength./Balls.PolyLength)];
PolyNumber=[PolyNumber;[1:numel(LmrIndexList)]'];
PolyIndex=[PolyIndex;LmrIndexList];


[PolyNumber,SortIDX]=sort(PolyNumber);
PolyIndex=PolyIndex(SortIDX);


IntersectionIndexes=PolyIndex;

ToAdd=find(IntersectionIndexes-1e-6<Balls.PolyIndex(Balls.Active(PolyNumber),Balls.Step));
IntersectionIndexesWithAdd=IntersectionIndexes;
IntersectionIndexesWithAdd(ToAdd)=IntersectionIndexesWithAdd(ToAdd)+size(Balls.Polygon,2)-1;


JDXTemp = accumarray(PolyNumber,IntersectionIndexesWithAdd,[numel(Balls.Active),1],@LocationOfMin);
JDX=JDXTemp;
StartingLocation=find(diff([0;PolyNumber]))-1;
JDX=JDX+StartingLocation;

%
%     [LmrPointsX,LmrPointsY]=PolyIndexToPoint(Polygon,LmrIndexList);
%
%     LmrPointsX=LmrPointsX+Balls.Xc(Balls.Active,Balls.Step);
%     LmrPointsY=LmrPointsY+Balls.Yc(Balls.Active,Balls.Step);

[LMRX,LMRY]=PolyIndexToPoint(Balls.Polygon,mod(LmrIndexList-1,size(Balls.Polygon,2)-1)+1);
X=[X;LMRX+Balls.Xc(Balls.Active,Balls.Step)];
Y=[Y;LMRY+Balls.Yc(Balls.Active,Balls.Step)];
WallNumber=[WallNumber;ones(numel(LmrIndexList),1)*(numel(Walls)-1)];
WallNumber=WallNumber(SortIDX);
X=X(SortIDX);
Y=Y(SortIDX);
ExtraRotations=ExtraRotations(SortIDX);

Balls.X(Balls.Active,Balls.Step+1)        = X(JDX);
Balls.Y(Balls.Active,Balls.Step+1)        = Y(JDX);
Balls.WallNo(Balls.Active,Balls.Step+1)   = WallNumber(JDX);
Balls.PolyIndex1(Balls.Active,Balls.Step)=mod(IntersectionIndexes(JDX)-1,size(Balls.Polygon,2)-1)+1;
Balls.ExtraRotations(Balls.Active,Balls.Step)        = ExtraRotations(JDX);
Balls.Theta1(Balls.Active,Balls.Step)     = PolyKFromPolyIndex(Balls.Polygon,Balls.PolyIndex1(Balls.Active,Balls.Step));
1;
end