function MatrixIn=MatrixRotate90(MatrixIn,NumberOfTimes)
%if complex, rotate the complex as well!!!!!
for i=1:NumberOfTimes
    MatrixIn=permute(fliplr(MatrixIn),[2,1,3:5]);
    if(mean(imag(MatrixIn(:)))~=0)
        MatrixIn=MatrixIn*1i;
    end
end
end