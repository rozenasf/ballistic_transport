function [Done,Balls]=ScatterV2(Balls,Walls,Parameters)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

Done=0;
%IDXStillActiveLogical=([Walls(Balls.WallNo(Balls.Active,Balls.Step+1)).P]'~=-1);
IDXStillActiveLogical=([Walls(Balls.WallNo(Balls.Active,Balls.Step+1)).AbsorbCoeff]'<rand(numel(Balls.Active),1)).*...
(Balls.X(Balls.Active,Balls.Step+1)<=Walls(1).MaxScaled(1)+1) .* ...
(Balls.X(Balls.Active,Balls.Step+1)>=Walls(1).MinScaled(1)-1) .* ...
(Balls.Y(Balls.Active,Balls.Step+1)<=Walls(1).MaxScaled(2)+1) .* ...
(Balls.Y(Balls.Active,Balls.Step+1)>=Walls(1).MinScaled(2)-1);

Dying=Balls.Active(find(~IDXStillActiveLogical));

Balls.FinalWall(Dying)=Balls.WallNo(Dying,Balls.Step+1);
% accumarray(Balls.WallNo(Balls.Active(find(~IDXStillActiveLogical)),Balls.Step+1),1,[4,1])
Balls.Active=Balls.Active(find(IDXStillActiveLogical)); %maybe remove the find

if(isempty(Balls.Active));Done=1;return;end
WallType=[Walls(Balls.WallNo(Balls.Active,Balls.Step+1)).Type]';
ScatterNames=fieldnames(Parameters.WallsBehavior);
% WallScatterType
%%

Logic0=(WallType==0);
Logic1=(WallType==1);
Logic2=(WallType==2);
LogicWalls=((Logic0+Logic1+Logic2)==1);
Logic3=(WallType==3);
%%
Normal=zeros(sum(LogicWalls),1);IsNormalMoreThanPi=Normal;
Normal(:,1)=[Walls(Balls.WallNo(Balls.Active(LogicWalls),Balls.Step+1)).Normal]';
IsNormalMoreThanPi(:,1)=Normal(:,1)>=pi;
Normal(:,1)=mod(Normal(:,1),pi);

Theta1List=Balls.Theta1(Balls.Active(LogicWalls),Balls.Step);
IncidenceAngle=Mod2Pi(  (Balls.Theta1(Balls.Active(LogicWalls),Balls.Step)+pi)  -Normal +pi/2)-pi/2;
NeedNormalFlip=~InRange(IncidenceAngle,-pi/2,pi/2);


WallScatterListForBalls=Balls.Active(LogicWalls);
for i=1:numel(ScatterNames)
   ScatterTypeLogic=find(ismember(Balls.WallNo(Balls.Active(LogicWalls),Balls.Step+1),Parameters.WallsBehavior(1).(ScatterNames{i})));
   
   
   if(isempty(ScatterTypeLogic));ScatterTypeLogic=zeros(0,1);end
   %The next if is used to suppert backwards compatibility before the
   %AlphaCharge new behavior
   RelevantWallNo=Balls.WallNo(Balls.Active(LogicWalls),Balls.Step+1);
   RelevantWallNo=RelevantWallNo(ScatterTypeLogic);
   
    FullIncidenceAngle=IncidenceAngle(ScatterTypeLogic)-pi*NeedNormalFlip(ScatterTypeLogic);
    
   Info=struct(...
       'IncidentAngle',FullIncidenceAngle,... IncidentAngle
       'Direction',xor(NeedNormalFlip(ScatterTypeLogic),IsNormalMoreThanPi(ScatterTypeLogic)),... Dircetion
       'Rc',Balls.R(WallScatterListForBalls(ScatterTypeLogic),Balls.Step),... Rc
       'lMR',Balls.lMR(WallScatterListForBalls(ScatterTypeLogic),Balls.Step),... lMR
       'AlphaCharge',Balls.AlphaCharge(WallScatterListForBalls(ScatterTypeLogic),Balls.Step),...AlphaCharge
       'WallNo',RelevantWallNo); %  Wall Number
       
       if(numel(Parameters.R)>1)
            Info.Polygon=Balls.Polygon;
            Info.Normal=[Walls(Balls.WallNo(Balls.Active(LogicWalls),Balls.Step+1)).Normal]';
            Info.ThetaK=Balls.Theta1(Balls.Active(LogicWalls),Balls.Step);
            Info.Walls=Walls;
            Info.InputAngle=Theta1List(ScatterTypeLogic); %No flip sides in polygon mode
       end
   [Info]=Parameters.WallsBehavior(2).(ScatterNames{i})(Info);
if(numel(Parameters.R)>1)
    Balls.Theta(WallScatterListForBalls(ScatterTypeLogic),Balls.Step+1)=Info.OutputAngle;
else
    Balls.Theta(WallScatterListForBalls(ScatterTypeLogic),Balls.Step+1)=Info.ReflectionAngle+Normal(ScatterTypeLogic)+pi*NeedNormalFlip(ScatterTypeLogic); 
end
   
    Balls.R  (WallScatterListForBalls(ScatterTypeLogic),Balls.Step+1)=Info.Rc;
    Balls.lMR(WallScatterListForBalls(ScatterTypeLogic),Balls.Step+1)=Info.lMR;
    Balls.AlphaCharge(WallScatterListForBalls(ScatterTypeLogic),Balls.Step+1)=Info.AlphaCharge;
end

%% Type 3
ScatterdBalls=(Balls.Active(Logic3));
if(~isfield(Parameters,'leeP'));Parameters.leeP=0;end
 IDXHydro=(rand(numel(ScatterdBalls),1)<Parameters.leeP);

Balls.Theta(ScatterdBalls(IDXHydro),Balls.Step+1)=pi+Balls.Theta1(ScatterdBalls(IDXHydro),Balls.Step);%+...
Balls.R(ScatterdBalls(IDXHydro),Balls.Step+1)=Balls.R(ScatterdBalls(IDXHydro),Balls.Step);
Balls.lMR(ScatterdBalls(IDXHydro),Balls.Step+1)=Balls.lMR(ScatterdBalls(IDXHydro),Balls.Step);
Balls.AlphaCharge(ScatterdBalls(IDXHydro),Balls.Step+1)=-Balls.AlphaCharge(ScatterdBalls(IDXHydro),Balls.Step);
% 
Balls.Theta(ScatterdBalls(~IDXHydro),Balls.Step+1)=Balls.Theta1(ScatterdBalls(~IDXHydro),Balls.Step)+Parameters.ThetaScatter(sum((~IDXHydro)));%+...
Balls.R(ScatterdBalls(~IDXHydro),Balls.Step+1)=Balls.R(ScatterdBalls(~IDXHydro),Balls.Step);
Balls.lMR(ScatterdBalls(~IDXHydro),Balls.Step+1)=Balls.lMR(ScatterdBalls(~IDXHydro),Balls.Step);
Balls.AlphaCharge(ScatterdBalls(~IDXHydro),Balls.Step+1)=Balls.AlphaCharge(ScatterdBalls(~IDXHydro),Balls.Step);

if(Parameters.leeP>0)
    NumelHyrdoEvents=sum(IDXHydro);
    if(numel(ScatterdBalls(IDXHydro))+Balls.HydroEvents.Location>numel(Balls.HydroEvents.X))
        Balls.HydroEvents.X=[Balls.HydroEvents.X;nan(10*Parameters.N,1)];
        Balls.HydroEvents.Y=[Balls.HydroEvents.Y;nan(10*Parameters.N,1)];
        Balls.HydroEvents.Theta=[Balls.HydroEvents.Theta;nan(10*Parameters.N,1)];
    end
   Balls.HydroEvents.X( (Balls.HydroEvents.Location+1) : (Balls.HydroEvents.Location+NumelHyrdoEvents) ) = Balls.X(ScatterdBalls(IDXHydro),Balls.Step+1);
   Balls.HydroEvents.Y( (Balls.HydroEvents.Location+1) : (Balls.HydroEvents.Location+NumelHyrdoEvents) ) = Balls.Y(ScatterdBalls(IDXHydro),Balls.Step+1);
   Balls.HydroEvents.Theta( (Balls.HydroEvents.Location+1) : (Balls.HydroEvents.Location+NumelHyrdoEvents) ) = Balls.Theta(ScatterdBalls(IDXHydro),Balls.Step+1);
   Balls.HydroEvents.Location=Balls.HydroEvents.Location+NumelHyrdoEvents;
end

%     Parameters.HydroScatter(Balls.Theta1(Logic3,Balls.Step+1),round(Balls.X(Logic3,Balls.Step+1)),round(Balls.Y(Logic3,Balls.Step+1)));
% Balls.Theta(Balls.Active(Logic3),Balls.Step+1)=Balls.Theta1(Balls.Active(Logic3),Balls.Step)+(rand(1)-0.5)*Parameters.ThetaScatterMR*ones(sum(Logic3),1);
%% mod 2pi
Balls.Theta(Balls.Active,Balls.Step+1)=Mod2Pi(Balls.Theta(Balls.Active,Balls.Step+1));
%% Temporery take care of Rc
% Balls.R(Balls.Active,Balls.Step+1)=Balls.R(Balls.Active,Balls.Step);
IDXFlipDir=Balls.R(Balls.Active,Balls.Step+1)>0;
Balls.Dir(Balls.Active,Balls.Step+1)=Balls.Dir(Balls.Active,Balls.Step).*(IDXFlipDir*2-1);
Balls.R(Balls.Active,Balls.Step+1)=abs(Balls.R(Balls.Active,Balls.Step+1));
end