function ThetaKOut=RandomContactLeakProtected(n,X,Y,Polygon,Flip)

ThetaKOut=zeros(n,1);
NotLegal=ones(n,1)==1;
while(sum(NotLegal>0))
    ThetaKOut(NotLegal) =  asin(rand(sum(NotLegal),1)*2-1)+PerpendicularAngle(X,Y)+pi*Flip;
    NotLegal=AngDiff(PolyAngleFromPolyK(Polygon,ThetaKOut)',PerpendicularAngle(X,Y))>=pi/2;
end
end