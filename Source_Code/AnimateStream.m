function AnimateStream(ICurrent,Stream)
Speed=mean(mean(abs(ICurrent.data)));
%Speed=-1;
    
[x,y]=meshgrid(ICurrent.axes{2}.data,ICurrent.axes{1}.data);
iverts = interpstreamspeed(y',x',real(ICurrent.data)'/Speed,imag(ICurrent.data)'/Speed,Stream,.05);
streamparticles(iverts, 100, ...
    'Animate',15,'FrameRate',40, ...
    'MarkerSize',5,'MarkerFaceColor',[0 .5 0])
end