function PlotGeometry(Geometry)
clf
GeometrySpliced=reshape([Geometry{:}],2,2,numel([Geometry{:}])/4);
X=squeeze(GeometrySpliced(1,:,:));Y=squeeze(GeometrySpliced(2,:,:));
plot(X,Y,'linewidth',3);
Ax=gca;Lines=Ax.Children;
Colormap=jet(numel(Lines));
for i=1:numel(Lines)
    Lines(i).Color=Colormap(i,:);
end
for i=1:size(X,2)
   text(mean(X(:,i)),mean(Y(:,i)),num2str(i),'FontSize',16) 
end
end