function Balls=InitializeBallsFermiSurface(Walls,Parameters)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

N=Parameters.N;MaxCollisions=Parameters.MaxCollisions;
Balls=struct('X',nan(N,MaxCollisions),'Y',nan(N,MaxCollisions),...
    'Theta',nan(N,MaxCollisions),'Theta1',nan(N,MaxCollisions),...
    'PolyIndex',nan(N,MaxCollisions),'PolyIndex1',nan(N,MaxCollisions),...
    'Xc',nan(N,MaxCollisions),'Yc',nan(N,MaxCollisions),...
    'lMR',nan(N,MaxCollisions),...
    'WallNo',nan(N,MaxCollisions),'Done',zeros(N,1),...
    'ExtraRotations',zeros(N,MaxCollisions),'AlphaCharge',nan(N,MaxCollisions),'AccumulatedLength',nan(N,MaxCollisions));


Balls=InitializeGenericBalls(Walls,Parameters,Balls);

%Temp fix!
% Balls.WallsPolygon=Parameters.WallsCoordinates.Polygons;
XX=[[Walls.X];nan(1,size([Walls.X],2))];
YY=[[Walls.Y];nan(1,size([Walls.Y],2))];
Balls.WallsPolygon=[XX(:)';YY(:)'];



Balls.Polygon=Parameters.R./Parameters.GridSize;
Balls.PolyLength=sum((sqrt(( Balls.Polygon(1,1:(end-1))- Balls.Polygon(1,2:end)).^2+( Balls.Polygon(2,1:(end-1))- Balls.Polygon(2,2:end)).^2)));
Balls.ThetaPolygon=ThetaPolygonFromPolygon(Balls.Polygon);
Balls.PolyIndex(:,1)=PolyIndexFromPolyK(Balls.Polygon,Balls.Theta(:,1));

Balls.Dir(:,1)=ones(N,1); %TEMP
Balls.R(:,1)=inf*ones(N,1); %TEMP

[Xc,Yc]=PolyCenterFromIndexLocation(Balls.Polygon,Balls.X(:,1),Balls.Y(:,1),Balls.PolyIndex(:,1));
Balls.Xc(:,1)=Xc;
Balls.Yc(:,1)=Yc;
% Balls.Kf=Parameters.Kf;
end