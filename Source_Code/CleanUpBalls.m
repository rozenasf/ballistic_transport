function Balls=CleanUpBalls(Balls)
% names={'X','Y','Theta','Theta1','Dir','lMR','R','WallNo','AlphaCharge'};
% if(isfield(Balls,'Xc'));
%     names=[names,{'Phi','Phi1','Xc','Yc','ExtraRotations'}];
% else
%     names=[names,{'m','n'}];
% end
% for i=1:numel(names)
%     Balls.(names{i})(:,1)=Balls.(names{i})(:,end-1);
%     Balls.(names{i})(:,2:end)=nan;
% end
% Balls.Step=1;

    Names=fieldnames(Balls);
    for i=1:numel(Names)
       if(size(Balls.(Names{i}),2)==size(Balls.X,2))
          Balls.(Names{i})=[];
       end
    end
  

end
