function Index=GetNextIndexFromCluster(CoreIndex)
persistent KillTic
if(isempty(KillTic));KillTic=tic;end
    pause(5*rand);
    [~,IndexSTR]=...
        system(['>>track.dat;NextJob=$(sort track.dat -g | awk ',...
        char(39),'{for(i=p+1; i<$1; i++) print i} {p=$1} END {print $1 +1}',char(39),...
        '| head -n 1);echo $NextJob ',num2str(CoreIndex),' ',sprintf('%0.5f',toc(KillTic)),...
        '>> track.dat; echo $NextJob']);
    Index=str2num(IndexSTR);
end