function out=Real2Scale(in,Axis,Parameters,Walls)
    out=(in-Walls(1).Min(Axis))./Parameters.GridSize+1;
end