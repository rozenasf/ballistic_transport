classdef AxisMatrix < handle
    properties
        Matrix
        XAxis
        YAxis
        SizeX
        SizeY
        MinX
        MaxX
        MinY
        MaxY
        BoxX
        BoxY
    end
    methods
        function obj=AxisMatrix(Matrix,XAxis,YAxis)
            %X is second axis
            obj.Matrix=Matrix;
            obj.XAxis=XAxis;
            obj.YAxis=YAxis;
            obj.SizeX=size(Matrix,2);
            obj.SizeY=size(Matrix,1);
            obj.MinX=min(XAxis);
            obj.MinY=min(YAxis);
            obj.MaxX=max(XAxis);
            obj.MaxY=max(YAxis);
            obj.BoxX=[obj.MinX,obj.MaxX]+[-0.5,0.5]*(obj.MaxX-obj.MinX)/(obj.SizeX-1);
            obj.BoxY=[obj.MinY,obj.MaxY]+[-0.5,0.5]*(obj.MaxY-obj.MinY)/(obj.SizeY-1);
        end
        function obj=AddBoundary(obj,XLength,YLength)
            
        end
        function Real=Int2Real(obj,IntX,IntY)
            Real=obj.XAxis(IntX)+1i*obj.YAxis(IntY);
        end
        function [XIdx,YIdx]=Real2Int(obj,RealX,RealY)
            XIdx= (floor( (RealX-obj.BoxX(1))/diff(obj.BoxX)*obj.SizeX )+1);
            YIdx= (floor( (RealY-obj.BoxY(1))/diff(obj.BoxY)*obj.SizeY )+1);
        end
        function out=ExtractFromMatrixIndex(obj,XIdx,YIdx)
            IDX=obj.SizeY*(XIdx-1)+YIdx;
            IDX(IDX>numel(obj.Matrix))=1;
            IDX(IDX<1)=1;
            out=obj.Matrix(IDX);
            out(find((XIdx>obj.SizeX)+(XIdx<1)))=0;
            out(find((YIdx>obj.SizeY)+(YIdx<1)))=0;
        end
        function out=ExtractFromMatrix(obj,X,Y)
            [XIdx,YIdx]=Real2Int(obj,X,Y);
            out=ExtractFromMatrixIndex(obj,XIdx,YIdx);
        end
    end
end