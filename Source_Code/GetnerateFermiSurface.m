function Polygon=GetnerateFermiSurface(Angle,K,Rotation)
%% Theta Fermi Surface
% KThetaPolygonBase=[0,pi/6-0.04,pi/6+0.04,pi/3;sqrt(3)/2,1,1,sqrt(3)/2]; %interesting hexagon
 %KThetaPolygonBase=[0,pi/6-0.1,pi/6+0.1,pi/3;1,0.7,0.7,1]; %regular hexagon
 KThetaPolygonBase=[Angle;K];
 Symmetry=round(2*pi./Angle(end));
KThetaPolygon=[];
for i=1:Symmetry
    KThetaPolygon=[KThetaPolygon, [ KThetaPolygonBase(:,(1:end-1)) ]+repmat([1;0]*(i-1)*2*pi/(Symmetry),1,size(KThetaPolygonBase,2)-1) ];
end
 KThetaPolygon=[KThetaPolygon,KThetaPolygon(:,1)];
% from polar to cartezian
KPolygon=[sin(KThetaPolygon(1,:)).*KThetaPolygon(2,:);cos(KThetaPolygon(1,:)).*KThetaPolygon(2,:)];
XPolygon=[KPolygon(2,:);-KPolygon(1,:)];

XPolygon=RotatePolygon(XPolygon,Rotation);
ThetaPolygon=ThetaPolygonFromPolygon(XPolygon);
Polygon=XPolygon;
[~,b]=min(ThetaPolygon);
IDXList=mod(-2+b+(1:numel(ThetaPolygon)),numel(ThetaPolygon))+1;
Polygon=Polygon(:,IDXList);
ThetaPolygon=ThetaPolygonFromPolygon(Polygon);
Polygon=[Polygon,Polygon(:,1)];
%  plot(XPolygon(1,:),XPolygon(2,:))

end