Blist=[0.25,0.25];
Density={};Current={};
Elist=[1,3];
ChannelWidth=5;
    ParametersArray=struct('P',0,'lMR',4,'ThetaScatterMR',2*pi,...
        'MaxCollisions',1e4,'N',1e3,'R',{ChannelWidth/4,-ChannelWidth/4},'MaxMument',6,'Absorbers',[1,7],...
        'AbsorbCoeff',1,...
        'Emitter',{1,7},'EmitterRange',[0,1],...
        'EmittedFunciton',{@()asin(rand*2-1),@()asin(rand*2-1)+pi},...
        'GridSize',0.1,'Charge',{1,-1},'ScatterFunction',@(n)asin(rand(n,1)*2-1)/pi+0.5, ...
        'FillerNumber',200,'Scramble',0.5);%@(n)rand(n,1)
     ParametersArray=repmat(ParametersArray,1,1);
    Y=[-10,-ChannelWidth/2,ChannelWidth/2,10];
    X=[-15,-5,5,15];
   
WallsCoordinates={[X(1),Y(1);X(1),Y(4);X(2),Y(4);X(2),Y(3);...
    X(3),Y(3);X(3),Y(4);X(4),Y(4);X(4),Y(1);X(3),Y(1);X(3),Y(2);...
    X(2),Y(2);X(2),Y(1);X(1),Y(1)]'};

%     figure(1);
    RunSimulation;
    %ContinueSimulation
    
%     figure(1)
%     q=Current{1}-Current{2};plot(real(q.roi(1,0.2*L,0.8*L).mean(1)));
%     figure(2)
%     q=Density{1}-Density{2};plot(real(q.roi(1,0.2*L,0.8*L).mean(1)));
%     
%     pause(eps)

    Stream=PlotSystem(IDensity,ICurrent,Walls,Parameters.Emitter);
    drawnow