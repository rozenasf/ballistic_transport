function out=AddSimulationsWeighted(Simulations,Weight)

out=Simulations{1};

Fieldsnames={'IDensity','data';'ICurrent','data';'Distribution','Counter';'Distribution','Data';'ICounter','data';'Accumulator',[]};

for j=1:size(Fieldsnames,1)
    if(~isempty(Fieldsnames{j,2})) %assume struct
        if(~isfield(out,Fieldsnames{j,1}));continue;end
        out.(Fieldsnames{j,1}).(Fieldsnames{j,2})=out.(Fieldsnames{j,1}).(Fieldsnames{j,2})*0;
        
        for i=1:numel(Simulations)
            out.(Fieldsnames{j,1}).(Fieldsnames{j,2})=out.(Fieldsnames{j,1}).(Fieldsnames{j,2})+...
                Weight(i)*Simulations{i}.(Fieldsnames{j,1}).(Fieldsnames{j,2});
         end
    else %assume cell
         if(~isfield(out,Fieldsnames{j,1}));continue;end
        for k=1:numel(out.(Fieldsnames{j,1}))
            out.(Fieldsnames{j,1}){k}=out.(Fieldsnames{j,1}){k}*0;
            
            for i=1:numel(Simulations)
                out.(Fieldsnames{j,1}){k}=out.(Fieldsnames{j,1}){k}+...
                    Weight(i)*Simulations{i}.(Fieldsnames{j,1}){k};
            end
        end
    end
end
end