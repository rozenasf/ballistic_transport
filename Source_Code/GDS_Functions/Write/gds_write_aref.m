function gds_write_aref (gdsFile, srefName, varargin)

p = inputParser;

p.addRequired('gdsFile', @(x)isempty(ferror(x)));
p.addRequired('srefName', @ischar);
p.addParamValue('refPoint', [0;0], @(x)isreal(x) && size(x,1) == 2 && size(x,2) == 1);
p.addParamValue('columns', 1, @(x)isscalar(x) && isreal(x) && mod(x,1) == 0 && x > 0);
p.addParamValue('columnOffset', [0;0], @(x)isreal(x) && (isscalar(x) || size(x,1) == 2 && size(x,2) == 1));

p.addParamValue('rows', 1, @(x)isscalar(x) && isreal(x) && mod(x,1) == 0 && x > 0);
p.addParamValue('rowOffset', [0;0], @(x)isreal(x) && (isscalar(x) || size(x,1) == 2 && size(x,2) == 1));

p.addParamValue('reflection', false, @(x)isscalar(x) && islogical(x));
p.addParamValue('absMagnification', false, @(x)isscalar(x) && islogical(x));
p.addParamValue('absAngle', false, @(x)isscalar(x) && islogical(x));
p.addParamValue('magnification', 1.0, @(x)isreal(x) && x > 0);
p.addParamValue('angle', 0.0, @(x)isreal(x) && x >= 0 && x < 360.0);

p.parse(gdsFile, srefName, varargin{:});

p = p.Results;

if isscalar(p.columnOffset)
    p.columnOffset = [p.columnOffset; 0];
end

if isscalar(p.rowOffset)
    p.rowOffset = [0; p.rowOffset];
end

% AREF record

fwrite(gdsFile, 4, 'uint16');
fwrite(gdsFile, 11, 'uint8');
fwrite(gdsFile, 0, 'uint8');

% SNAME record
if mod(length(p.srefName),2) == 1
    p.srefName = [p.srefName 0];
end

fwrite(gdsFile, 4+length(p.srefName), 'uint16');
fwrite(gdsFile, 18, 'uint8');
fwrite(gdsFile, 6, 'uint8');

fwrite(gdsFile, p.srefName, 'uint8');

if any([p.reflection p.absMagnification p.absAngle p.magnification~=1.0 p.angle~=0.0])

    % STRANS record
    
    strans = 0;
    
    if p.reflection
        strans = bitor(strans, 2^15);
    end
    
    if p.absMagnification
        strans = bitor(strans, 2^2);
    end
    
    if p.absAngle
        strans = bitor(strans, 2^1);
    end

    fwrite(gdsFile, 6, 'uint16');
    fwrite(gdsFile, 26, 'uint8');
    fwrite(gdsFile, 1, 'uint8');

    fwrite(gdsFile, strans, 'uint16');

    
    if p.magnification ~= 1.0
        
        % MAG record

        fwrite(gdsFile, 12, 'uint16');
        fwrite(gdsFile, 27, 'uint8');
        fwrite(gdsFile, 5, 'uint8');
        
        gds_write_real8(gdsFile, p.magnification);
    end
    
    if p.angle ~= 0.0
        
        % ANGLE record

        fwrite(gdsFile, 12, 'uint16');
        fwrite(gdsFile, 28, 'uint8');
        fwrite(gdsFile, 5, 'uint8');
        
        gds_write_real8(gdsFile, p.angle);
    end
end

% COLROW record

fwrite(gdsFile, 8, 'uint16');
fwrite(gdsFile, 19, 'uint8');
fwrite(gdsFile, 2, 'uint8');

fwrite(gdsFile, p.columns, 'int16');
fwrite(gdsFile, p.rows, 'int16');

% XY record

xy = [p.refPoint p.refPoint+p.columns*p.columnOffset p.refPoint+p.rows*p.rowOffset];

xy = reshape(xy, 1, []);

fwrite(gdsFile, 4+4*length(xy), 'uint16');
fwrite(gdsFile, 16, 'uint8');
fwrite(gdsFile, 3, 'uint8');

fwrite(gdsFile, round(1000*xy), 'int32');

% ENDEL record

fwrite(gdsFile, 4, 'uint16');
fwrite(gdsFile, 17, 'uint8');
fwrite(gdsFile, 0, 'uint8');

    