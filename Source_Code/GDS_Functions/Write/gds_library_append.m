function gdsFile = gds_library_append (libName)

gdsFile = fopen([libName '.gds'], 'r+', 'ieee-be');

% for the moment, I expect that the last record is ENDLIB record, so
% I need to seek four bytes from the end of the file

fseek(gdsFile, -4, 'eof');