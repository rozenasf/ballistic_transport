function gds_close_library (gdsFile)

% ENDLIB record
fwrite(gdsFile, 4, 'uint16');
fwrite(gdsFile, 4, 'uint8');
fwrite(gdsFile, 0, 'uint8');

fclose(gdsFile);