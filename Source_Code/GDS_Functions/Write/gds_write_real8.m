function gds_write_real8(fid, value)

if value < 0
    sign = 1;
else
    sign = 0;
end

expFit = floor((56-log(value)/log(2))/4);

exponent = -expFit+64+14;
mantise = round(value*16^expFit);

fwrite(fid, sign, 'ubit1');
fwrite(fid, exponent, 'ubit7');
fwrite(fid, mantise, 'ubit56');

