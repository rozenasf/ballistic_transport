gdsFile = gds_create_library('first');

gds_start_structure(gdsFile, 'my structure');

myRect = struct('points', [0 0 1 1 0; 0 1 1 0 0], 'dataType', 3, 'layer', 5);

myShapes = [myRect setfield(applyTransform(myRect, 'translate(5,5) rotate(45)'), 'layer', 2)];

gds_write_boundaries(gdsFile, myShapes);

gds_close_structure(gdsFile);


gds_start_structure(gdsFile, 'my structure 2');

myRect = struct('points', [0 0 1 1 0; 0 1 1 0 0], 'dataType', 3, 'layer', 5);


myShapes = [];

for k = 1:10
        myShapes = [myShapes applyTransform(myRect, sprintf('translate(%d 0)', k*5))];
end

gds_write_boundaries(gdsFile, myShapes);

gds_close_structure(gdsFile);


gds_start_structure(gdsFile, 'my structure 3');

gds_write_sref(gdsFile, 'my structure', 'refPoint', [10 ;10]);
gds_write_sref(gdsFile, 'my structure', 'refPoint', [0 ;10]);
gds_write_sref(gdsFile, 'my structure', 'refPoint', [0 ;0]);

gds_write_aref(gdsFile, 'my structure', 'columns', 3, 'columnOffset', [10; 0], 'rows', 2, 'rowOffset', [0 ; 6], 'refPoint', [0; 20]);

gds_write_boundaries(gdsFile, str2boundaries('ERAN', 'letterHeight', 12, 'fontName', '5x7 pixels', 'hAlign', 'center'));

gds_close_structure(gdsFile);


gds_start_structure(gdsFile, 'my structure 4');

rect = struct('points', [-1 -1 1 1 -1 ; -1 1 1 -1 -1], 'dataType', 0, 'layer', 3);

t = 0:(2*pi/36):2*pi;
circ = struct('points', [cos(t(1:end-1)) 1; sin(t(1:end-1)) 0], 'dataType', 1, 'layer', 0);
circ = applyTransform(circ, 'translate(0.5 0.5)');

gds_write_boundaries(gdsFile, gpcMultiClip(rect, circ, GPC_DIFF));


gds_close_structure(gdsFile);

% from clipping\ directory
% mex gpcMultiClip.c

gds_close_library(gdsFile);