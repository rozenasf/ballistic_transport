function m = trScale(sx, sy)

if nargin < 2
    
    sy = sx;
end

m = [ sx 0 0 ; 0 sy 0 ; 0 0 1 ];