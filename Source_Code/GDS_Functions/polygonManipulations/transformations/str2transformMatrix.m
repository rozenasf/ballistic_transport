function matrix = str2transformMatrix(transformStr)

matrix = eye(3);

transformStr = lower(transformStr);
atomicTransformations = regexp(transformStr, '(\w*)\s*\((.*?)\)', 'tokens');

for transformInd = 1:length(atomicTransformations)

    matrix = matrix * genAtomicTransformation( ...
        atomicTransformations{transformInd}{1}, ...
        atomicTransformations{transformInd}{2} );
end

function matrix = genAtomicTransformation(kindStr, paramsStr)


numericParams = sscanf(regexprep(paramsStr, ',', ' '), '%f')';

switch kindStr
    case 'matrix'
        
        matrix = trMatrix(numericParams(1), numericParams(3), numericParams(5), numericParams(2), numericParams(4), numericParams(6));

    case 'translate'
        
        if length(numericParams) == 1
            numericParams = [numericParams 0];
        end
        
        matrix = trTranslate(numericParams(1), numericParams(2));

    case 'scale'
        
        if length(numericParams) == 1
            numericParams = [numericParams numericParams];
        end
        
        matrix = trScale(numericParams(1), numericParams(2));

    case 'rotate'
    
        if length(numericParams) == 1
            numericParams = [numericParams 0 0];
        end
        
        matrix = trRotate(numericParams(1), numericParams(2), numericParams(3));
        
    case 'skewx'
    
        matrix = trSkewX(numericParams(1));

    case 'skewy'
    
        matrix = trSkewY(numericParams(1));

    case 'xflip'
        
        matrix = trXFlip;
        
    case 'yflip'
        
        matrix = trYFlip;
        
    case 'rotate345'
        
        matrix = [0.8 -0.6 0; 0.6 0.8 0; 0 0 1];
        
    case 'rotate90'
        
        matrix = [0 -1 0; 1 0 0; 0 0 1];
        
    otherwise
        
        fprintf('what the fuck?!\n');
end
        
