function m = trMatrix(xx, yx, dx, xy, yy, dy)

m = [ xx yx dx ; xy yy dy ; 0 0 1 ];