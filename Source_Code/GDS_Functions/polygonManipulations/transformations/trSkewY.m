function m = trSkewY(ang)

t = tan(ang/180*pi);

m = [ 1 0 0 ; t 1 0 ; 0 0 1 ];