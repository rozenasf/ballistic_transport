function pOut = fishTransform(pIn, R)

if isstruct(pIn)
    
    pOut = pIn;
    
    for pInd = 1:length(pOut)
       
        pOut(pInd).points = innerFishTransform(pOut(pInd).points, R);
    end
else
    
    pOut = innerFishTransform(pIn, R);
end

function pOut = innerFishTransform(pIn, R)

r = sqrt(pIn(1,:).^2+pIn(2,:).^2);
r(find(r == 0)) = inf;

ratio = R./r.*atan(r/R);

pOut = [pIn(1,:).*ratio ; pIn(2,:).*ratio];


