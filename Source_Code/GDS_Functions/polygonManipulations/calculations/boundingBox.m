function bbox = boundingBox(pIn)
%  bbox = boundingBox(pIn)
%
%    calculating the bounding box of a polygon[s]
%
%
%    pIn - (REQURED) a polygon or an array of polygons. a polygon in this
%          this context simply means a structure with a field named
%          "points", which should be 2 (rows) by n (coloumns) values. First
%          row contains the x values, second holds the y values
%
%
%    bbox - (OUTPUT) a 4 values vector that contains the bounding box:
%                    
%           [ minX minY maxX maxY ]
%

if isstruct(pIn)
    
    bbox = [inf inf -inf -inf];
    
    for p = pIn
        bbox = [min([p.points(1,:) bbox(1)]) min([p.points(2,:) bbox(2)]) max([p.points(1,:) bbox(3)]) max([p.points(2,:) bbox(4)])];
        
    end
    
else
   
    bbox = [min(pIn(1,:)) min(pIn(2,:)) max(pIn(1,:)) max(pIn(2,:))];
    
end