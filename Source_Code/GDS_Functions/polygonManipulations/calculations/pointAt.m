function p = pointAt(points, distance, fromTail)

if nargin == 2
    fromTail = false;
end

p = [];

if isstruct(points)
    points = points.points;
end

[totalLength,allLengths] = pathLength(points);

if abs(distance) > totalLength
    
    return;
end

if fromTail

    distance = totalLength - distance;
end

if distance == totalLength
    p = points(:,end);
    return;
end

pointBefore = max(find(allLengths <= distance));
ps = [pointBefore pointBefore+1];

p = [polyval(polyfit(allLengths(ps), points(1,ps), 1), distance) ; ...
    polyval(polyfit(allLengths(ps), points(2,ps), 1), distance) ];

