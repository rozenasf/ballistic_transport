function flatStructure = gds_flat_structure(gdsStructures, rootStructureName)

%fprintf('>>> gds_flat_structure [%s]\n', rootStructureName);

flatSturcture = [];

structureIndex = strmatch(rootStructureName, {gdsStructures.name}, 'exact');

if isempty(structureIndex)
    
    return;
end

flatStructure.name = rootStructureName;

% now we start with taking the boundaries which are part of the root
% structure
flatStructure.plgs = []; 

if isempty(gdsStructures(structureIndex).elements)
    return;
end

flatStructure.plgs = [gdsStructures(structureIndex).elements(find([gdsStructures(structureIndex).elements.kind] == GDS_BOUNDARY)).element];

% now I collect all the reference elements
srefElements = [gdsStructures(structureIndex).elements(find([gdsStructures(structureIndex).elements.kind] == GDS_SREF)).element];
arefElements = [gdsStructures(structureIndex).elements(find([gdsStructures(structureIndex).elements.kind] == GDS_AREF)).element];

if isempty(srefElements) && isempty(arefElements)
    
else
    
    refNames = {};
    if ~isempty(srefElements)
        refNames = { refNames{:} srefElements.sName };
    end
    
    if ~isempty(arefElements)
        refNames = { refNames{:} arefElements.sName };
    end
    
    sortedNames = sort( refNames );
    distinctNames = { sortedNames{1} };
    for name = sortedNames
       if ~strcmp(name{1}, distinctNames{end})
           distinctNames = { distinctNames{:} name{1} };
       end
    end

    refStructures = [];
    for name = distinctNames
        
        refStructures = [refStructures gds_flat_structure(gdsStructures, name{1})];
    end
    
    % now we go over the reference elements, one by one
    for srefElement = srefElements

        refIndex = strmatch(srefElement.sName, {refStructures.name}, 'exact');
        
        transformation = eye(3);
        
        if srefElement.magnification ~= 1.0
            transformation = diag([srefElement.magnification srefElement.magnification 1]);
        end
           
        if srefElement.reflection
            transformation = [1 0 0; 0 -1 0; 0 0 1] * transformation;
        end
        
        if srefElement.angle ~= 0.0
            C = cos(srefElement.angle/180*pi);
            S = sin(srefElement.angle/180*pi);
            
            transformation = [C -S 0; S C 0; 0 0 1] * transformation;
        end
        
        transformation(1,3) = srefElement.refPoint(1);
        transformation(2,3) = srefElement.refPoint(2);

        if ~isempty(refStructures(refIndex).plgs)
            flatStructure.plgs = [flatStructure.plgs applyTransform(refStructures(refIndex).plgs, ...
                transformation)];
        end
    end    
    
    % now we go over the reference elements, one by one
    for arefElement = arefElements

        refIndex = strmatch(arefElement.sName, {refStructures.name}, 'exact');
        
        transformation = eye(3);
        
        if arefElement.magnification ~= 1.0
            transformation = diag([arefElement.magnification arefElement.magnification 1]);
        end
           
        if arefElement.reflection
            transformation = [1 0 0; 0 -1 0; 0 0 1] * transformation;
        end
        
        if arefElement.angle ~= 0.0
            C = cos(arefElement.angle/180*pi);
            S = sin(arefElement.angle/180*pi);
            
            transformation = [C -S 0; S C 0; 0 0 1] * transformation;
        end
        
        for column = 0:arefElement.columns-1
            for row = 0:arefElement.rows-1
                transformation(1,3) = arefElement.refPoint(1)+column*arefElement.interColumnsSpacing(1)+row*arefElement.interRowsSpacing(1);
                transformation(2,3) = arefElement.refPoint(2)+column*arefElement.interColumnsSpacing(2)+row*arefElement.interRowsSpacing(2);

                flatStructure.plgs = [flatStructure.plgs applyTransform(refStructures(refIndex).plgs, ...
                    transformation)];
            end
        end
        
    end    
    
end