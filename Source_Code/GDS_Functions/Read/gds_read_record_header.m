function vRecord = gds_read_record(fid)

    vRecord.length = gds_read_uint2(fid);
    vRecord.recKind = fread(fid, 1, 'uint8');
    vRecord.valKind = fread(fid, 1, 'uint8');

    fread(fid, vRecord.length-4, 'uchar');
end

