function vReal8 = gds_read_real8(fid)

    sign = fread(fid, 1, 'ubit1');
    exponent = fread(fid, 1, 'ubit7');
    mantisa = fread(fid, 1, 'ubit56');
    
    vReal8 = (1-2*sign)*mantisa*16^(exponent-64-14);

    