function [Accumulator,Distribution]=AddToAccumulatorWithField(Balls,Parameters,AccumulatorSize)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

Distribution=[];
Accumulator={};
for i=1:Parameters.MaxMument
    Accumulator{i}=zeros(AccumulatorSize);
end

Distribution=zeros([AccumulatorSize,Parameters.Distribution]);


tic

LogicDir=(Balls.Dir(:)==1);
IDXShiftPositive=Balls.Phi1(:)>Balls.Phi(:);
Balls.Phi1(LogicDir & IDXShiftPositive)=Balls.Phi1(LogicDir & IDXShiftPositive)-2*pi;
Balls.Phi1(~LogicDir & ~IDXShiftPositive)=Balls.Phi1(~LogicDir & ~IDXShiftPositive)+2*pi;


tic
if(~isfield(Parameters,'Slice'));Parameters.Slice=2;end
Slice=round(linspace(1,size(Balls.Xc,1)+1,Parameters.Slice));
for k=1:(numel(Slice)-1)
    DistributionTemp=zeros([AccumulatorSize,Parameters.Distribution]);
    RelevantBalls=Slice(k):(Slice(k+1)-1);
    TXc=single(Balls.Xc(RelevantBalls,:))';
    if(prod(isnan(TXc(:)))==1);continue;end
    TYc=single(Balls.Yc(RelevantBalls,:))';
    Rc=single(Balls.R(RelevantBalls,:))';
    Dir=(Balls.Dir(RelevantBalls,:))';
    TP=Balls.Phi(RelevantBalls,:)';
    TP1=Balls.Phi1(RelevantBalls,:)';
    EX1=Balls.ExtraRotations(RelevantBalls,:)';
    
    %memory % (LE) I am adding this here since this is where the out of memory
    %error occurs first
    %     Kf=Balls.Kf;exp(1i*cumsum(Balls.AccumulatedLength(RelevantBalls,:)')'*Kf);
    TAC=single(Balls.AlphaCharge(RelevantBalls,:))';
    
    
    IDX=find(~isnan(TXc));
    
    TXc=TXc(IDX);TYc=TYc(IDX);Rc=Rc(IDX);Dir=Dir(IDX);
    TAC=TAC(IDX);
    
    TP=TP(IDX);TP1=TP1(IDX)-2*pi*Dir.*EX1(IDX);
    
    FillerNumbel=Parameters.FillerNumber;
    
    Spacing=ones(numel(IDX),1)*[(0.5/FillerNumbel):(1/FillerNumbel):(1-0.5/FillerNumbel)];
    Spacing=Spacing+(rand(size(Spacing))-0.5)/FillerNumbel;
    
    Rep=ones(1,size(Spacing,2));
    
    ListX=floor((TXc*Rep)+...
        (Rc*Rep).*cos((TP1*Rep).*Spacing...
        +(TP*Rep).*(1-Spacing)));
    
    ListY=floor((TYc*Rep)-...
        (Rc*Rep).*sin((TP1*Rep).*Spacing+...
        (TP*Rep).*(1-Spacing)));
    
    ListT=(TP1*Rep).*Spacing+...
        (TP*Rep).*(1-Spacing);
    %The new Parameters.GridSize addition is to set correctly the normalization
    %exp(-1i*Balls.Kf.*(TP(:,1)-TP1(:,1)).*Rc*ones(1,FillerNumbel).*Spacing).*
    Weight=(TAC(:,1).*(TP(:,1)-TP1(:,1)).*Rc.*Dir(:,1)*ones(1,FillerNumbel)/FillerNumbel.*Parameters.GridSize);
    
    IDX = (ListX(:)) + (ListY(:)-1)*(AccumulatorSize(1));
    Logic=find(((IDX>=1) .* (IDX<=prod(AccumulatorSize))));
    
    if(isfield(Parameters,'Distribution'))
        BarIndexList=floor(mod(-(pi/2-ListT(Logic))/(2*pi),1)*Parameters.Distribution);
        out=accumarray(IDX(Logic)+prod(AccumulatorSize)*BarIndexList(:),Weight(Logic));
        DistributionTemp(1:numel(out))=out;
        Distribution=DistributionTemp+Distribution; %*Parameters.Charge
    end
end
end