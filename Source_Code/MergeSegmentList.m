function in=MergeSegmentList(in)
for k=(size(in,2)-1):-1:1
    if(in(1,k+1)==in(2,k))
        in(2,k)=in(2,k+1);
        in(:,k+1)=[];
    end
end
end