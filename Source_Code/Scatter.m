function Done=Scatter()
global Balls Walls Parameters
Done=0;
%IDXStillActiveLogical=([Walls(Balls.WallNo(Balls.Active,Balls.Step+1)).P]'~=-1);
IDXStillActiveLogical=([Walls(Balls.WallNo(Balls.Active,Balls.Step+1)).AbsorbCoeff]'<rand(numel(Balls.Active),1));

Dying=Balls.Active(find(~IDXStillActiveLogical));

Balls.FinalWall(Dying)=Balls.WallNo(Dying,Balls.Step+1);
% accumarray(Balls.WallNo(Balls.Active(find(~IDXStillActiveLogical)),Balls.Step+1),1,[4,1])
Balls.Active=Balls.Active(find(IDXStillActiveLogical)); %maybe remove the find
WallP=[Walls(Balls.WallNo(Balls.Active,Balls.Step+1)).P]';
if(isempty(Balls.Active));Done=1;return;end
WallType=[Walls(Balls.WallNo(Balls.Active,Balls.Step+1)).Type]';

%%
Logic0=(WallType==0);
Logic1=(WallType==1);
Logic2=(WallType==2);
Logic3=(WallType==3);
%% Type 2
Balls.Theta(Balls.Active(Logic2),Balls.Step+1)=mod(-Balls.Theta1(Balls.Active(Logic2),Balls.Step),2*pi);
Balls.Theta(Balls.Active(Logic2),Balls.Step+1)=mod(WallP(Logic2,1).*Balls.Theta(Balls.Active(Logic2),Balls.Step+1)+...
    (1-WallP(Logic2,1)).*(Parameters.ScatterFunction(sum(Logic2))+...
    InRange(Balls.Theta(Balls.Active(Logic2),Balls.Step+1),pi,2*pi))*pi,2*pi);
%% Type 1
Balls.Theta(Balls.Active(Logic1),Balls.Step+1)=mod((pi-Balls.Theta1(Balls.Active(Logic1),Balls.Step)),2*pi);
RandomTheta=(Parameters.ScatterFunction(sum(Logic1))-0.5+InRange(Balls.Theta(Balls.Active(Logic1),Balls.Step+1),pi/2,3*pi/2))*pi;
Logic_a=(RandomTheta-Balls.Theta(Balls.Active(Logic1),Balls.Step+1)>pi);
Logic_b=(RandomTheta-Balls.Theta(Balls.Active(Logic1),Balls.Step+1)<-pi);
RandomTheta(Logic_a)=RandomTheta(Logic_a)-2*pi;
RandomTheta(Logic_b)=RandomTheta(Logic_b)+2*pi;
Balls.Theta(Balls.Active(Logic1),Balls.Step+1)=WallP(Logic1,1).*Balls.Theta(Balls.Active(Logic1),Balls.Step+1)+(1-WallP(Logic1,1)).*RandomTheta;
%% Type 3
Balls.Theta(Balls.Active(Logic3),Balls.Step+1)=Balls.Theta(Balls.Active(Logic3),Balls.Step)+(rand(1)-0.5)*Parameters.ThetaScatterMR*ones(sum(Logic3),1);
%% Type 0
if(sum(Logic0)>0)
Wall_Angle=atan([Walls(Balls.WallNo(Balls.Active(Logic0),Balls.Step+1)).m]');
% size(Wall_Theta)
% size(Balls.Theta1(Balls.Active(Logic0),Balls.Step))
Balls.Theta(Balls.Active(Logic0),Balls.Step+1)=mod((-2*Wall_Angle-Balls.Theta1(Balls.Active(Logic0),Balls.Step)),2*pi);
% ThetaOri=obj.Theta;  %--------------------------
Wall_Theta=Angle2Theta(Wall_Angle,-1); %from pi/2 to 3*pi/2

BT=mod(Balls.Theta1(Balls.Active(Logic0),Balls.Step),2*pi);
PositiveSlope=InRange(Wall_Theta,0,pi);
HitFromTheRight=InRange(BT-Wall_Theta+pi*(~PositiveSlope),0,pi); %Hit from the right


FlipSide=~xor(HitFromTheRight,PositiveSlope);
% BT((BT+Wall_Theta)>pi)=BT((BT+Wall_Theta)>pi)-2*pi;
RandomTheta=mod((Parameters.ScatterFunction(sum(Logic0))+FlipSide)*pi+Wall_Theta,2*pi);

Logic_a=(RandomTheta-Balls.Theta1(Balls.Active(Logic0),Balls.Step+1)>pi);
Logic_b=(RandomTheta-Balls.Theta1(Balls.Active(Logic0),Balls.Step+1)<-pi);
RandomTheta(Logic_a)=RandomTheta(Logic_a)-2*pi;
RandomTheta(Logic_b)=RandomTheta(Logic_b)+2*pi;
% obj.Theta=P*obj.Theta+(1-P)*RandomTheta;
Balls.Theta(Balls.Active(Logic0),Balls.Step+1)=WallP(Logic0,1).*Balls.Theta(Balls.Active(Logic0),Balls.Step+1)+(1-WallP(Logic0,1)).*RandomTheta;
end
%%
Balls.Theta(Balls.Active,Balls.Step+1)=mod(Balls.Theta(Balls.Active,Balls.Step+1),2*pi);

end