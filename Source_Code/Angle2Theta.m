function out=Angle2Theta(Angle,Dir)
    out=mod( -(Angle-(Dir-1)/2.*pi),2*pi);
end