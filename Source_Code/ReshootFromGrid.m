function [EmitterP,EmitterN,RemoveFromDistribution]=ReshootFromGrid(Simulation,Fraction)

VectorPositive=Simulation.Distribution.Data(:);
VectorNegative=VectorPositive;

VectorPositive(VectorPositive<0)=VectorPositive(VectorPositive<0)*0;
VectorNegative(VectorNegative>0)=VectorNegative(VectorNegative>0)*0;
VectorNegative=-VectorNegative;
%
NP=round(Fraction*sum(VectorPositive));
NN=round(Fraction*sum(VectorNegative));
%
PositiveNewBalls=randsample(1:numel(Simulation.Distribution.Data),NP,true,VectorPositive);
NegativeNewBalls=randsample(1:numel(Simulation.Distribution.Data),NN,true,VectorNegative);

[xPI,yPI,tPI]=ind2sub(size(Simulation.Distribution.Data),PositiveNewBalls);
[xNI,yNI,tNI]=ind2sub(size(Simulation.Distribution.Data),NegativeNewBalls);

EmitterP.X=Simulation.IDensity.axes{1}.data(xPI)+(rand(size(xPI))-0.5)*Simulation.Parameters.GridSize;
EmitterN.X=Simulation.IDensity.axes{1}.data(xNI)+(rand(size(xNI))-0.5)*Simulation.Parameters.GridSize;

EmitterP.Y=Simulation.IDensity.axes{2}.data(yPI)+(rand(size(yPI))-0.5)*Simulation.Parameters.GridSize;
EmitterN.Y=Simulation.IDensity.axes{2}.data(yNI)+(rand(size(yNI))-0.5)*Simulation.Parameters.GridSize;

EmitterP.Theta=Simulation.Distribution.ThetaList(tPI)+(rand(size(tPI))-0.5)*360/Simulation.Parameters.Distribution;
EmitterN.Theta=Simulation.Distribution.ThetaList(tNI)+(rand(size(tNI))-0.5)*360/Simulation.Parameters.Distribution;

EmitterP.Theta=EmitterP.Theta/180*pi;
EmitterN.Theta=EmitterN.Theta/180*pi;

RemoveFromDistribution=(reshape(accumarray(PositiveNewBalls',1,[numel(Simulation.Distribution.Data),1])...
    -accumarray(NegativeNewBalls',1,[numel(Simulation.Distribution.Data),1]),...
    size(Simulation.Distribution.Data)));

end