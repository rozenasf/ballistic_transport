for i=1:10000
ParametersArray=struct('P',0,'lMR',10,'ThetaScatterMR',2*pi,...
    'MaxCollisions',1e3,'N',10,'R',Rlist(k),'MaxMument',2,'Absorbers',[1,3],...
    'Emitter',1,'EmitterRange',[0,1],...
    'EmittedFunciton',@()asin(rand*2-1),...
    'GridSize',0.01,'Charge',1,'ScatterFunction',@(n)asin(rand(n,1)*2-1)/pi+0.5);
%struct('X',300,'Y',70)
WallsCoordinates={[0,0;0,1;6,1;6,0;0,0]'};
%figure(1);
RunSimulation;
if(sum(~isnan(Balls.X(:,end))))
   break 
end
end