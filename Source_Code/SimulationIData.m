classdef SimulationIData
    properties
        Session
        data %is matrix/cell
        axes %is struct
    end
    methods
        function obj = SimulationIData(Data,ParameterSweepBare)
            if(~exist('ParameterSweepBare'))
                ParameterSweepBare=Data{1}.ParameterSweepBare;
                Data=cellfun(@(x){x.Simulation},Data);
                obj.Session=inputname(1);
            end
            [~,Size]=ExplodeStruct(ParameterSweepBare);
            
            if(numel(Size)==1)
                obj.data=cell(Size,1);
            else
                obj.data=cell(Size);
            end
            for i=1:numel(obj.data)
                if(~isempty(Data{i}))
                    obj.data{i}=Data{i};
                else
                    obj.data{i}=[];
                end
            end
            Names=fieldnames(ParameterSweepBare);
            obj.axes=ParameterSweepBare;
            for i=1:numel(Names)
                if(numel(obj.axes.(Names{i}))==1)
                    obj.axes=rmfield(obj.axes,Names{i});
                end
            end
        end
        function obj=SliceValue(obj,varargin)
            for i=1:numel(varargin)/2
                name=varargin{2*i-1};value=varargin{2*i};
                [NameIndex,names]=GetNameIndex(obj,name)
                IndexInLine=zeros(size(value));
                for j=1:numel(value)
                    IndexInLine(j)=find(obj.axes.(names{NameIndex})==value(j));
                end
                obj=SliceIndex(obj,name,IndexInLine);
            end
        end
        function obj=SliceIndex(obj,varargin)
            for i=1:numel(varargin)/2
                name=varargin{2*i-1};IndexInLine=varargin{2*i};
                [NameIndex,names]=GetNameIndex(obj,name);
                Size=size(obj.data);LeftAxesIndex=setdiff(1:numel(size(obj.data)),NameIndex);
                if(numel(IndexInLine)==1)
                    obj.axes=rmfield(obj.axes,names{NameIndex});
                    Tempdata=permute(obj.data,[NameIndex,LeftAxesIndex]);
                    if(numel(Size(LeftAxesIndex))>1)
                        obj.data=reshape(Tempdata(IndexInLine,:),Size(LeftAxesIndex));
                    else
                        obj.data=Tempdata(IndexInLine,:)';
                    end
                else
                    obj.axes.(names{NameIndex})=obj.axes.(names{NameIndex})(IndexInLine);
                    PermuteVec=[NameIndex,LeftAxesIndex];
                    UnPermuteVec=[];UnPermuteVec(PermuteVec)=1:numel(PermuteVec);
                    Tempdata=permute(obj.data,PermuteVec);
                    Tempdata=reshape(Tempdata(IndexInLine,:),[numel(IndexInLine),Size(LeftAxesIndex)]);
                    obj.data=permute(Tempdata,UnPermuteVec);
                end
            end
        end
        function [PermuteVec,UnPermuteVec]=GetPermuteVec(obj,name)
            [NameIndex,~]=GetNameIndex(obj,name);
            Size=size(obj.data);
            LeftAxesIndex=setdiff(1:numel(size(obj.data)),NameIndex);
            PermuteVec=[NameIndex,LeftAxesIndex];
            UnPermuteVec=[];UnPermuteVec(PermuteVec)=1:numel(PermuteVec);
        end
        function [NameIndex,names]=GetNameIndex(obj,name)
            names=fieldnames(obj.axes);
            NameIndex=find(strcmp(names,name));
        end
        function disp(obj)
            names=fieldnames(obj.axes);
            Text=[];
            for i=1:numel(names)
                Text=[Text,sprintf(' %s[%0.2g>>%d>>%0.2g]; ',names{i},obj.axes.(names{i})(1)...
                ,numel(obj.axes.(names{i})),obj.axes.(names{i})(end)  )];
            end
            fprintf([obj.Session,':(',Text(1:end-2),')\n'])
        end
        function out=plus(obj1,obj2)
            out=obj1;
            for i=1:numel(obj1.data)
                out.data{i}=out.data{i}+obj2.data{i};
            end
        end
        function out=minus(obj1,obj2)
            out=obj1;
            for i=1:numel(obj1.data)
                out.data{i}=out.data{i}-obj2.data{i};
            end
        end
        function out=times(obj1,num)
            out=obj1;
            for i=1:numel(obj1.data)
                out.data{i}=out.data{i}*num;
            end
        end
        function out=sum(obj,direction)
            out=obj.SliceIndex(direction,1);
            for i=2:size(obj,direction)
                out=out+obj.SliceIndex(direction,i);
            end
        end
        function out=mean(obj,direction)
            out=sum(obj,direction);out=out.*(1/size(obj,direction));
        end
        function out=cell2mat(obj)
            out=cell2mat(obj.data);
        end
        function out=IData(obj)
            Ax={};
            names=fieldnames(obj.axes);
            for i=1:numel(names)
               Ax{i}=iaxis(names{i},obj.axes.(names{i}));
            end
            if(iscell(obj.data))
                Ndata=cell2mat(obj.data);
            else
                Ndata=obj.data;
            end
            out=idata(obj.Session,Ndata,Ax,{0},' ',[]);
        end
        function obj=cellfun(obj,f)
            obj.data=cellfun(f,obj.data);
        end
        function out=size(obj,Direction)
            if(~exist('Direction'))
               out=size(obj.data); 
            else
                [NameIndex,~]=GetNameIndex(obj,Direction);
                out=size(obj.data,NameIndex);
            end
        end
        function obj=ExtractProperty(obj,Path)
            obj.data=cellfun(str2func(['@(x){x.',Path,'}']),obj.data);
        end
        function out = AddSimulations(obj1,obj2)
            out=obj1;
            for i=1:numel(obj1.data)
                out.data{i}.IDensity=obj1.data{i}.IDensity+obj2.data{i}.IDensity;
                out.data{i}.ICurrent=obj1.data{i}.ICurrent+obj2.data{i}.ICurrent;
                out.data{i}.TotalCurrent=obj1.data{i}.TotalCurrent+obj2.data{i}.TotalCurrent;
            end
        end
        function out = SubtractSimulations(obj1,obj2)
            out=obj1;
            for i=1:numel(obj1.data)
                out.data{i}.IDensity=obj1.data{i}.IDensity-obj2.data{i}.IDensity;
                out.data{i}.ICurrent=obj1.data{i}.ICurrent-obj2.data{i}.ICurrent;
                out.data{i}.TotalCurrent=obj1.data{i}.TotalCurrent-obj2.data{i}.TotalCurrent;
            end
        end
        function out = MultiplySimulations(obj1,Num)
            out=obj1;
            for i=1:numel(obj1.data)
                out.data{i}.IDensity=obj1.data{i}.IDensity.*Num;
                out.data{i}.ICurrent=obj1.data{i}.ICurrent.*Num;
                out.data{i}.TotalCurrent=obj1.data{i}.TotalCurrent.*Num;
            end
        end
        function out=Sym(obj,name)
            out=obj.SliceIndex(name,1);
            [NameIndex,names]=GetNameIndex(obj,name);
            rawlist=obj.axes.(name);
            NewList=unique(abs(rawlist));
            out.axes.(name)=NewList;
            for i=1:numel(NewList)
                PI=find(rawlist==NewList(i));if(numel(PI)>1);PI=PI(1);end
                NI=find(rawlist==-NewList(i));if(numel(NI)>1);NI=NI(1);end
                if(numel(PI)==1 && numel(NI)==1)
                    NewMat=MultiplySimulations((AddSimulations(obj.SliceIndex(name,PI),obj.SliceIndex(name,NI))),0.5);
                elseif(numel(PI)==1 && numel(NI)==0)
                    NewMat=(obj.SliceIndex(name,PI));
                elseif(numel(PI)==0 && numel(NI)==1)
                    NewMat=(obj.SliceIndex(name,NI));
                else
                    error('FAIL!');
                end
                if(i==1)
                    out.data=NewMat.data;
                else
                    out.data=cat(numel(names),out.data,NewMat.data);
                end
            end
        end
        function out=ASym(obj,name)
            out=obj.SliceIndex(name,1);
            [NameIndex,names]=GetNameIndex(obj,name);
            rawlist=obj.axes.(name);
            NewList=unique(abs(rawlist));
            out.axes.(name)=NewList;
            for i=1:numel(NewList)
                PI=find(rawlist==NewList(i));if(numel(PI)>1);PI=PI(1);end
                NI=find(rawlist==-NewList(i));if(numel(NI)>1);NI=NI(1);end
                if(numel(PI)==1 && numel(NI)==1)
                    NewMat=MultiplySimulations((SubtractSimulations(obj.SliceIndex(name,PI),obj.SliceIndex(name,NI))),0.5);
                elseif(numel(PI)==1 && numel(NI)==0)
                    NewMat=(obj.SliceIndex(name,PI)).*0;
                elseif(numel(PI)==0 && numel(NI)==1)
                    NewMat=(obj.SliceIndex(name,NI)).*0;
                else
                    error('FAIL!');
                end
                if(i==1)
                    out.data=NewMat.data;
                else
                    out.data=cat(numel(names),out.data,NewMat.data);
                end
            end
        end
    end
end