function PlotSimulationSimple()
global Walls Balls
    clf
    PlotWalls(Walls);
    hold on
    plot(Scale2Real(Balls.X',1),Scale2Real(Balls.Y',2))
    hold off
    axis equal
end