function out=AngDiff(An1,An2)
    if(isempty(An1));out=An1;return;end
    out=mod(An1-An2,2*pi);
    out=min([out,2*pi-out]')';
end