function out=UnWrapPhase(in)
% for i=1:size(in,2)
%     if(in(2,i)<in(1,i))
%         in(2,i)=in(2,i)+2*pi;
%         if(i<size(in,2))
%             in(2,i+1:end)=in(2,i+1:end)+2*pi;
%         end
%     end
% end
out=[];
for k=1:size(in,2)
    if(in(2,k)<in(1,k))
        out=[out,[in(1,k),0;2*pi,in(2,k)]];
    else
        out=[out,in(:,k)];
    end
end
end