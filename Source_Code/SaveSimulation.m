function ToSave=SaveSimulation(SaveLocation,varargin)
    ToSave=struct();
    for i=1:numel(varargin)
        ToSave.(inputname(i+1))=DigIn(varargin{i});
    end
    if(nargout==0)
        save(SaveLocation,'-struct','ToSave');
    end
    function input=DigIn(input)
        switch(class(input))
            case 'cell'
                for j=1:numel(input)
                    input{j}=DigIn(input{j});
                end
            case 'struct'
                if(numel(input)==1)
                    if(isfield(input,'Distribution'))
                        input.Distribution=[];
                        input.Transport.StartWall=[];
                        input.Transport.EndWall=[];
                        input.Transport.LengthOfFlight=[];
                        input.Transport.Charge=[];
                    else
                        Names=fieldnames(input);
                        for j=1:numel(Names)
                            input.(Names{j})= DigIn(input.(Names{j}));
                        end
                    end
                else
                    for j=1:numel(input)
                        input(j)=DigIn(input(j));
                    end
                end
        end
    end
% function SaveSimulation(name)
%still very costly in memory with no reason. fix it in later version
%     save(['Q:\users\asafr\simulations\',name,'.mat'], '-regexp', '^(?!(Weight|ListT|ListX|ListY|Logic|IDX|Dum|Balls|out|EX1)$).')
% end
end
