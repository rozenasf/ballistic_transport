function [Accumulator,Distribution]=AddToAccumulatorNoField(Balls,Parameters,AccumulatorSize)
%-------------------------------------------%
% Written by Asaf Rozen - Weizmann Insitute %
%       asaf.rozen@weizmann.ac.il           %
%-------------------------------------------%

Distribution=[];
Accumulator={};
for i=1:Parameters.MaxMument
    Accumulator{i}=zeros(AccumulatorSize);
end

    Distribution=zeros([AccumulatorSize,Parameters.Distribution]);
    

% Balls.X(:,end)=nan;Balls.Y(:,end)=nan;Balls.Theta(:,end)=nan; % fix for accumarray bug
if(~isfield(Parameters,'Slice'));Parameters.Slice=2;end
Slice=round(linspace(1,size(Balls.X,1)+1,Parameters.Slice));
for k=1:(numel(Slice)-1)
    DistributionTemp=zeros([AccumulatorSize,Parameters.Distribution]);
    RelevantBalls=Slice(k):(Slice(k+1)-1);
    TX=single(Balls.X(RelevantBalls,:))';
    if(prod(isnan(TX(:)))==1);continue;end
    TY=single(Balls.Y(RelevantBalls,:))';
    TT=single(Balls.Theta(RelevantBalls,:))';
    TAC=single(Balls.AlphaCharge(RelevantBalls,:))';
    TX=[TX(1:end-1)',TX(2:end)'];
    TY=[TY(1:end-1)',TY(2:end)'];
    TT=[TT(1:end-1)'];
    TAC=[TAC(1:end-1)'];
    IDX=find(prod(~isnan(TX),2));
    TX=TX(IDX,:);TY=TY(IDX,:);TT=TT(IDX,:);TAC=TAC(IDX,:);
    

        FillerNumbel=Parameters.FillerNumber;
    
    Spacing=ones(numel(IDX),1)*[(0.5/FillerNumbel):(1/FillerNumbel):(1-0.5/FillerNumbel)];
    Spacing=Spacing+(rand(size(Spacing))-0.5)/FillerNumbel;
    
    Rep=ones(1,size(Spacing,2));
    %The buttle neck
    ListX=floor((TX(:,1)*Rep).*Spacing+...
        (TX(:,2)*Rep).*(1-Spacing));
    ListY=floor((TY(:,1)*Rep).*Spacing+...
        (TY(:,2)*Rep).*(1-Spacing));
    ListT=(TT*Rep).*Spacing+(TT*Rep).*(1-Spacing);
    %end
    
    %The new Parameters.GridSize addition is to set correctly the normalization
    Weight=TAC(:,1).*sqrt((TX(:,2)-TX(:,1)).^2+(TY(:,2)-TY(:,1)).^2)*ones(1,FillerNumbel)/FillerNumbel*Parameters.GridSize;
    TT=TT*ones(1,FillerNumbel);
    IDX = (ListX(:)) + (ListY(:)-1)*AccumulatorSize(1);
    
    Logic=find(((IDX>=1) .* (IDX<=prod(AccumulatorSize))));
    

    if(isfield(Parameters,'Distribution'))
        
        BarIndexList=floor(mod(ListT(Logic)/(2*pi),1)*Parameters.Distribution);
        out=accumarray(IDX(Logic)+prod(AccumulatorSize)*BarIndexList(:),Weight(Logic));
        DistributionTemp(1:numel(out))=out;
        Distribution=DistributionTemp+Distribution; %*Parameters.Charge
    end
end
end